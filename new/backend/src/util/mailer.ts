import nodemailer from "nodemailer";
import fs from "fs";
import * as Suml from "@pronounspage/common/suml";
import log from "#self/log";
import Handlebars from "handlebars";
import { HelperDelegate, HelperOptions } from "handlebars";
import { getConfig } from "#self/config";
import { Locale, getLocale } from "#self/locales";
import { LOGO_COLOR } from "@pronounspage/common/constants";
import { db } from "#self/db";
import time from "@pronounspage/common/util/time";
import { minify as htmlMinify, Options as MinifyOptions } from "html-minifier-terser";
import { getStaticData, StaticData } from "#self/util/static";

export const DEFAULT_MAXWIDTH = '480px';
export const MINIFY_OPTIONS: MinifyOptions = {
    collapseWhitespace: true,
    conservativeCollapse: true,
    removeComments: true,
    sortAttributes: true,
    sortClassName: true
}

let transporter: nodemailer.Transporter;
function getTransporter() {
    if (transporter == null) {
        const config = getConfig();
        transporter = nodemailer.createTransport(config.mailer.transport, { from: config.mailer.fromAddress });
    }
    return transporter;
}

function sendEmail(to: string, subject: string, text: string | undefined = undefined, html: string | undefined = undefined) {
    getTransporter().sendMail({
        to,
        subject,
        text,
        html,
    }, function(err) {
        if (err) {
            log.warn(`Failed to send e-mail: `, err);
        }
    });
}

export enum TemplateContext {
    Subject = "subject",
    Html = "html",
    Text = "text"
}

interface EmailTemplate<T> extends Partial<Record<TemplateContext, T | undefined>> {
    subject: T,
    html: T,
    text?: T | undefined
}

type EmailTemplateSource = EmailTemplate<string>;
type CompiledEmailTemplate = EmailTemplate<Handlebars.TemplateDelegate>;

const handlebarsInstance = Handlebars.create();

type EmailTemplateContext = {
    locale: any;
    _locale: Locale;
    templateContext: TemplateContext;
    handlebarsOptions: Handlebars.RuntimeOptions;
    maxwidth: string;
    static: StaticData;
}

interface EmailHelperOptions extends HelperOptions {
    data: { root: EmailTemplateContext };
}

type CachedTranslationData = { success: true; template: Handlebars.TemplateDelegate } | { success: false; error: unknown; };
type CachedTranslation = CachedTranslationData & { lastLoaded: number };
let translationCache: Record<string, CachedTranslation> = {};
const translate = ((key: string, options: EmailHelperOptions) => {
    const context: EmailTemplateContext = options.data.root;

    const locale = context._locale;
    const translationCacheKey = `${locale.code}::${key}`;
    let translation = translationCache[translationCacheKey];
    if (translation == undefined || translation.lastLoaded !== locale.lastLoaded) {
        try {
            const template = handlebarsInstance.compile(locale.getTranslation(key), {noEscape: true});
            translation = { success: true, template, lastLoaded: locale.lastLoaded };
        } catch (e) {
            translation = {
                success: false,
                lastLoaded: locale.lastLoaded,
                error: e
            }
        }
        translationCache[locale.code] = translation;
    }

    if (!translation.success) {
        throw new Error(`Cannot translate key '${key}': ${translation.error}`);
    }

    return translation.template(context, context.handlebarsOptions);
}) satisfies HelperDelegate;
const list = ((array: Array<string>, options: EmailHelperOptions) => {
    if (!Array.isArray(array)) {
        throw new Error("Argument must be an array!");
    }
    const context: EmailTemplateContext = options.data.root;
    switch (context.templateContext) {
        case TemplateContext.Subject:
            throw new Error("Lists are not supported in the subject");
        case TemplateContext.Html:
            return array.map(s => `<li>${s}</li>`).join("");
        case TemplateContext.Text:
            return array.map(s => ` - ${s}`).join("\n");
    }
}) satisfies HelperDelegate;
const nl2br = ((str: string, options: HelperOptions) => {
    const context: EmailTemplateContext = options.data.root;
    switch (context.templateContext) {
        case TemplateContext.Subject:
            return str;
        case TemplateContext.Html:
            return str.replace("\n", "<br>");
        case TemplateContext.Text:
            return str;
    }
}) satisfies HelperDelegate;
handlebarsInstance.registerHelper({ translate, list, nl2br });

function compileTemplate(template: EmailTemplateSource | CompiledEmailTemplate): CompiledEmailTemplate {
    const subject = typeof template.subject === "string";
    const html = typeof template.html === "string";
    const text = typeof template.text === "string";
    if (subject) {
        template.subject = handlebarsInstance.compile(template.subject, { noEscape: true });
    }
    if (html) {
        template.html = handlebarsInstance.compile(template.html);
    }
    if (text) {
        template.text = handlebarsInstance.compile(template.text, { noEscape: true });
    }
    return template as CompiledEmailTemplate;
}

const baseTemplate = compileTemplate({
    subject: `{{translate 'title'}} » {{{content}}}`,
    html: `
            <div style="margin: 36px auto; width: 100%; max-width: {{maxwidth}}; border: 1px solid #aaa;border-radius: 8px;overflow: hidden;font-family: Nunito, Quicksand, Helvetica, sans-serif;font-size: 16px;box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15)">
                <div style="padding: 16px; padding-top: 10px; background: #f8f8f8; border-bottom: 1px solid #aaa;font-size: 20px;color: ${LOGO_COLOR};">
                    <img src="{{ static.logo.primary }}" style="height: 24px;width: 24px; position: relative; top: 6px; margin-right: 6px;" alt="Logo"/>
                    {{translate 'title'}}
                </div>
                <div style="padding: 8px 16px; background: #fff;">
                    {{{content}}}
                </div>
            </div>
        `,
});

const templates: Record<string, EmailTemplate<string>> = {
    notify: {
        subject: "There are entries awaiting moderation",
        text: "Entries awaiting moderation:\n\n{{list stats}}",
        html: `
            <p>Entries awaiting moderation</p>
            <ul>{{list stats}}</ul>
        `,
    },
    confirmCode: {
        subject: "{{translate 'user.login.email.subject'}}",
        text: `{{translate 'user.login.email.instruction'}}\n\n{{code}}\n\n{{translate 'user.login.email.extra'}}`,
        html: `
            <p>{{translate 'user.login.email.instruction'}}</p>
            <p style="border: 1px solid #aaa;border-radius: 8px;overflow: hidden;text-align: center;user-select: all;font-size: 24px; padding:8px;letter-spacing: 8px; font-weight: bold;">{{code}}</p>
            <p style="font-size: 12px; color: #777">{{translate 'user.login.email.extra'}}</p>
        `,
    },
    ban: {
        subject: "{{translate 'ban.header'}}",
        text: `{{translate 'ban.header'}}\n\n{{translate 'ban.reason'}}{{translate 'quotation.colon'}} {{reason}}\n\n{{translate 'quotation.start'}}{{locale.tosContentPart}}{{translate 'quotation.end'}}`,
        html: `
            <p>{{translate 'ban.header'}}</p>
            <p>{{translate 'ban.reason'}}{{translate 'quotation.colon'}} {{reason}}</p>
            <p style="font-size: 12px; color: #777">{{translate 'quotation.start'}}{{locale.tosContentPart}}{{translate 'quotation.end'}}</p>
            <p style="font-size: 14px;">{{translate 'ban.appealEmail'}}</p>
            <p style="color: #999; font-size: 10px;">@{{username}}</p>
        `,
    },
    inactivityWarning: {
        subject: "{{translate 'user.removeInactive.email.subject'}}",
        text: "{{translate 'user.removeInactive.email.content'}}",
        html: `
            <p>{{translate 'user.removeInactive.email.content'}}</p>
            <p>{{translate 'user.removeInactive.email.clarification'}} @{{username}}</p>
            <p style="text-align: center; padding-top: 16px; padding-bottom: 16px;">
                <a href="https://en.pronouns.page/account" target="_blank" rel="noopener" style="background-color: #C71585; color: #fff; padding: 8px 16px; border: none; border-radius: 6px;text-decoration: none">
                    {{translate 'user.removeInactive.email.cta'}}
                </a>
            </p>
            <p style="color: #999; font-size: 10px;">@{{username}}</p>
        `,
    },
    cardsWarning: {
        subject: "Cards queue is getting long",
        text: "There's {{count}} cards in the queue!",
        html: "<p>There's {{count}} cards in the queue!</p>",
    },
    linksWarning: {
        subject: "Links queue is getting long",
        text: "There's {{count}} links in the queue!",
        html: "<p>There's {{count}} links in the queue!</p>",
    },
    translationProposed: {
        subject: "[{{locale.code}}] New translations proposed",
        text: "Check them out here: https://{{translate 'domain'}}/admin",
        html: "<p>Check them out here: <a href=\"https://{{translate 'domain'}}/admin/translations/awaiting\" target=\"_blank\" rel=\"noopener\">{{translate 'domain'}}/admin/translations/awaiting</a></p>",
    },
    translationToMerge: {
        subject: "[{{locale.code}}] New translations ready to be merged",
        text: "Check them out here: https://{{translate 'domain'}}/admin",
        html: "<p>Check them out here: <a href=\"https://{{translate 'domain'}}/admin/translations/awaiting\" target=\"_blank\" rel=\"noopener\">{{translate 'domain'}}/admin/translations/awaiting</a></p>",
    },
    modMessage: {
        subject: "{{translate 'user.modMessage.subject'}}",
        text: `{{translate 'user.modMessage.intro'}}\n\n{{nl2br:message}}\n\n{{translate 'user.modMessage.respond'}}`,
        html: `
            <p>{{translate 'user.modMessage.intro'}}</p>
            <p style="color: #222; padding-left: 1em;padding-right: 1em;font-style: italic;">{{nl2br message}}</p>
            <p>{{translate 'user.modMessage.respond'}}</p>
            <p style="color: #999; font-size: 10px;">@{{modUsername}} → @{{username}}</p>
        `,
    },
    sensitiveApplied: {
        subject: "{{translate 'profile.sensitive.email.subject'}}",
        text: `{{translate 'profile.sensitive.email.content'}}\n\n{{warnings}}`,
        html: `
            <p>{{translate 'profile.sensitive.email.content'}} <strong>{{warnings}}</strong></p>
            <p style="color: #999; font-size: 10px;">@{{modUsername}} → @{{username}}</p>
        `,
    },
    miastamaszerujace: {
        subject: "miastamaszerujace.pl – zmiana treści",
        text: "",
        html: `
            <div style="display: flex;">
                <div style="width: 50%; overflow-x: scroll">
                    <p>przed:</p>
                    <pre><code>{{before}}</code></pre>
                </div>
                <div style="width: 50%; overflow-x: scroll">
                    <p>po:</p>
                    <pre><code>{{after}}</code></pre>
                </div>
            </div>
        `,
    },
};


export async function applyTemplate(locale: Locale, template: string | CompiledEmailTemplate | EmailTemplateSource, context: TemplateContext, params: any): Promise<string | undefined> {
    if (!params["maxwidth"]) {
        params["maxwidth"] = "480px";
    }
    if (typeof (template) === "string") {
        template = templates[template];
    }
    if (template == undefined) {
        return undefined;
    }
    template = compileTemplate(template);

    // This really just reassigns the name of the variable (and gives TypeScript some type inference); the object is the same
    const paramsCtx = params as EmailTemplateContext;

    function makeParamCtx(paramsCtx: EmailTemplateContext) {
        paramsCtx._locale = locale;
        // console.log(JSON.stringify(locale));
        if (paramsCtx.locale == undefined) {
            paramsCtx.locale = locale.toJSON();
        }
        paramsCtx.templateContext = context;
        paramsCtx.handlebarsOptions = {}
        if (!paramsCtx.maxwidth) {
            paramsCtx.maxwidth = DEFAULT_MAXWIDTH;
        }
        paramsCtx.static = getStaticData();
    }
    makeParamCtx(paramsCtx);
    // console.log(context);

    const specificTemplate = template[context];

    if (specificTemplate == undefined) {
        return undefined;
    }

    let result;

    result = specificTemplate(params);

    const specificBaseTemplate = baseTemplate[context];
    if (specificBaseTemplate != null) {
        const data = { content: result } as any;
        makeParamCtx(data as EmailTemplateContext);
        result = specificBaseTemplate(data);
    }
    if (context === TemplateContext.Html) {
        result = await htmlMinify(result, MINIFY_OPTIONS);
    }

    return result;
};

export async function sendLocalisedEmail(to: string, locale: Locale, template: string, params = {}) {
    const config = getConfig();
    params = Object.create(params); // Make a copy so that we don't interfere
    sendEmail(
        config.mailer.overwrite || to,
        (await applyTemplate(locale, template, TemplateContext.Subject, params))!,
        await applyTemplate(locale, template, TemplateContext.Text, params),
        await applyTemplate(locale, template, TemplateContext.Html, params),
    );
}

/**
 * Helper for deduplicating emails.
 * It will check that no email (by the identifier `email`) has been sent within a certain amount of seconds
 * (`ttlSeconds`, default 5 minutes), and add a record if that's the case.
 *
 * @param email The e-mail.
 *              In reality this does not have to be an e-mail - you could add prefixes to
 *              multiply the possible uses of this function.
 * @param success Will be run if the function does not find an e-mail sent within the last `ttlSeconds`.
 *                In that case, a record will be added to the database before this function is run, and the function
 *                will be provided with the record's `sentAt` field (in seconds).
 * @param failure Will be run if the function finds at least one e-mail sent within the last `ttlSeconds`.
 *                No change will be performed in this case.
 * @param ttlSeconds The amount of seconds the records should "live" for.
 */
export async function deduplicateEmail(email: string, success: (sentAtSeconds: number) => Awaitable<void>, failure: () => Awaitable<void>, ttlSeconds: number = 5 * 60) {
    const found = await db.email.count({
        where: {
            email: email,
            sentAt: {
                gte: time.nowSeconds() - ttlSeconds
            }
        }
    });
    if (found > 0) {
        log.trace({email}, `Duplicate email request for ${email}`);
        if (failure) { await failure(); }
    } else {
        const sentAtSeconds = time.nowSeconds();
        await db.email.create({
            data: {
                email,
                sentAt: sentAtSeconds
            }
        })
        await success(sentAtSeconds);
    }
}