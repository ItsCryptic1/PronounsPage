import { db, Authenticator, User } from "#self/db";
import { ulid } from "ulid";
import * as social from "#self/social";
import time from "@pronounspage/common/util/time";
import { isInBanArchive } from "./ban.js";

export enum AuthenticatorType {
    Apple = "apple",
    ChangeEmail = "changeEmail",
    Discord = "discord",
    Email = "email",
    Facebook = "facebook",
    Google = "google",
    IndieAuth = "indieauth",
    Mastodon = "mastodon",
    MultifactorRecovery = "mfa_recovery",
    MultifactorSecret = "mfa_secret",
    Twitter = "twitter",
}

type IAuthenticatorPayload = Record<string, string | null | undefined>;

export interface EmailAuthenticatorPayload extends IAuthenticatorPayload {
    username?: string | null;
    email: string;
    code: string;
}

export interface GenericAuthenticatorPayload extends IAuthenticatorPayload {
    id: string;
}

export interface GenericSocialAuthenticatorPayload
    extends IAuthenticatorPayload {
    avatar?: string;
    avatarCopy?: string;
}

type AuthenticatorPayload = IAuthenticatorPayload | string;
type SocialPayloadTypes = {
    [K in keyof typeof social.concreteLookup]: GenericSocialAuthenticatorPayload;
};
type PayloadTypes = Record<AuthenticatorType, AuthenticatorPayload> &
    SocialPayloadTypes & {
        email: EmailAuthenticatorPayload;
        mfa_secret: string;
        mfa_recovery: GenericAuthenticatorPayload;
    };
type PayloadTypeKey = keyof PayloadTypes;

export async function saveAuthenticator<T extends AuthenticatorType>(
    type: T,
    user: User | null,
    payload: PayloadTypes[T],
    validForMinutes?: number
): Promise<Authenticator> {
    if (!user && (await findInBanArchiveWithPayload(type, payload))) {
        // NOTE(tecc): Maybe we shouldn't throw here? Perhaps info in the return type would be more suitable
        throw new Error("banned");
    }

    const id = ulid();

    const authenticator = await db.authenticator.create({
        data: {
            id,
            type: type as string,
            userId: user?.id,
            payload: JSON.stringify(payload),
            validUntil: validForMinutes
                ? Date.now() + validForMinutes * 60
                : null,
        },
    });
    if (user && user.socialLookup) {
        await saveSocialLookupFromAuthenticator({
            userId: user.id,
            type,
            payload,
        });
    }
    return authenticator;
}

interface AuthenticatorSave<K extends PayloadTypeKey> {
    userId: string;
    type: K;
    payload: PayloadTypes[K];
}

export async function saveSocialLookupFromAuthenticator<
    K extends keyof PayloadTypes,
>(authenticator: AuthenticatorSave<K>) {
    const lookup = social.lookup[authenticator.type];
    if (lookup == undefined) {
        return;
    }
    for (let field of lookup.payloadFields) {
        const identifier = (authenticator.payload as Record<string, string>)[
            field
        ];
        if (identifier == undefined) {
            continue;
        }
        // Maybe this should be an upsert but that would be changing behaviour quite drastically
        await db.socialLookup.create({
            data: {
                userId: authenticator.userId,
                provider: authenticator.type,
                identifier: identifier,
            },
        });
    }
}

export async function findLatestEmailAuthenticator(
    email: string,
    type: string
) {
    const authenticator = await db.authenticator.findFirst({
        where: {
            payload: {
                contains: `"email":"${email}+"`,
            },
            type: type,
            OR: [
                {
                    validUntil: null,
                },
                {
                    validUntil: { gt: time.nowSeconds() },
                },
            ],
        },
        orderBy: {
            id: "desc",
        },
    });

    if (authenticator) {
        authenticator.payload = JSON.parse(authenticator.payload);
    }

    return authenticator;
}

interface ParsedAuthenticator<
    K extends keyof PayloadTypes = keyof PayloadTypes,
> {
    id: string;
    userId: string | null;
    type: K;
    payload: PayloadTypes[K];
}

function parseAuthenticator<T extends AuthenticatorType>(
    authenticator: Authenticator | null | undefined
): ParsedAuthenticator<T> | null {
    if (authenticator == null) {
        return null;
    }
    return {
        id: authenticator.id,
        userId: authenticator.userId ?? null,
        type: authenticator.type as T,
        payload: JSON.parse(authenticator.payload),
    };
}

export async function findAuthenticatorsByUser<T extends AuthenticatorType>(
    userId: string,
    type: T
): Promise<Array<ParsedAuthenticator<T>>>;
export async function findAuthenticatorsByUser(
    userId: string
): Promise<Array<ParsedAuthenticator>>;
export async function findAuthenticatorsByUser(
    userId: string,
    type: string
): Promise<Array<ParsedAuthenticator>>;
export async function findAuthenticatorsByUser<T extends AuthenticatorType>(
    userId: string,
    type?: T | undefined
): Promise<Array<ParsedAuthenticator<T>>> {
    const authenticator = await db.authenticator.findMany({
        where: {
            userId,
            type,
            OR: [
                {
                    validUntil: null,
                },
                {
                    validUntil: time.nowSeconds(),
                },
            ],
        },
    });

    return authenticator.map(parseAuthenticator) as Array<
        ParsedAuthenticator<T>
    >;
}

export async function findAuthenticatorByUser<T extends AuthenticatorType>(
    userId: string,
    type?: T | undefined
): Promise<ParsedAuthenticator<T> | null> {
    const result = await db.authenticator.findFirst({
        where: {
            userId,
            type,
            OR: [
                {
                    validUntil: null,
                },
                {
                    validUntil: time.nowSeconds(),
                },
            ],
        },
    });
    return parseAuthenticator(result);
}

export function findInBanArchiveWithPayload<
    T extends AuthenticatorType = AuthenticatorType,
>(type: AuthenticatorType, value: PayloadTypes[T]) {
    if (type === AuthenticatorType.Email) {
        return isInBanArchive(type, (value as EmailAuthenticatorPayload).email);
    }
    throw new Error("Unsupported payload type");
}

export async function findAuthenticatorById<
    T extends AuthenticatorType = AuthenticatorType,
>(authenticatorId: string, type: T): Promise<ParsedAuthenticator<T> | null> {
    return parseAuthenticator(
        await db.authenticator.findFirst({
            where: {
                id: authenticatorId,
                type,
            },
        })
    );
}

export async function invalidateAuthenticator(authenticatorId: string) {
    await db.authenticator.update({
        where: {
            id: authenticatorId,
        },
        data: {
            validUntil: time.nowSeconds(),
        },
    });
}
