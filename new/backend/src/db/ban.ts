import { db } from "#self/db";
import { AuthenticatorType, EmailAuthenticatorPayload } from "#self/db/authenticator";

export async function isInBanArchive(type: AuthenticatorType, value: string) {
    const found = await db.ban.findFirst({
        where: {
            type,
            value
        }
    });
    return found != null;
}