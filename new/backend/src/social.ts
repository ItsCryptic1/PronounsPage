export interface LookupDefinition {
    payloadFields: Array<string>;
}

// TODO(tecc): Handlers
export const concreteLookup = {
    discord: {
        payloadFields: ["username", "id"],
    },
    facebook: {
        payloadFields: ["id"],
    },
    google: {
        payloadFields: ["id"],
    },
    indieauth: {
        payloadFields: ["indieauth"],
    },
    mastodon: {
        payloadFields: ["name", "id"],
    },
    twitter: {
        payloadFields: ["name"],
    },
};
export const lookup: typeof concreteLookup & Record<string, LookupDefinition> = concreteLookup;