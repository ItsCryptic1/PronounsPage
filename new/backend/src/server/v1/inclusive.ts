import { localeSpecific } from "#self/server/v1";
import { db, Inclusive } from "#self/db";

export function transformInclusive(inc: Inclusive) {
    return {
        id: inc.id,
        insteadOf: inc.insteadOf,
        say: inc.say,
        because: inc.because,
        locale: inc.locale,
    };
}

export const plugin = async function (app) {
    app.get(
        "/:locale/inclusive",
        {
            schema: {
                params: localeSpecific,
            },
        },
        async (req, reply) => {
            const values = await db.inclusive.findMany({
                where: {
                    locale: req.params.locale,
                },
                take: 50,
            });
            return reply.send(values.map(transformInclusive));
        }
    );
} satisfies AppPluginAsync;
export default plugin;
