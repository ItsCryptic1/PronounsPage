import * as SumlImpl from "suml";

declare module "suml";

const instance = new SumlImpl.default();

export function parse(value: string): unknown {
    return instance.parse(value);
}

export function dump(value: unknown): string {
    return instance.dump(value);
}
