export function identity<T>(value: T): T {
    return value;
}

// Generally, don't use this function to define commonly used functions.
// Prefer to write a manual implementation.
export function not<Args extends Array<unknown>>(
    f: (...args: Args) => boolean
): (...args: Args) => boolean {
    return (...args: Args) => !f(...args);
}
export function isNotNull<T>(value: T | null | undefined): value is T {
    return value != null;
}
export function isNull<T>(
    value: T | null | undefined
): value is null | undefined {
    return value == null;
}
export function isNotBlank(value: string | null | undefined): boolean {
    if (!isNotNull(value)) {
        return false;
    }
    if (value.length < 1) {
        return false;
    }
    return value.trim().length > 0;
}
export const isBlank = not(isNotBlank);

// This is unnecessarily typed but I don't really care, it works doesn't it?
export function toLowerCase<S extends string>(value: S): Lowercase<S> {
    return value.toLowerCase() as Lowercase<S>;
}

export function normalise(value: string) {
    return toLowerCase(value.trim());
}

/**
 * A generic `toJSON` implementation.
 * It supports getters and does not serialise properties
 * that start with _.
 *
 * @see https://github.com/Microsoft/TypeScript/issues/16858#issuecomment-384715673
 */
export function toJSONGeneric(target: any): any {
    const proto = Object.getPrototypeOf(target);
    const jsonObj: any = Object.assign({}, target);

    Object.entries(Object.getOwnPropertyDescriptors(proto))
        .filter(([key, descriptor]) => typeof descriptor.get === "function")
        .map(([key, descriptor]) => {
            if (descriptor && key[0] !== "_") {
                try {
                    const val = (target as any)[key];
                    jsonObj[key] = val;
                } catch (error) {
                    console.error(`Error calling getter ${key}`, error);
                }
            }
        });

    return jsonObj;
}

export const NUMERIC_CHARS = "01234656789";
export const UPPER_ALPHA_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
export const LOWER_ALPHA_CHARS = "abcdefghijklmnopqrstuvwxyz";
export const ALPHA_CHARS = UPPER_ALPHA_CHARS + LOWER_ALPHA_CHARS;
export const ALPHANUMERIC_CHARS = ALPHA_CHARS + NUMERIC_CHARS;
export function makeId(
    length: number,
    characters: string = ALPHANUMERIC_CHARS
): string {
    let result = "";
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(
            Math.floor(Math.random() * charactersLength)
        );
    }

    return result;
}

export function parseInteger(value: string, radix?: number): number {
    if (radix != null && (radix < 2 || radix > 36)) {
        throw new Error(
            `invalid radix ${radix} - must be between 2 and 36 (inclusive)`
        );
    }
    const parsed = parseInt(value, radix);
    if (isNaN(parsed)) {
        throw new Error("parsed value is NaN");
    }
    return parsed;
}

export function parseBool(value: unknown): boolean {
    switch (typeof value) {
        case "boolean":
            return value;
        case "string":
            value = value.trim().toLowerCase();
            if (value === "true" || value === "1" || value === "yes") {
                return true;
            }
            if (value === "false" || value === "0" || value === "no") {
                return false;
            }
            throw new Error(`unknown boolean constant: ${value}`);
        case "number":
            return value != 0;
        default:
            throw new Error(`Cannot convert value ${value} to a boolean`);
    }
}

export function capitalizeFirstLetter(value: string) {
    return value.charAt(0).toUpperCase() + value.substring(1);
}

const compileTemplateDefaultValue = (key: string) => `{unknown key ${key}}`;

// TODO: Benchmark this function with different implementations.
/**
 * Compiles a template with specified parameters.
 * Really, it's the "processing" part of the template but whatever.
 *
 * **SHOULD ONLY BE USED FOR SIMPLE STRINGS.** If you need more complex templates, use Handlebars or something.
 *
 * @param template The template string
 * @param values The values that can be substituted into the template (equivalent of Handlebars data/context)
 * @param defaultValue A function that provides a default substitution for a key
 */
export function compileTemplate(
    template: string,
    values: Record<string, string>,
    defaultValue: (key: string) => string = compileTemplateDefaultValue
): string {
    let string = "";
    let key = null;
    for (let i = 0; i < template.length; i++) {
        const c = template.charAt(i);
        if (key === null) {
            switch (c) {
                case "{":
                    key = "";
                    continue;
                default:
                    string += c;
                    break;
            }
        } else {
            switch (c) {
                case "}":
                    string += values[key] ?? defaultValue(key);
                    key = null;
                    continue;
                default:
                    key += c;
                    break;
            }
        }
    }
    /*for (const key in values) {
        const value = values[key];
        string = string.replace(`{${key}}`, value);
    }*/
    return string;
}
