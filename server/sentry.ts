import fs from 'fs';

import SentryCli from '@sentry/cli';

import './setup.ts';
import type { Config } from '../locale/config.ts';
import allLocales from '../locale/locales.ts';

import { loadSuml } from './loader.ts';

const __dirname = new URL('.', import.meta.url).pathname;

async function notifyDeployment(): Promise<void> {
    if (!process.env.SENTRY_DSN) {
        process.stdout.write('SENTRY_DSN is not defined, skipping deployment information to Sentry');
        return;
    }

    const version = fs.readFileSync(`${__dirname}/../cache/version`, 'utf-8');
    const config = loadSuml('config') as Config;

    const sentryCli = new SentryCli();
    await sentryCli.releases.setCommits(version, {
        auto: true,
    });
    await sentryCli.releases.finalize(version);
    const environment = process.env.NODE_ENV === 'production' ? config.locale : process.env.NODE_ENV!;
    await sentryCli.releases.newDeploy(version, {
        env: environment,
        url: allLocales.find((locale) => locale.code === config.locale)?.url,
    });
    process.stdout.write(`Sent deployment information for environment ${environment} to Sentry`);
}

await notifyDeployment();
