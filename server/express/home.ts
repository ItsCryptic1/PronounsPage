import fs from 'fs';

import { Router } from 'express';

import buildLocaleList from '../../src/buildLocaleList.ts';
import { handleErrorAsync } from '../../src/helpers.ts';
import { rootDir } from '../paths.ts';

const router = Router();

router.get('/versions', handleErrorAsync(async (req, res) => {
    return res.json(buildLocaleList(global.config.locale));
}));

router.get('/locales', handleErrorAsync(async (req, res) => {
    return res.json(buildLocaleList(global.config.locale));
}));

router.get('/version', handleErrorAsync(async (req, res) => {
    const versionFile = `${rootDir}/cache/version`;
    return res.json(fs.existsSync(versionFile) ? fs.readFileSync(versionFile).toString('utf-8') : null);
}));

export default router;
