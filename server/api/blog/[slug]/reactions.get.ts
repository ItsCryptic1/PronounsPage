import { getPostReactions } from '~/server/blog.ts';
import useAuthentication from '~/server/utils/useAuthentication.ts';

export default defineEventHandler(async (event) => {
    const db = useDatabase();
    const { user } = await useAuthentication(event);
    const slug = getRouterParam(event, 'slug')!;
    return await getPostReactions(db, slug, user?.id);
});
