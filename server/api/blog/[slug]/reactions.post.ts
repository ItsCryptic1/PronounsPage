import SQL from 'sql-template-strings';
import { ulid } from 'ulid';

import { getPostReactions } from '~/server/blog.ts';
import useAuthentication from '~/server/utils/useAuthentication.ts';
import { reactionOptions } from '~/src/blog/reactions.ts';

export default defineEventHandler(async (event) => {
    const { user } = await useAuthentication(event);
    if (!user) {
        throw createError({
            status: 401,
            statusMessage: 'Unauthorised',
        });
    }

    const body = await readBody(event);
    const emoji = body.emoji;
    if (!reactionOptions.includes(emoji)) {
        throw createError({
            status: 400,
            statusMessage: 'Bad Request',
            message: 'Invalid emoji',
        });
    }
    const slug = getRouterParam(event, 'slug')!;
    const db = useDatabase();
    await db.get(SQL`DELETE FROM blog_reactions
       WHERE locale = ${global.config.locale} AND slug = ${slug} AND user_id = ${user.id} AND emoji = ${emoji}`);
    if (body.reaction) {
        await db.get(SQL`INSERT INTO blog_reactions (id, locale, slug, emoji, user_id)
            VALUES (${ulid()}, ${global.config.locale}, ${slug}, ${emoji}, ${user.id})`);
    }
    return await getPostReactions(db, slug, user.id);
});
