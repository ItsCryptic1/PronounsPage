import fs from 'node:fs/promises';

import markdownit from 'markdown-it';

import { rootDir } from '~/server/paths.ts';

const md = markdownit({ html: true });

export default defineEventHandler(async (event) => {
    const { isGranted } = await useAuthentication(event);
    if (!isGranted('panel')) {
        throw createError({
            status: 401,
            statusMessage: 'Unauthorised',
        });
    }

    const dir = `${rootDir}/moderation`;

    return {
        susRegexes: (await fs.readFile(`${dir}/sus.txt`, 'utf-8'))
            .split('\n')
            .filter((x) => !!x && !x.startsWith('#')),
        rulesUsers: md.render(await fs.readFile(`${dir}/rules-users.md`, 'utf-8')),
        rulesTerminology: md.render(await fs.readFile(`${dir}/rules-terminology.md`, 'utf-8')),
        rulesSources: md.render(await fs.readFile(`${dir}/rules-sources.md`, 'utf-8')),
        timesheets: md.render(await fs.readFile(`${dir}/timesheets.md`, 'utf-8')),
        expenses: md.render(await fs.readFile(`${dir}/expenses.md`, 'utf-8')),
        blog: md.render(await fs.readFile(`${dir}/blog.md`, 'utf-8')),
    };
});
