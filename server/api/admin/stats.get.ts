import { fetchStats } from '~/server/admin.ts';

export default defineEventHandler(async (event) => {
    const { isGranted } = await useAuthentication(event);
    if (!isGranted('panel')) {
        throw createError({
            status: 401,
            statusMessage: 'Unauthorised',
        });
    }

    const db = useDatabase();
    const stats = await fetchStats(db);
    if (stats === null) {
        throw createError({
            status: 404,
            statusMessage: 'Not Found',
        });
    }

    for (const locale of Object.keys(stats.locales)) {
        if (!isGranted('panel', locale)) {
            delete stats.locales[locale];
        }
    }

    return stats;
});
