import dotenv from 'dotenv';

import type { Config } from '../locale/config.ts';
import buildLocaleList from '../src/buildLocaleList.ts';
import { buildList } from '../src/helpers.ts';

import { loadSuml } from './loader.ts';
import { rootDir } from './paths.ts';

export default () => {
    dotenv.config({ path: `${rootDir}/.env` });
    if (process.env.__INCLUDE) {
        dotenv.config({ path: `${rootDir}/${process.env.__INCLUDE}` });
    }
    process.env.CLOUDFRONT = `https://${process.env.AWS_CLOUDFRONT_ID}.cloudfront.net`;

    const config = loadSuml('config') as Config;
    const locales = buildLocaleList(config.locale);

    const allLocalesUrls = buildList(function*() {
        if (process.env.NODE_ENV === 'development') {
            if (process.env.BASE_URL) {
                yield process.env.BASE_URL;
            }
            yield 'http://pronouns.test:3000';
            yield 'http://localhost:3000';
        } else if (process.env.ENV === 'test') {
            if (process.env.BASE_URL) {
                yield process.env.BASE_URL;
            }
        } else {
            yield 'https://pronouns.page';
            for (const localeDescription of Object.values(locales)) {
                yield localeDescription.url;
            }
        }
    });
    process.env.ALL_LOCALES_URLS = allLocalesUrls.join(',');
};
