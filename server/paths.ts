// Nitro is designed to be independent of the filesystem, but that requires migration in some express routes
// in dev mode, import.meta.url references `.nuxt/dev/index.mjs` and not the file path of this file
// in production mode, import.meta.url is not a directory, so by convention the working directory must be the repo root,
// however, `nuxi preview` automatically changes into .output
export const rootDir = process.cwd().replace(/\/\.output$/, '');
