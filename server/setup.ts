import * as Sentry from '@sentry/node';

import dotenv from './dotenv.ts';

dotenv();

Sentry.init({
    dsn: process.env.SENTRY_DSN,
    tracesSampleRate: 1.0,
    beforeSend(event, hint) {
        console.error(hint.originalException || hint.syntheticException);
        return event;
    },
});
