import fs from 'node:fs';

import Suml from 'suml';

import { rootDir } from './paths.ts';

export const loadSumlFromBase = (name: string): unknown => new Suml().parse(fs.readFileSync(`${rootDir}/${name}.suml`, 'utf-8'));
export const loadSuml = (name: string): unknown => loadSumlFromBase(`data/${name}`);
