import * as Sentry from '@sentry/node';
import { H3Error } from 'h3';
import { defineNitroPlugin, useRuntimeConfig } from 'nitropack/runtime';

import formatError from '../../src/error.ts';

export default defineNitroPlugin((nitroApp) => {
    const { public: { env, sentry } } = useRuntimeConfig();

    if (!sentry.dsn) {
        return;
    }

    Sentry.init({
        dsn: sentry.dsn,
        environment: env,
        tracesSampleRate: 0.1,
        attachStacktrace: true,
    });

    nitroApp.hooks.hook('error', (error, { event }) => {
        if (error instanceof H3Error) {
            if (error.statusCode === 404 || error.statusCode === 422) {
                return;
            }
        }

        Sentry.captureException(error);
        console.error(formatError(error, event?.node.req));
    });

    nitroApp.hooks.hookOnce('close', async () => {
        await Sentry.close(2000);
    });
});
