import crypto from 'crypto';
import type { KeyObject, BinaryLike } from 'crypto';
import fs from 'fs';

import { rootDir } from './paths.ts';

class Crypto {
    privateKey: KeyObject;
    publicKey: KeyObject;

    constructor(privateKey: string, publicKey: string) {
        this.privateKey = crypto.createPrivateKey(fs.readFileSync(privateKey));
        this.publicKey = crypto.createPublicKey(fs.readFileSync(publicKey));
    }

    sign(payload: BinaryLike): string {
        const sign = crypto.createSign('SHA256');
        sign.update(payload);
        return sign.sign(this.privateKey, 'hex');
    }

    validate(payload: BinaryLike, signature: string): boolean {
        const verify = crypto.createVerify('SHA256');
        verify.update(payload);
        return verify.verify(this.publicKey, signature, 'hex');
    }
}

export default new Crypto(`${rootDir}/keys/private.pem`, `${rootDir}/keys/public.pem`);
