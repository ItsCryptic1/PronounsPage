import './setup.ts';

import { DateTime } from 'luxon';
import { ulid } from 'ulid';

import { connect } from './audit.ts';

const execute = process.env.EXECUTE === '1';

const LIMIT_MONTHS = 18;

console.log(execute ? 'WILL REMOVE ENTRIES!' : 'Dry run');

async function cleanup(): Promise<void> {
    const cutoff = DateTime.local().minus({ months: LIMIT_MONTHS });
    const cutoffId = ulid(cutoff.toMillis());

    console.log(`Removing entries older than ${cutoff.toISO()} (${cutoffId})`);

    const db = await connect();

    const count = (await db.get<{ c: number }>(
        `SELECT COUNT(*) as c FROM audit_log WHERE id < ?`,
        [cutoffId],
    ))!.c;

    console.log(`Found ${count} entries to remove`);

    if (execute) {
        await db.run(`DELETE FROM audit_log WHERE id < ?`, [cutoffId]);
    }
}

cleanup();
