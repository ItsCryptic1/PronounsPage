import * as sqlite from 'sqlite';
import sqlite3 from 'sqlite3';

import { rootDir } from './paths.ts';

export type SQLQuery = sqlite.ISqlite.SqlType;
export interface Database {
    get<T = unknown>(sql: SQLQuery, ...args: unknown[]): Promise<T | undefined>;
    each<T = unknown>(sql: SQLQuery, callback: (err: unknown, row: T) => void): Promise<number>;
    all<T = unknown>(sql: SQLQuery, ...args: unknown[]): Promise<T[]>;
    close(): Promise<void>;
}

export default (): Promise<sqlite.Database> => sqlite.open({
    filename: `${rootDir}/db.sqlite`,
    driver: sqlite3.Database,
});
