import fs from 'fs';

import { registerFont } from 'canvas';

import { rootDir } from './paths.ts';

const vars: Record<string, string> = {};

for (const [, name, value] of fs.readFileSync(`${rootDir}/data/variables.scss`).toString('utf-8')
    .matchAll(/^\$([^:]+): '([^']+)'(?:, '[^']+')*;$/gm)) {
    vars[name] = value;
}

const fontSources: Record<string, string> = {
    'Baloo Bhaijaan 2': 'baloo-bhaijaan-2-v19-arabic_latin-{weight}.ttf',
    'Nanum Gothic': 'nanum-gothic-v17-latin_korean-{weight}.ttf',
    'Nunito': 'nunito-v16-latin-ext_latin_cyrillic-ext_cyrillic-{weight}.ttf',
    'Quicksand': 'quicksand-v21-latin-ext_latin-{weight}.ttf',
    'Zen Maru Gothic': 'zen-maru-gothic-v4-latin_japanese-{weight}.ttf',
};

const weightsValues: Record<string, string> = {
    bold: '700',
};

export const registerLocaleFont = (v: string, weights: string[] = ['regular']): string => {
    const family = vars[v];
    const source = fontSources[family];
    for (const weight of weights) {
        registerFont(`public/fonts/${source.replace('{weight}', weightsValues[weight] || weight)}`, { family, weight });
    }
    return family;
};
