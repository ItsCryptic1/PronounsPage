// load this module before other modules which require these global variables
import type { Config } from '../locale/config.ts';
import type { Translations } from '../locale/translations.ts';

import dotenv from './dotenv.ts';
import { loadSuml } from './loader.ts';

dotenv();

export const config = loadSuml('config') as Config;
global.config = config;
global.translations = loadSuml('translations') as Translations;
