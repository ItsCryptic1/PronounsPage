import './setup.ts';

import util from 'util';

import buildLocaleList from '../src/buildLocaleList.ts';
import { calculateStats } from '../src/stats.ts';

import dbConnection from './db.ts';

const __dirname = new URL('.', import.meta.url).pathname;

async function calculate(): Promise<void> {
    const db = await dbConnection();
    const stats = await calculateStats(db, buildLocaleList(null, true), `${__dirname}/..`);
    await db.close();

    console.log(util.inspect(stats, { showHidden: false, depth: null, colors: true }));
}

await calculate();
