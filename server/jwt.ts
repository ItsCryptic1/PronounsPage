import type { PathLike } from 'node:fs';
import fs from 'node:fs/promises';

import * as Sentry from '@sentry/node';
import { importPKCS8, importSPKI, jwtVerify, SignJWT } from 'jose';
import type { KeyLike, JWTPayload } from 'jose';

import { rootDir } from './paths.ts';

class Jwt {
    constructor(private privateKey: KeyLike, private publicKey: KeyLike) {}

    static async from(privateKeyPath: PathLike, publicKeyPath: PathLike) {
        const privateKeyPromise = fs.readFile(privateKeyPath, 'utf-8')
            .then((privateKeyContent) => importPKCS8(privateKeyContent, 'RS256'));
        const publicKeyPromise = fs.readFile(publicKeyPath, 'utf-8')
            .then((publicKeyContent) => importSPKI(publicKeyContent, 'RS256'));
        const [privateKey, publicKey] = await Promise.all([privateKeyPromise, publicKeyPromise]);
        return new Jwt(privateKey, publicKey);
    }

    async sign(payload: JWTPayload, expiresIn = '365d'): Promise<string> {
        return await new SignJWT(payload)
            .setProtectedHeader({ alg: 'RS256' })
            .setExpirationTime(expiresIn)
            .setAudience(process.env.ALL_LOCALES_URLS!.split(','))
            .setIssuer(process.env.BASE_URL!)
            .sign(this.privateKey);
    }

    async validate<PayloadType = JWTPayload>(token: string): Promise<PayloadType | undefined> {
        try {
            const { payload } = await jwtVerify<PayloadType>(token, this.publicKey, {
                algorithms: ['RS256'],
                audience: process.env.ALL_LOCALES_URLS!.split(','),
                issuer: process.env.ALL_LOCALES_URLS!.split(','),
            });
            return payload;
        } catch (error) {
            Sentry.captureException(error);
        }
    }
}

export default await Jwt.from(`${rootDir}/keys/private.pem`, `${rootDir}/keys/public.pem`);
