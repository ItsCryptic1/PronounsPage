import type { RouteLocationRaw } from 'vue-router';

import type { Day } from '~/src/calendar/helpers.ts';

export default (day: Day): RouteLocationRaw => ({
    name: 'calendarDay',
    params: { year: day.year, month: day.month.toString().padStart(2, '0'), day: day.day.toString().padStart(2, '0') },
});
