import type { Config } from '~/locale/config.ts';

export default (config: Config, paths: string[]): string[] => {
    const prefixes = [config.pronouns.prefix || '', ...config.pronouns.sentence?.prefixes || []];
    return paths.flatMap((path) => {
        return prefixes.map((prefix) => `${prefix}${path}`);
    });
};
