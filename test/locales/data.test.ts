import type { SyncExpectationResult } from '@vitest/expect';
import { describe, expect, test } from 'vitest';

import type { Config } from '../../locale/config.ts';
import type { NounTemplatesData, PronounGroupData, PronounExamplesData, PronounData } from '../../locale/data.ts';
import allLocales from '../../locale/locales.ts';
import { loadSumlFromBase } from '../../server/loader.ts';
import { Example, gendersWithNumerus } from '../../src/classes.ts';
import { loadTsv } from '../../src/tsv.ts';

import { normaliseKey } from '~/src/buildPronoun.ts';

const __dirname = new URL('.', import.meta.url).pathname;

function toHaveValidMorphemes(actual: string, morphemes: string[]): SyncExpectationResult {
    const containedMorphemes = Example.parse(actual).filter((part) => part.variable)
        .map((part) => part.str.replace(/^'/, ''));
    const unknownMorphemes = containedMorphemes.filter((morpheme) => !morphemes.includes(morpheme));
    if (unknownMorphemes.length > 0) {
        return {
            message: () => `expected example '${actual}' to have valid morphemes,` +
                ` but these are unknown:\n${unknownMorphemes.join(', ')}`,
            pass: false,
        };
    } else {
        return {
            message: () => 'expected example to have invalid morphemes',
            pass: true,
        };
    }
}

interface CustomMatchers<R> {
    toHaveValidMorphemes(morphemes: string[]): R;
}

declare module 'vitest' {
    interface Assertion<T> extends CustomMatchers<T> {}
}

expect.extend({ toHaveValidMorphemes });

describe.each(allLocales)('data files of $code', ({ code }) => {
    const config = loadSumlFromBase(`locale/${code}/config`) as Config;

    const pronouns = loadTsv<PronounData<string>>(`${__dirname}/../../locale/${code}/pronouns/pronouns.tsv`);
    const pronounGroups = loadTsv<PronounGroupData>(`${__dirname}/../../locale/${code}/pronouns/pronounGroups.tsv`);
    const examples = loadTsv<PronounExamplesData>(`${__dirname}/../../locale/${code}/pronouns/examples.tsv`);

    const nounTemplates = loadTsv<NounTemplatesData>(`${__dirname}/../../locale/${code}/nouns/nounTemplates.tsv`);

    test('pronouns/pronouns.tsv match schema', async () => {
        const { default: MORPHEMES } = await import(`../../locale/${code}/pronouns/morphemes.ts`);
        if (pronouns.length === 0) {
            return;
        }
        const required = [
            'key',
            'description',
            'normative',
            'plural',
            'pluralHonorific',
            'pronounceable',
            ...MORPHEMES,
        ];
        const optional = ['history', 'thirdForm', 'smallForm', 'sourcesInfo', 'hidden'];
        const actual = Object.keys(pronouns[0]);
        expect(actual).toEqual(expect.arrayContaining(required));
        expect([...required, ...optional]).toEqual(expect.arrayContaining(actual));
    });
    test('pronouns/pronouns.tsv have unique keys', () => {
        const keys = new Set();
        for (const pronoun of pronouns) {
            const pronounKeys = pronoun.key
                .replace(/،/g, ',')
                .split(',')
                .map((key) => normaliseKey(key));
            for (const key of pronounKeys) {
                expect(keys).not.toContain(key);
                keys.add(key);
            }
        }
    });
    test('pronouns/pronounGroups.tsv match schema', () => {
        if (pronounGroups.length === 0) {
            return;
        }
        const required = ['name', 'pronouns'];
        const optional = ['key', 'description', 'hidden'];
        const actual = Object.keys(pronounGroups[0]);
        expect(actual).toEqual(expect.arrayContaining(required));
        expect([...required, ...optional]).toEqual(expect.arrayContaining(actual));
    });
    test('pronouns/pronounGroups.tsv reference pronouns by canonical name', () => {
        const knownPronounNames = pronouns.map((pronoun) => {
            return pronoun.key.replace(/،/g, ',').split(',')[0];
        });
        for (const pronounGroup of pronounGroups) {
            const pronounNames = pronounGroup.pronouns?.replace(/،/g, ',').split(',') ?? [];
            for (const pronounName of pronounNames) {
                expect(knownPronounNames).toContain(pronounName);
            }
        }
    });
    test('pronouns/examples.tsv match schema', () => {
        if (examples.length === 0) {
            return;
        }
        const required = [
            'singular',
        ];
        if (config.pronouns.plurals) {
            required.push('plural');
        }
        if (config.pronouns.honorifics) {
            required.push('isHonorific');
        }
        const actual = Object.keys(examples[0]);
        expect(actual).toEqual(required);
    });
    test('pronouns/examples.tsv contain valid morphemes', async () => {
        const { default: MORPHEMES } = await import(`../../locale/${code}/pronouns/morphemes.ts`);
        for (const example of examples) {
            expect(example.singular).toHaveValidMorphemes(MORPHEMES);
            if (example.plural) {
                expect(example.plural).toHaveValidMorphemes(MORPHEMES);
            }
        }
    });
    test('pronouns/examples.tsv contains plural examples when language has plurals', () => {
        const hasExamplesWithPlurals = examples.some((example) => {
            return example.plural && example.plural !== example.singular;
        });
        expect(hasExamplesWithPlurals).toBe(!!config.pronouns.plurals);
    });
    test('pronouns/examples.tsv contains honorific examples when language has honorifics', () => {
        const hasExamplesWithHonorifics = examples.some((example) => example.isHonorific);
        expect(hasExamplesWithHonorifics).toBe(!!config.pronouns.honorifics);
    });

    test('nouns/nounTemplates.tsv match schema', () => {
        if (nounTemplates.length === 0) {
            return;
        }
        const required = ['masc', 'fem', 'neutr', 'mascPl', 'femPl', 'neutrPl'];
        const actual = Object.keys(nounTemplates[0]);
        expect(actual).toEqual(required);
    });
    test('nouns/nounTemplates.tsv contains templates when templates are enabled', () => {
        expect(nounTemplates.length > 0).toBe(config.nouns.templates?.enabled);
    });
    test('nouns/nounTemplates.tsv have exactly one hyphen as placeholder for root', () => {
        for (const template of nounTemplates) {
            for (const genderWithNumerus of gendersWithNumerus) {
                expect(template[genderWithNumerus]).toMatch(/^(?:[^-]*-[^-]*(?:\/|$))+/);
            }
        }
    });
});
