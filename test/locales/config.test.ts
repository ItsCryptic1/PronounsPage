import { describe, expect, test } from 'vitest';

import type { Config } from '~/locale/config.ts';
import allLocales from '~/locale/locales.ts';
import { loadSumlFromBase } from '~/server/loader.ts';

describe.each(allLocales)('config for $code', async ({ code }) => {
    const config = loadSumlFromBase(`locale/${code}/config`) as Config;

    const { default: morphemes } = await import(`../../locale/${code}/pronouns/morphemes.ts`);

    test('pronouns.null.morphemes contain valid morphemes', () => {
        if (!config.pronouns.null || config.pronouns.null.morphemes === undefined) {
            return;
        }
        expect(morphemes).toEqual(expect.arrayContaining(Object.keys(config.pronouns.null.morphemes)));
    });
    test('pronouns.null.emoji contain valid morphemes', () => {
        if (!config.pronouns.emoji) {
            return;
        }
        expect(morphemes).toEqual(expect.arrayContaining(Object.keys(config.pronouns.emoji.morphemes)));
    });
});
