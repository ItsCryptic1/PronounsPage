import fs from 'fs';

import { expect, test } from 'vitest';

import { buildFlags, flags } from '../src/flags.ts';

const __dirname = new URL('.', import.meta.url).pathname;

test('flags correspond to static image files', () => {
    const expectedFlagNames = fs.readdirSync(`${__dirname}/../public/flags/`)
        .filter((flagName) => {
            return !flagName.startsWith('.') && !flagName.startsWith('_');
        })
        .map((flagName) => flagName.replace(/\.png$/, ''));
    expect(Object.keys(flags)).toEqual(expectedFlagNames);
});

test('building flags removes locale foreign flags', () => {
    const flags = buildFlags('en');
    expect(Object.keys(flags)).toContain('-en-Genderdoe');
    expect(Object.keys(flags)).not.toContain('-Drag');
    expect(Object.keys(flags)).not.toContain('-pl-Dukaizmy');
});
