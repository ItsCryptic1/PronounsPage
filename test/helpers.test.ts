import { describe, expect, test } from 'vitest';

import {
    escapeControlSymbols,
    unescapeControlSymbols,
    convertPronunciationStringToSsml,
    escapePronunciationString,
} from '../src/helpers.ts';

const controlSymbols = [
    {
        description: 'slashes',
        unescaped: 'w/o n/A',
        escaped: 'w`/o n`/A',
    },
    {
        description: 'ampersands',
        unescaped: 'me & you',
        escaped: 'me `& you',
    },
    {
        description: 'backticks',
        unescaped: '`code` in markdown',
        escaped: '``code`` in markdown',
    },
    {
        description: 'commas',
        unescaped: 'me, you',
        escaped: 'me`, you',
    },
    {
        description: 'pipes',
        unescaped: '0 | 1',
        escaped: '0 `| 1',
    },
];

describe('when escaping control symbols', () => {
    test('safely handles null', () => {
        expect(escapeControlSymbols(null)).toBeNull();
    });
    test.each(controlSymbols)('$description get escaped with `', ({ unescaped, escaped }) => {
        expect(escapeControlSymbols(unescaped)).toBe(escaped);
    });
});

describe('when unescaping control symbols', () => {
    test('safely handles null', () => {
        expect(unescapeControlSymbols(null)).toBeNull();
    });
    test.each(controlSymbols)('$description get unescaped', ({ unescaped, escaped }) => {
        expect(unescapeControlSymbols(escaped)).toBe(unescaped);
    });
});

describe('when escaping pronunciation', () => {
    test.each([
        {
            description: 'slashes get escaped',
            text: 'w/o n/A',
            pronunciationString: String.raw`w\/o n\/A`,
        },
        {
            description: 'backslashes get escaped',
            text: String.raw`\n is the symbol for a newline, \t for a tab`,
            pronunciationString: String.raw`\\n is the symbol for a newline, \\t for a tab`,
        },
    ])('$description', ({ text, pronunciationString }) => {
        expect(escapePronunciationString(text)).toBe(pronunciationString);
    });
});

describe('when converting pronunciation', () => {
    test.each([
        {
            description: 'simple text is passed as-is',
            pronunciationString: 'text',
            ssml: '<speak>text</speak>',
        },
        {
            description: 'slashes describe IPA phonemes',
            pronunciationString: '/ðeɪ/',
            ssml: '<speak><phoneme alphabet="ipa" ph="ðeɪ"></phoneme></speak>',
        },
        {
            description: 'simple text and slashes can be combined',
            pronunciationString: '/ðeɪ/ are',
            ssml: '<speak><phoneme alphabet="ipa" ph="ðeɪ"></phoneme> are</speak>',
        },
        {
            description: 'slashes can be escaped at front',
            pronunciationString: String.raw`w\/o, n/A`,
            ssml: '<speak>w/o, n/A</speak>',
        },
        {
            description: 'slashes can be escaped at back',
            pronunciationString: String.raw`w/o, n\/A`,
            ssml: '<speak>w/o, n/A</speak>',
        },
        {
            description: 'provided HTML is escaped',
            pronunciationString: '<break time="1s"/>',
            ssml: '<speak>&lt;break time=&quot;1s&quot;/&gt;</speak>',
        },
    ])('$description', ({ pronunciationString, ssml }) => {
        expect(convertPronunciationStringToSsml(pronunciationString)).toBe(ssml);
    });
});
