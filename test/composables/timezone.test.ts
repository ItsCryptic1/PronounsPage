// @vitest-environment jsdom

import { DateTime } from 'luxon';
import { beforeAll, describe, expect, test, vi } from 'vitest';

import useTimezone from '../../composables/useTimezone.ts';

describe('timezone info', () => {
    const { getTimezoneInfo } = useTimezone();

    beforeAll(() => {
        vi.spyOn(DateTime, 'local').mockReturnValue(DateTime.fromISO('2024-01-01T10:00:00'));
    });

    test('for known timezone displays offset', () => {
        const timezoneInfo = getTimezoneInfo('America/New_York');
        expect(timezoneInfo).toEqual({
            area: 'America',
            displayLocation: 'New York',
            icon: 'globe-americas',
            location: 'New York',
            long: 'Eastern Standard Time',
            offset: -300,
            offsetFormatted: '-05:00',
            short: 'EST',
            timezone: 'America/New_York',
        });
    });
    test('for known wrongly named timezone uses correct name', () => {
        const timezoneInfo = getTimezoneInfo('Europe/Kiev');
        expect(timezoneInfo).toEqual({
            area: 'Europe',
            displayLocation: 'Kyiv',
            icon: 'globe-europe',
            location: 'Kiev',
            long: 'Eastern European Standard Time',
            offset: 120,
            offsetFormatted: '+02:00',
            short: 'GMT+2',
            timezone: 'Europe/Kiev',
        });
    });
    test('for unknown timezone returns null', () => {
        const timezoneInfo = getTimezoneInfo('Etc/Unknown');
        expect(timezoneInfo).toBeNull();
    });
});
