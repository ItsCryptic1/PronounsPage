include .env.dist
-include .env

KEYS_DIR=./keys

# make targets phony because some commands match with a directory
.PHONY: install test run start stop deploy switch migrate

install:
	-cp -n .env.dist .env
	if [ ! -d "${KEYS_DIR}" ]; then mkdir -p ${KEYS_DIR}; openssl genrsa  -out ${KEYS_DIR}/private.pem 2048; openssl rsa -in ${KEYS_DIR}/private.pem -outform PEM -pubout -out ${KEYS_DIR}/public.pem; fi
	mkdir -p moderation
	touch moderation/sus.txt moderation/rules-users.md moderation/rules-terminology.md moderation/rules-sources.md moderation/timesheets.md moderation/expenses.md moderation/blog.md
	pnpm install
	pnpm run-file server/migrate.ts

lint:
	pnpm lint

test:
	pnpm test

run:
	pnpm dev

start:
	node_modules/.bin/avris-daemonise start webserver pnpm dev

stop:
	node_modules/.bin/avris-daemonise stop webserver

deploy: install
	mkdir -p ./cache
	git log -n 1 --pretty=format:"%H" > ./cache/version
	pnpm build
	pnpm run-file server/migrate.ts
	-pnpm run-file server/sentry.ts

switch:
	rm -rf cache
	rm -rf .nuxt/cache
	ln -sfn ./locale/${LANG} ./data
	ln -sfn ../locale/${LANG}/img ./public/img-local
	ln -sfn ../locale/${LANG}/docs ./public/docs-local
	if [ -d ./locale/${LANG}/img/logo ]; then echo ""; else ln -s ../../_/img/logo ./locale/${LANG}/img/logo; fi

migrate:
	pnpm run-file server/migrate.ts
