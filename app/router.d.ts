import type { RouteRecordRaw } from 'vue-router';

declare global {
    var originalRoutes: readonly RouteRecordRaw[];
}
