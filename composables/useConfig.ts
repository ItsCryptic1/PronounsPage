import config from '../data/config.suml';
import type { Config } from '../locale/config.ts';

export default (): Config => {
    return config;
};
