-- Up

create table translations_dg_tmp
(
    id        TEXT    not null
        primary key,
    locale    TEXT    not null,
    tKey      TEXT    not null,
    tValue    TEXT    not null,
    status    INTEGER not null,
    author_id TEXT
                      references users
                          on delete set null
);

insert into translations_dg_tmp(id, locale, tKey, tValue, status, author_id)
select id, locale, tKey, tValue, status, author_id
from translations;

drop table translations;

alter table translations_dg_tmp
    rename to translations;

create index translations_locale
    on translations (locale);

create index translations_status
    on translations (status);

-- Down
