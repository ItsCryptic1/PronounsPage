-- Up

ALTER TABLE audit_log ADD COLUMN aboutUserId TEXT NULL DEFAULT NULL;

UPDATE audit_log
SET aboutUserId = json_extract(payload, '$.userId')
WHERE payload like '%userId%';

CREATE INDEX "audit_log_aboutUserId" ON "audit_log" ("aboutUserId");

-- Down
