-- Up

ALTER TABLE links ADD COLUMN faviconCache TEXT NULL DEFAULT NULL;
UPDATE links SET expiresAt = null WHERE 1=1;

-- Down
