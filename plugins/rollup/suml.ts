import fs from 'node:fs';

import type { TransformResult, Plugin } from 'rollup';
import Suml from 'suml';

const replacer = (key: string, value: unknown): unknown => {
    if (value === Infinity) {
        return '<<<{{{Infinity}}}>>>';
    }
    if (value === -Infinity) {
        return '<<<{{{-Infinity}}}>>>';
    }
    if (value instanceof Date) {
        return `<<<{{{new Date('${value.toISOString()}')}}}>>>`;
    }
    return value;
};

export default (): Plugin => {
    return {
        name: 'transform-suml',
        async transform(code, id): Promise<TransformResult> {
            if (/\.suml$/.test(id)) {
                const suml = new Suml();
                const source = await fs.promises.readFile(id, 'utf8');
                return {
                    code: `export default ${JSON.stringify(suml.parse(source), replacer)}`.replace(/"<<<{{{([^}]*)}}}>>>"/g, '$1'),
                    map: { mappings: '' },
                };
            }
            return null;
        },
    };
};
