import fs from 'node:fs';

import Papa from 'papaparse';
import type { TransformResult, Plugin } from 'rollup';

import { tsvParseConfig } from '../../src/tsv.ts';

export default (): Plugin => {
    return {
        name: 'transform-tsv',
        async transform(code, id): Promise<TransformResult> {
            if (/\.tsv$/.test(id)) {
                const content = Papa.parse(await fs.promises.readFile(id, 'utf-8'), tsvParseConfig).data;
                return {
                    code: `export default ${JSON.stringify(content)}`,
                    map: { mappings: '' },
                };
            }
            return null;
        },
    };
};
