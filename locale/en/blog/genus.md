# We could use a better word for “grammatical gender”

<small>2024-10-26 | [@andrea](/@andrea)</small>

Many languages have “genders” – a system of separating nouns into classes. Those classes then follow different declension rules
or are used with different articles. Sometimes it has nothing to do with human genders,
like with Dutch “de” and “het”, where the contrast is common–neuter, or Proto-Indo-European where the contrast was animate–inanimate.
But often the distinction is masculine-feminine or masculine-feminine-neuter – which has a big overlap with human genders,
might be arbitrary when it comes to inanimate nouns.
For instance, in German many nouns describing gender are congruent with their genus, e.g. “die Frau” and “der Mann” (“woman” and “man”), 
but for most objects, the assignment to a grammatical category is more or less arbitrary, e.g.
“der Schlüssel” (“the key” – masc.), “die Tür” (“the door” – fem.), “das Auto” (“the car” – neuter).

So with that in mind… When writing articles in English, I often find myself having to quantify the word “gender”
with “grammatical” or to rephrase a sentence in order to make it clear which concept I'm talking about – 
human gender or grammatical one (and in a project about both language and queer stuff – that's often unclear).
Meanwhile, my native Polish has two separate words for them: “płeć” and “rodzaj gramatyczny” – making things so much easier.

Why not create a similar distinction in English? Have separate words for separate concepts.
How about… just using the Latin word for it? **“Genus”**, which means “a kind”.
It's already being used in English in [biological taxonomy](https://en.wikipedia.org/wiki/Genus),
so it shouldn't sound weird, while its usage in a very different context shouldn't make things confusing.

For example:

 - “Polish enbies are reclaiming the neuter genus to describe nonbinary genders”
 - “Gender ≠ genus. You can still use masculine or feminine forms, if you're nonbinary”

What do y'all think? 😉