# Does Gender Exist? Do We Want to Abolish It?

<small>2024-06-10 | [@andrea](/@andrea)</small>

![A picture of a person wearing a face mask and sunglasses, participating in a protest, and holding a sign that says: My favourite season is the FALL of the patriarchy](/img-local/blog/fall-of-the-patriarchy.png)
<p style="margin-top: -.75rem"><small class="text-muted">Photo by <a href="https://unsplash.com/fr/@gmalhotra?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Gayatri Malhotra</a> on
<a href="https://unsplash.com/photos/ft3ndjrS2TI?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></small></p>

While browsing the responses in the [2023 Polish Nonbinary Census](https://zaimki.pl/spis), I came across one where the respondent refused to
answer most questions, adding notes like “gender doesn’t exist”, “gender doesn’t matter”, “we don’t need pronouns”, “you can’t escape
labels by adding more labels”, “be yourself”, “abandon the concept of gender”, “don’t let yourself be pigeonholed”, etc. This is not a
lone voice (though, admittedly, very rare) – I once discussed similar issues with an agender person who is currently working on one of
the upcoming language versions of Pronouns.page; and I myself am agender and have delved into similar reflections many times.

For our [zine “Poczytałosie”](https://zaimki.pl/zin), I wrote a piece ([“Analogies”](https://avris.it/texts/analogie)), in which I consider this theme
– how from the perspective of a person who doesn’t feel a connection to any gender and understands it more in intellectual terms than
a personal experience, dividing people into categories more or less related to their genitals seems as absurd as if we treated each other
differently based on our blood type or hair color. It’s hard for me to see gender as anything other than an arbitrary and harmful
division.

But at the same time, I understand people for whom their gender identity and the ways of expressing it are extremely important. Not
just transgender and nonbinary people, but also cisgender people. After all, when a cis man lowers his voice to sound more masculine,
or when a cis woman puts on makeup to emphasize her femininity – that’s also _gender performance_. Heck, I myself, even though I don’t
consider things like facial hair, makeup, or clothing as inherently appropriate for only one gender, when I paint my nails or wear a
dress, it’s partly just to feel good in my skin and feel that I look great – but undeniably, I often do it also to signal to the world:
I am not a man, stop seeing a man in me.

Why are gender identity and expression so important to us? Because, even if we see the matter differently as individuals, we live in a
society where gender _is_ important. Even if it is “made up”.

Gender is a **social construct**. There is no immutable law of physics
or biology that makes pink girly and blue boyish, that half of society must be referred to as “she” and the other half as “he”, that part of society
must be frowned upon for wearing makeup, and the other part for _not_ wearing it, etc., etc., etc. All these things are just conventions!
The society in France agrees on different norms than in Japan, people in antiquity observed different norms than we do now.

This does not mean, however, that gender does not exist. A good analogy might be the law – yes, the law is a social construct, an
agreement, a convention, nothing tangible, objective, or immutable. But if you break it, you _will_ feel the consequences.

Sure, it’s nice to speculate about how wonderful it would be to live in a world where gender doesn’t matter – where you can be whoever
you want, where no one looks at you through the lens of gender, judges, makes assumptions, imposes, forbids… where your gender has as
little impact on your life as your eye color. I would love to live in such a world and I bet a large part of nonbinary people shares a
similar vision. I don’t know if it could be called “abolishing” or “overthrowing” gender – since gender would still exist, it just
wouldn’t be a social compulsion. Maybe rather “abolishing gender roles”?

But call it what you will. It seems that whether we collect queer labels and experiment with our gender expression, or completely
reject the concept of socio-cultural gender as absurd, we have the same goal – a world free of patriarchal constraints. The question
remains, how to achieve it. Pretend that it already is the way we would like it to be? Live as if gender didn’t matter until it finally doesn’t?
The pragmatic part of my mind screams that this is not the way.

Using pronouns can be a good example… I fully understand people who use [_any pronouns_](/any), basically saying “call me whatever you want”.
I  usually don’t feel dysphoria just because someone addresses me using [female](/she), [male](/he), [neutral](/they), or [neological](/xe)
forms. But I also know very well that if I told the world “my pronouns don’t matter, call me whatever you want” – 99% of people would
use male forms. Their mistaken assumption that I am a man will not be challenged. Language will reinforce the pattern. Although
_individually_ pronouns don’t make much difference to me, _collectively_ I prefer to choose (and expect respect for) the forms that
strike the patriarchal system.

I approach the queer labels in a similar way. My identity (gender, sexual, romantic, relational) is complicated, ambiguous,
fluid, and constantly rediscovered. I don’t want to be pigeonholed, I’ve never been comfortable in any box. But I still gladly use
labels and flags – because I know they are our collective effort to understand and describe reality; that they are a tool by which we
tell each other: “I feel the same way”, “being like you is okay”. It’s easy to tell someone “just be yourself” – but it’s hard to know
who you really are, if you have no point of reference, if you don’t know what is even possible.

Pretending that gender doesn’t exist can also be dangerous. Violence often has a gender – should we also ignore, for example, that
restricting reproductive rights is tied to how gender roles are perceived? Denying trans people the right to gender identity using the
excuse “I don’t believe in gender” could be compared to advocating for the abolition of borders: it’s a wonderful, utopian vision of
the world, and it’s nice to strive for it – but it requires systemic changes, not empty declarations and attacking the most vulnerable.
If, for example, you deny immigrants citizenship “because I don’t believe in borders”, you are not fixing the system, but under the
pretext of disagreement with the system, you are actually reinforcing it.

I'm not saying any of this to attack [people who don’t use labels](/terminology#unlabeled) or who use [any pronouns](/any) –
there’s absolutely nothing wrong with that! Everyone has their reasons, their thought process, and their preferences. However, I must
admit that it hurts to read demands like “stop seeing gender!” while the whole world constantly sees gender in me and imposes
arbitrary behavioral norms based on it. I don’t understand why someone tells me “just be yourself”, but at the same time tells me to
abandon all the vocabulary that helps me name (even if it's not super precise) who I am…

Pronouns.page exists to help solve specific problems created by patriarchy. Yes, I would love a world where there is _no need_ to create
such resources, where there is _no need_ to introduce yourself with pronouns, to think about your gender or look for labels.
But as long as the oppressive patriarchal system imposes gender roles on us, surrendering and pretending that
“gender doesn’t exist” does not help us overthrow it in any way.
