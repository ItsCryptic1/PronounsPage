# Avant-Drag! – “Greece shall die so that we can live!”

<small>2024-05-11 | [@T_Vos](/@T_Vos), [@andrea](/@andrea)</small>

![Title sequence of the movie: text “Avant-Drag! – Radical performers re-imagine Athens” on a sparkly background](/img-local/blog/avant-drag/avant-drag.png)

<div class="alert alert-warning">
    <span class="fal fa-exclamation-triangle"></span>
    <strong>Content warnings for the movie:</strong>
    flashing lights, blood, self-harm, abuse, queerphobia, xenophobia,
    hate, murder, cops, medical issues, drugs.
</div>

It’s a rough world out there, especially for the queers. Luckily for us, there are always part of our community ready to fight and able to overcome fear.

We had the pleasure to talk with [Thanasis McMorait](https://www.instagram.com/mcmorait), one of the performers portrayed in the documentary Avant-Drag!.
They are a brave political queen from Thessaloniki, famous for their shows with heavy punk-anarchist commentary. 
We've learned a lot and will share their thoughts intertwined within this review.

Avant-Drag! is a Greek production that showcases ten underground drag artists from Greece with all the beautiful and the ugly, no filter.
In the movie we get to know them through varying expressions and stories, their actions, political statements;
they are ready to be punk and to demand from the society their worth in gold.
Each distinctively unique, yet going in one direction: the future. The future that is queer.
The future for which a battle is being fought in the streets, on social media, in the buildings of parliaments all around the world.
A battle fought by, as McMorait has nicely put it, “a lot for queer people, who managed to be proud of who they are now,
and not who their grand grand grand grandmother _could have been_ potentially”.

In the movie we can see the underground scene of Greek drag. The one that is often unheard of internationally,
less accessed by tourists, less Instagram-worthy, less algorithm friendly, more brave, messy, silly, imperfect, human, not doll.
We learn of a community that's rough and quirky at first, but one that supports each other. 
There is competition for gigs and opportunities, but there is also sisterhood. McMorait talks about their community as warm, welcoming –
this allows new performers to improve their acts and gives a sense of belonging and motivation to be better.

But all is not roses.

Avant-Drag! touches on many problems affecting the community that are deeply rooted in traditions and old ways – 
something that is endemic for many, many countries in Europe and all around the world. In Poland, how often we hear of old archetypes?
Of hussars and kings, of heroes of world wars and of the independence struggle?
We cling to the old times in search for glory, and we don’t recognize that new times call for new archetypes.

![](/img-local/blog/avant-drag/avant-drag-2-er-libido.png)

In the movie we hear how Greek patriotism and patriarchy are upholding mechanisms of oppression,
how ancient glory is being used to radicalize people today. The heroines of the movie take this old radicalism and put a mirror to its face.
They are not afraid to put on traditionally religious elements together with sexual symbolism and go out on the streets to praise abortion.
Why? To make you think. To question the authority that is deemed as unquestionable.
They are brave enough to go out and scream to no audience about social justice, daring to take aim at conservatism, patriarchy,
racism, and sexism, mock xenophobia and sometimes simply says the quiet part out loud.
We see their acts being performed in the streets, them claiming their space and being proud of who they are. Authentically.

Watching Avant-Drag! reminded me of my time and reality in Poland a few years ago.
In Szczecin, I remember going to a memorial of a killed gay from my community,
I saw the suffering and the struggles of my community, the hopelessness that accompanied it all.
I remember fearing the vast queer world out there. Not because it’s scary or wrong, but because if such a small deviation from the societal
cishet-normative narrative as being gay was enough to get killed, how to tap to even wider, often less accepted,
queer world full of beautiful and unique identities? And then I met local drag queens, misfits who were trying to make a change –
my local equivalent of Avant-Drag!'s cast. Clumsy in their steps, with intrinsic motivation to live free,
getting stumped on and stopped by external factors, they also took to the streets, sometimes alone. On the same fight for survival.

Here I need to mention name Zak Kostopoulos – Zackie Oh – a Greek drag queen murdered on an Athens street in 2018.
In the movie McMorait is using a lot of their platform to tell a story of grief and pain and loss.
6 years later, court cases are still happening. 4 years after murder two civilians involved got their sentences – but not the four cops.

Avant-Drag! shows us that queer struggles are universal in the European civilization.
That no matter if an Orthodox, a Catholic, or a Protestant country, we all deal with the same religion that doesn’t want us.
That can’t compartmentalize ourselves in little pastures – because we’re plenty and uniquely beautiful in our authentic selves.
No matter, Poland or Greece or the Dutch Bible Belt, we deal with the same indifference from moral figures and authorities.
In our streets we scream and protest for recognition of our rights. We have different backgrounds, but our struggle unites us.

![](/img-local/blog/avant-drag/avant-drag-5-mcmorait-murder-site.png)

It's a struggle the creators of this documentary face first-hand.
McMorait told us about a very warm welcome that the movie received from the queer community,
but also about protests happening before its screening at the Thessaloniki Documentary Festival,
the fear of repercussions (especially due to a scene where Aurora spray-paints an Albanian flag over the Greek one),
and the lack of allyship from the festival's organizers.
McMorait is now looking for happiness and safety living outside of Greece – and we wish them all the best in the Netherlands!
Yet we've never gotten an impression that they regret telling their story in the movie, quite the opposite.
The truth and reality can be frightening, not many have courage to tell bitter stories of life.

Before we signed off, after hearing the story of struggle, we asked McMorait what gives them power to go on?
how do they find strength to continue the struggle despite adversity?
We got three beautiful pillars of support, and we think they may be a good foundation for us all in our struggles:

> The things that give me power, especially in moments of darkness and despair are mainly three things.
> The first one is **sisterhood**. We live in an era where I feel so bad hearing little girls call each other “bro”.
> And I really can't stand this thing. Why do we need to have this bro identity and the bro code of masculine men
> even in our own more girly, more faggotry relationships? So having the sisterhood, relationships of care, of self-reflection,
> free space to communicate and to be in a bad place and to be in a bad mood and that people can accept you and show their way to support you.
> This is a very important thing, and this thing can only happen, in my perspective, in sisterhood.

> And the second thing that I find confluence to, which is **communities**. 
> I am one of the people that do not believe in the so-called LGBTQIA+ community.
> I think that, it's a very neoliberal, and very white perspective of the queer identities. 
> It erases all other identities and all other perspectives that queer people can carry. […]
> So I think that people should understand that we have to create our very own communities, and we need to be active parts of them.
> Because not everyone can offer the same things, in one community, and not everyone has the same things to offer all the time.
> And in these communities, we need to create spaces where authorities and traditional formations of power do not have a place in,
> and where people can feel safe to exist and to communicate everything that might be going wrong so that it can be done right.

> And the third thing is **intersectionality**, which again, is a key to understand why we live in periods of darkness and despair.
> It's one thing to find the space to exist and to find courage in these times, and another thing to understand why [those bad times] are happening.
> And for me, having an intersectional perspective on everything that is happening helps us understand why things are happening
> and how they're affecting the other people. Because, I might have felt despair a lot of times,
> but I can understand that there are people every time that experience the same thing, that brought this despair to me,
> must have been even harsher for them. And the murder of Zackie Oh was a very important moment in Greece, that showcase that we need to
> be very intersectional about it. Because, first, they said ‘it was an immigrant’, They didn't know, it was a Greek person that was murdered.
> Then they tried to say that Zaki was under influence of drugs at the moment that she was murdered, so she might have died from a heart attack 
> or something like that from the drug abuse. This again, was proved to be a lie by the medical examinations. 
> So, when we tried to communicate what happened, we had to communicate that, first of all, it was racism, 
> then it was queerphobia and femmephobia and sexism, then it was hate for people who are drug addicts and the way they are perceived as less of humans.
> It was a case that intersectionality was key to understanding what happened, and I believe that it's key in almost all cases.

> So sisterhood, communities, intersectionality. Yes.

We all have each other. Avant-Drag! is reminding us of it in the most avant-garde way possible, through ten stories and many artistic expressions.

I want to finish this article with a chant created during protests after Zackie's murder, a chant that really stuck with us:
“Greece shall die, so that we can live”.

Old archetypes that don’t fit our modern understanding must be replaced. They helped us in the past times – but now they are hurting us.
This reminds me of another quote, from Willem De Kooning, displayed on his academy in Rotterdam:
“I have to change to stay the same”. We have to adapt to new times to stay ourselves.

We wholeheartedly recommend Avant-Drag! It shows real emotions entwined with angst for the future and appreciation of the community.
You can see the documentary during [DAG festival Warsaw](https://mdag.pl/21/pl/warszawa/movie/Avant-Drag%2521) and during Xposed Queer Film Festival Berlin.

<a href="https://mdag.pl/21/pl/warszawa/movie/Avant-Drag%2521" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force mt-5">
    <span class="fal fa-ticket-alt"></span>
    Get tickets here
</a>

<a href="https://www.instagram.com/avant.drag.film/" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force mt-3">
    <span class="fab fa-instagram-square"></span>
    Follow on Instagram
</a>

<a href="https://www.facebook.com/avantdrag" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force mt-3">
    <span class="fab fa-facebook-square"></span>
    Follow on Facebook
</a>

{gallery={
    "/img-local/blog/avant-drag/avant-drag-1-kangela.png":               "Kangela",
    "/img-local/blog/avant-drag/avant-drag-1-kangela-art.png":           "Kangela",
    "/img-local/blog/avant-drag/avant-drag-2-er-libido.png":             "Er Libido",
    "/img-local/blog/avant-drag/avant-drag-2-er-libido-mary.png":        "Er libido",
    "/img-local/blog/avant-drag/avant-drag-3-aurora.png":                "Aurora",
    "/img-local/blog/avant-drag/avant-drag-3-aurora-flaga.png":          "Aurora",
    "/img-local/blog/avant-drag/avant-drag-4-parakatyanova.png":         "Parakatyanova",
    "/img-local/blog/avant-drag/avant-drag-4-parakatyanova-sergay.png":  "Parakatyanova & SerGay", 
    "/img-local/blog/avant-drag/avant-drag-4-sergay.png":                "SerGay",
    "/img-local/blog/avant-drag/avant-drag-5-mcmorait.png":              "McMorait",
    "/img-local/blog/avant-drag/avant-drag-5-mcmorait-murder-site.png":  "McMorait",
    "/img-local/blog/avant-drag/avant-drag-6-cotsos.png":                "Cotsos",
    "/img-local/blog/avant-drag/avant-drag-6-cotsos-konstatntinos.png":  "Cotsos",
    "/img-local/blog/avant-drag/avant-drag-7-lala-kolopi.png":           "Lala Kolopi",
    "/img-local/blog/avant-drag/avant-drag-8-veronique.png":             "Veronique",
    "/img-local/blog/avant-drag/avant-drag-interview.png":               "",
}}

