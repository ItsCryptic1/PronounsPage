# The Gender Accelerationist Manifesto

<small>2024-05-29 | [@andrea](/@andrea)</small>

![Cover of the book: split into a pink part on top and black on bottom, with a silhouette of a person carrying a machine gun over their head; title “Gender Accelerationist Manifesto” on top, and names of authors, Eme Flores & Vikky Storm, on the bottom](/img-local/blog/gender-accelerationist-manifesto.png)

<div class="alert alert-warning">
    <span class="fal fa-exclamation-triangle"></span>
    <strong>Content warnings:</strong>
    mentions of violence, including sexual
</div>

I read something this week that made my brain very happy. It showed me a beautiful new perspective, a kind of unified theory of class struggle.
I might not _entirely_ agree with its content, I'll get to that later, but oh my, do I need to share this gem with more people!

It starts like this:

> Death to gender! Freedom to the queers! But gender dies through eating its own tail. Gender is dying already. Its death rattle is upon us, but it still has time to save itself. It is on us to hurry it along to its final end. To speed it on. To make it...
> 
> Accelerate.

I'm taking about [The Gender Accelerationist Manifesto](https://theanarchistlibrary.org/library/vikky-storm-the-gender-accelerationist-manifesto),
written by Eme Flores & Vikky Storm and published in 2019.
In this post I will introduce you to the content of the manifesto, but of course I'll have to leave a lot of details out
(and tbh, I might be misunderstanding some parts of it), so I _highly_ recommend reading the full thing yourself:

<a href="https://theanarchistlibrary.org/library/vikky-storm-the-gender-accelerationist-manifesto" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-4">
    <span class="fal fa-book-open"></span>
    Read the manifesto on The Anarchist Library website
</a>

So… What is gender _really_ about? The authors of the Manifesto claim that it's actually no different from our other social systems, like capitalism:
it's also about… **labour**.

> Gender is produced primarily by the division of reproductive labor.
> Reproductive labor is any labor that helps to produce the next generation, including sex, birth, childcare, and homemaking,
> and gender is defined by how this labor is divided up, with the different genders being distinct classes
> which are expected to perform specific sorts of tasks regarding reproductive labor.

This blew my mind. We usually think of gender in terms of culture or biology – but not labour.
But I guess most people also don't think of reproductive labour as labour, so that shouldn't surprise me…

We usually think of gender as some characteristic of an individual, just their categorisation into a group
– but Eme & Vikky equate gender to… patriarchy.

> As has been referenced previously, gender is a system of class, and is one defined by the domination of manhood over society.
> This is why another name for the gender class system is patriarchy.
> Gender as a social system is patriarchy and patriarchy is the social class system of gender.
> Within this class system, we find three distinct classes, two accepted and one subversive.

Just like Marx and Marxists distinguish social classes (bourgeoisie, proletariat, petite bourgeoisie, lumpenproletariat)
and call for their abolition in the name of building a classless society,
the authors of the Manifesto apply the same logic to distinguish **three classes** in the gender system:
men – those who control reproductive labor and its fruits,
women – those who are ruled,
and queers – those who “relate to reproductive labor differently than how it’s imposed upon the population”,
and for whom “while it’s still possible [...] to reproduce the next generation, it is no longer a necessary part of sex and romance.”

We usually think of sex as some biological objective reality – conveniently brushing off the existence of people
with mixed sexual characteristics as some insignificant outliers – while according to the Manifesto,
sex is just as made up as other arbitrary divisions.

> But sex is a thing and, if it isn’t the basis of gender, what is it? Well, this formulation isn’t wrong, per se, it’s merely backwards.
> Gender forms the basis of sex. We are not born with sex already within us. We have penises, vaginas, breasts, beards, chromosomes, etc,
> but these things are not sex on their own. They are features of our biology, but we group them into sexes.
> When we call penises boy parts we are creating and imposing gender upon the body.

> What this means is that sex is the gendering of our biological features. We assign gender to our biology and claim them to be innate.
> This is used to present the gender class system as a natural thing that just exists rather than a social system that gets imposed upon us.
> By gendering our bodies, we act as if gender just is rather than it being something that we’ve created.
> As such, sex serves to reinforce and defend gender.

The reason why it all blew my mind is because of how this simple initial claim puts _everything_ into perspective.
It's a basis for a consistent framework of thought that, at least in my head, turns all the puzzle pieces
into a finished picture.

For example… You know how some people claim that “LGB should split away from T, because one is about sexual orientation and the other about gender”?
Or those who argue that “polyamory isn't queer, because some polyamorous people can be cis and straight”?
I've been disagreeing with those claims and coming up with arguments against them –
but if we simply look at gender as a way to enforce the division of reproductive labour, then we don't even need to look any further:
patriarchy is a system that enforces a _very_ specific model of family, relationships and sex life,
and if you reject that model and live your life outside of this system – you're queer.

> Queerness is a lack of engagement with the enforced labor of gender, after all.

I can't cover here _everything_ that's being discussed in the Manifesto –
seriously, just go and read the whole thing, it's not that long –
but honourable mentions go to the fact that the Manifesto also examines the **diversity of gender systems in different cultures**
and how that fits into their framework; the fact that they discuss and explain **sexual violence**;
and also to how wonderfully **intersectional** the Manifesto is, for example by also explaining ableism from the same perspective
of forced labour division.

But if gender/patriarchy is an oppressive and inherently violent system that enforces unfair, limiting and strict division of reproductive labour,
what even makes us follow it? The answer is, unfortunately, ourselves. **We perform gender until we start believing it.**
We say “I'm a man”, we decide not to play with some toys because “it's a boy toy”, we decide not to wear clothes or makeup
we like because it's from a different section of the store that we were assigned to.

> This serves as a method of control and reproduction. Gender isn’t inherent, but it spreads by assigning us
> to a class and forcing us to say yes to that class. “Yes, I am a man. It is who I am and who I always have been.
> I cannot escape it or deny it. I am a man.” This is nothing but a lie we are forced to repeat. But by repeating it enough,
> we come to believe it. Gender becomes natural, inescapable, eternal. It ceases to be an imposed identity
> and becomes an eternal part of who we are. By objecting to my gender, you are objecting to that which is inherently me.

The authors go on to introduce The Communist Movement and to draw a parallel between how communism is the solution
to the oppressive regime of capitalism – and how Queer is the solution to the tyranny of gender.
They call their movement “**Gender Communism**”.

And honestly, that kinda helps me accept the popular division of political stances into the “left” and “right”.
Considering all the various subjects one could have all the various opinions about, turning them into a single axis
always seemed like too much of an oversimplification for me. But discovering this relationship between
“economical” and “social” issues through the communist, anarchist and queer lens of labour division systems
actually makes me understand why so many views converge around that left-right axis,
and appreciate how this simplification can be useful for understanding the world.

But anyways… Eme & Vikky continue on to cover their bases by addressing a concern that a lot of people
probably have about their vision of the world: if I like being my gender, why would you take that away from me?
Gender identity is _obviously_ important for many people, including trans people,
and especially those who experience strong gender dysphoria (or euphoria).
They explain that abandoning a system that produces the need to divide ourselves into genders
will not remove anyone's identity – **it will just make it optional as opposed to enforced**.

I would say: imagine you end up living on a planet where you are provided with literally everything you could possibly need;
the scarcity of resources doesn't limit you, you're free to do whatever you want without ever worrying about money again!
Would you _really_ miss the concept of money in such a world?
And in a world where you're free to be as feminine or masculine as you want, to be attracted to people of any body type,
to be in a consensual relationship of any kind you want – would you _really_ miss the fact
that your genitals at birth were used to assign you a category that determines what you're allowed to wear,
how to behave and who you're allowed to be with? 

Authors then proceed to a section titled “No Future” – and I'd like to simply quote it in full, because it's just beautiful
as a summary of the social conflicts and as a hopeful view on what's coming:

> Tied up with all parts of the present state of things is the necessity for continual growth.
> States and white supremacy push forever outward, and often inward, through imperialistic and colonial expansion
> Capitalism seeks the infinite expansion of capital. And gender? The ultimate purpose it serves is the continuous expansion of people.
> The reproductive labor its based around all serves unending population growth.
> 
> This unsustainable growth is characteristic of the present state of things and connects all the systems of oppression within it.
> Communism of all sorts must ultimately challenge this need to grow and expand.
> Socialism destroys the need for economic growth, anarchy the need for state growth, queerness ultimately decouples love and reproduction.
> No longer are we all constrained to roles which force us to reproduce continuously and, instead, we can live free to choose whether we want to or not.
> 
> By destroying the need for growth and ending endless reproduction, queerness and communism in general abolishes the future as we know it.
> Here we find the most radical end to queerness. Through queerness we free ourselves from the need to grow and, in turn, say “no” to the future.
> And, with that radical “no”, we can imagine it could be another way.

The fall of capitalism, patriarchy and state are inevitable. Fuck yeah!

But now we get to the point where I start disagreeing with the Manifesto a bit – the “accelerationist” part of “gender accelerationist” 😅

The course of action that Eme & Vikky consider best in this situation is to speed up the process.
If the system is gonna fall anyway, and that fall is likely to be turbulent, let's just have a **revolution** now.
Similar to the Marxist “Dictatorship of the proletariat”, they postulate a “**Dictatorship of the Queer**” and “**Pink Terror**”.
According to them, we need “queer militias to fight against the structures of power”.
And I suppose we do… After all, this system is evil, it needs to be destroyed, and counting on its institutions
to protect the people actively subversive of that very system sounds naïve.

But to be honest, calling it “Pink Terror” makes it sound way worse than what they actually talk about in the Manifesto:
protecting queer people from violence, while keeping their responses proportional.
It's not like the queer militias would run around murdering people for misgendering us or for being straight.

Anyways… The authors say that a brutal, fascist counter-revolution would inevitably come and require an appropriate response from the queers.
And well… I sure _hope_ they're wrong. Maybe it's hopefulness, maybe it's naïveté, call what you want. But hear me out 😉

The far-right might be on the rise in the West, but we live in a vastly different world
from the one in which Hitler or Mussolini came to power.
The patriarchy might still be holding strong, but our liberation is looking more achievable than ever before.
There are horrible atrocities happening in the world, but compared the violent history of our species,
we're being surprisingly peaceful nowadays<sup>[[src]](https://ourworldindata.org/war-and-peace)</sup>.
At the time of the Russian Revolution only one in three men and boys could read and write, and among women and girls it was one in eight<sup>[[src]](https://web.archive.org/web/20120313183610/http://www.russia.by/russia.by/print.php?subaction=showfull&id=1190296667&archive=&start_from=&ucat=22&)</sup>.
Today we have magic rectangles in our pockets that give us access to all of humanity's knowledge, 
that connect us to people across the world in real time, and that expose us to diverse people and opinions.
Marx lived in a time and place where homosexuality was a crime – today we have marriage equality and access to gender-affirming care
in many countries all over the world.
The world has changed _massively_ since Marx or Lenin – maybe our methods should evolve too?
The world keeps changing so rapidly that many people can barely keep up – do we really need to speed things up?
Of course, a lot those positive social changes happened _precisely_ thanks to revolution, riots, protests, civil unrest, strikes, blood and tears.
But maybe after all that bloodshed and fight humanity is finally reaching a point where we're able to achieve
positive social change without violence?

I know, I know. Just because a lot of us might believe in a bloodless revolution,
doesn't mean fascist will stop being hateful, violent pieces of shit that we'll need to protect ourselves against.
But it's not just queers vs fascists – it's a whole world that's slowly but surely becoming queerer and more open.
Am I naïve to think that cishet people of good will would at least make the fascists less audacious?

I realise that my thoughts on the subject come from a position of **privilege**. As much queerphobic shit
I've had gone through in my life, it pales in comparison to what other queers around the world are suffering from.
Maybe my cosy life in an accepting country is just _good enough_ to make me think that small incremental change
is a better idea than starting a “pink terror” – but if I lived in a place where sucking a dick carries a death penalty,
I'd be the first to grab a gun? Maybe I'm just too cowardly for more drastic action? Maybe.
But maybe what the world really needs right now _is_ more of peaceful evolution and less of righteous revolution?
I honestly don't know. I should read up on the subject. And I should finally learn how to shoot a gun, just in case – 
but I really hope that it won't ever become necessary for me to use this skill.

Just to be clear: just as Eme & Vikky, I strongly reject assimilationist approach to the subject.
And I realise that a less extreme approach to achieving the post-gender society carries a risk of LGBTQ+ people
becoming complacent with the status quo instead of demanding an even better world.
I do, however, believe that we'll always have brave queer heroes that will inspire us to reach for our rights and freedoms,
that it's possible to empower queers to fight for what they deserve and for us to win that fight –
even if their lives at that already become _not terrible_.

At the end of the day, I guess we need a bit of both.
We need to be able to navigate and improve the system while it's still imposed on us –
but if I'm wrong and this scary world isn't redeemable after all, then we do need to be ready for a revolution.

Anyways… I'm sooooo glad that I stumbled upon this Manifesto – and I wish more people read it.
Whether you share the authors' predictions for the future, or feel more optimistic about what's coming,
whether it inspires you to start a revolution or not, 
it will hopefully give you an amazing perspective on gender and queerness.
In a way, the Manifesto glorifies transness – but not as in “transing the cis”, but: **empowering the trans**.
We need to feel our power. We need to share a vision of a better world where the oppressive systems are history.
Every little bit helps: the normalisation, the representation, the courage of going out on a street wearing something
that your gender is “not supposed to” wear, your pride and your conviction.
Because the more people say “no” to gender, the more they empower others to do the same,
and the sooner the societal fiction of gender collapses.

<a href="https://theanarchistlibrary.org/library/vikky-storm-the-gender-accelerationist-manifesto" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-4">
    <span class="fal fa-book-open"></span>
    Read the manifesto on The Anarchist Library website
</a>
