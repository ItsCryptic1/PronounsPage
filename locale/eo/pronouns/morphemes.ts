export default [
    'pronoun_n',
    'pronoun_a',
    'possessive_s_n',
    'possessive_s_a',
    'possessive_pl_n',
    'possessive_pl_a',
] as const;
