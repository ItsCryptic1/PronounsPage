export default [
    'subject',
    'absent_possesive',
    'direct',
    'direct_verb',
    'direct_possessive',
    'absent',
    'verb_start',
    'present',
    'past',
    'future',
    'order',
    'noun_end',
] as const;
