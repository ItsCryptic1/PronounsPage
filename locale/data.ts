import type { gendersWithNumerus } from '~/src/classes.ts';

export type PronounData<M extends string> = {
    key: string;
    description: string;
    normative: boolean;
    plural: boolean;
    pluralHonorific: boolean;
    pronounceable: boolean;
    history?: string;
    thirdForm?: M;
    smallForm?: M;
    sourcesInfo?: string;
    hidden?: boolean;
} & { [morpheme in M]: string | null };

export interface PronounGroupData {
    key?: string;
    name: string;
    pronouns: string;
    description?: string;
    hidden?: boolean;
}

export interface PronounExamplesData {
    singular: string;
    plural?: string;
    isHonorific?: boolean;
}

export type NounTemplatesData = {
    [G in typeof gendersWithNumerus[number]]?: string;
};
