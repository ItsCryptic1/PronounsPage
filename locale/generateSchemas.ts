import fs from 'fs';

import { createGenerator } from 'ts-json-schema-generator';

const __dirname = new URL('.', import.meta.url).pathname;

const schema = createGenerator({
    path: `${__dirname}/config.ts`,
    strictTuples: true,
    markdownDescription: true,
    // speed up schema generation; type checking happens separately
    skipTypeCheck: true,
}).createSchema('Config');

fs.writeFileSync(`${__dirname}/config.schema.json`, `${JSON.stringify(schema, null, 4)}\n`);
