declare module '*/config.suml' {
    import type { Config } from '~/locale/config.ts';

    const config: Config;
    export default config;
}

declare module '*/translations.suml' {
    import type { Translations } from '~/locale/translations.ts';

    const translations: Translations;
    export default translations;
}

declare module '*/pronouns/pronouns.tsv' {
    import type { PronounData } from '~/locale/data.ts';

    const data: PronounData<string>[];
    export default data;
}

declare module '*/pronouns/pronounGroups.tsv' {
    import type { PronounGroupData } from '~/locale/data.ts';

    const data: PronounGroupData[];
    export default data;
}

declare module '*/pronouns/examples.tsv' {
    import type { PronounExamplesData } from '~/locale/data.ts';

    const data: PronounExamplesData[];
    export default data;
}

declare module '*/nouns/nounTemplates.tsv' {
    import type { NounTemplatesData } from '~/locale/data.ts';

    const data: NounTemplatesData[];
    export default data;
}

declare module '*.tsv' {
    const data: Record<string, any>[];
    export default data;
}
