export default [
    'nominative',
    'accusative',
    'pronominal_poss',
    'predicative_poss',
    'dative',
] as const;
