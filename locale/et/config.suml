locale: 'et'

header: true

pronouns:
    enabled: false
    route: 'pronouns'
    default: 'tema'
    any: 'mistahes'
    plurals: false
    honorifics: false
    generator:
        enabled: true
        slashes: true
    multiple:
        name: 'Interchangeable forms'
        description: >
            Many nonbinary people use more than one form interchangeably
            and are fine with being called either of them.
            Some like when others alternate between those forms when talking about them.
        examples: ['tema&temake', 'tema&see']
    null: false
    emoji: false
    others: 'Teised asesõnad'

pronunciation:
    enabled: false
    voices:
        FI:
            language: 'fi-FI'
            voice: 'Suvi'
            engine: 'neural'

sources:
    enabled: true
    route: 'sources'
    submit: true
    mergePronouns: {}
    extraTypes: []

nouns:
    enabled: true
    route: 'dictionary'
    collapsable: false
    plurals: true
    pluralsRequired: false
    categories: []
    declension: false
    submit: true
    templates:
        enabled: true

# community:
#    route: 'terminology'

inclusive:
    enabled: false

terminology:
    enabled: false
    published: false
    categories:
        -
            key: 'sexual orientation'
            text: 'sexual orientation'
        -
            key: 'romantic orientation'
            text: 'romantic orientation'
        -
            key: 'tertiary orientation'
            text: 'tertiary orientation'
        -
            key: 'gender'
            text: 'gender'
        -
            key: 'gender expression'
            text: 'gender expression'
        -
            key: 'relationship model'
            text: 'relationship model'
        -
            key: 'language'
            text: 'language'
        -
            key: 'attraction'
            text: 'attraction'
        -
            key: 'politics'
            text: 'politics'
        -
            key: 'prejudice'
            text: 'prejudice'
    route: 'terminology'

names:
    enabled: false

people:
    enabled: false

# optional, but would be nice to have
english:
    enabled: true
    route: 'english'
    pronounGroups:
        -
            name: 'Normative forms'
            description:
                - >
                    Because of the limitations of <language> grammar, or simply because they just prefer it that way,
                    many nonbinary people decide to simply use “he” ({/on=„on”}) or “she” ({/ona=„ona”})
                    – either the same as their gender assigned at birth or the opposite.
                    That doesn't make them any less nonbinary! Pronouns ≠ gender.
            table: { on: 'Masculine', ona: 'Feminine' }

faq:
    enabled: false
    route: 'faq'

links:
    enabled: true
    split: false
    route: 'links'
    blogRoute: 'blog'
    links:
        -
            icon: 'globe-europe'
            url: 'https://web.archive.org/web/20220928213020/http://pronoun.is/'
            headline: 'Pronoun.is'
            extra: '– selle veebilehe inspiratsioon.'
        -
            icon: 'file-pdf'
            url: 'https://www.europarl.europa.eu/cmsdata/187113/GNL_Guidelines_ET-original.pdf'
            headline: 'Sooneutraalne keel Euroopa Parlamendis'
        -
            icon: 'atlas'
            url: 'https://eki.ee/teatmik/sugusid-tahistavad-sonad/'
            headline: 'Sugusid tähistavad sõnad'
            extra: '– Eesti Keele Instituut'
        -
            icon: 'newspaper'
            url: 'https://keeljakirjandus.ee/ee/archives/31521'
            headline: 'Sooliselt markeeritud sõnad Eesti spordiuudistes'
            extra: '– artikkel portaalis Keel ja Kirjandus'
        -
            icon: 'books'
            url: 'https://www.ajakiri.ut.ee/artikkel/1445'
            headline: 'Isa jalutab, ema tõreleb'
            extra: '– väitekiri Tartu Ülikooli teadusportaalis'
        -
            icon: 'newspaper'
            url: 'https://toimetaja.eu/sugu-keeles/'
            headline: 'Sugu keeles – kuidas see ühiskonda mõjutab?'
            extra: '– artikkel portaalis Toimetaja'
        -
            icon: 'newspaper'
            url: 'https://toimetaja.eu/sooneutraalsus-keeles/'
            headline: 'Kas sooneutraalsus keeles on parim siht?'
            extra: '– artikkel portaalis Toimetaja'
        -
            icon: 'newspaper'
            url: 'https://opleht.ee/2017/04/sonal-sabast-oved/'
            headline: 'Sõnal sabast: Õved'
            extra: '– artikkel portaalis Õpetajate Leht'
        -
            icon: 'newspaper'
            url: 'https://opleht.ee/2017/06/oved-voi-onnaksed/'
            headline: 'Õved või õnnaksed?'
            extra: '– artikkel portaalis Õpetajate Leht'
        -
            icon: 'tree'
            url: 'https://www.geni.com/discussions/72826?page=1'
            headline: 'Sugulussidemeid tähistavad mõisted'
            extra: '– arutelu portaalis Geni'
        -
            icon: 'book-open'
            url: 'https://www.lgbt.ee/sonastik'
            headline: 'LGBT-sõnastik'
            extra: '– Eesti LGBT Ühing'
        -
            icon: 'book-open'
            url: 'https://feministeerium.ee/sonastik/'
            headline: 'LGBT-sõnastik'
            extra: '– Feministeerium'
    academic: {}
    mediaGuests: []
    mediaMentions: []
    recommended: []
    blog: false
    zine:
        enabled: false

contact:
    enabled: true
    route: 'contact'
    team:
        enabled: true
        route: 'team'

support:
    enabled: true

user:
    enabled: true
    route: 'account'
    termsRoute: 'terms'
    privacyRoute: 'privacy'

profile:
    enabled: true
    editorEnabled: true
    defaultWords:
        -
            header: 'Eesliide'
            values: ['[eesliide puudub]', 'hr.', 'pr.', 'prl.']
        -
            header: 'Isiku- ja perekirjeldused'
            values: ['inimene', 'isik', 'mees', 'naine', 'härra', 'daam', 'proua', 'poiss', 'tüdruk', 'sõps', 'kutt', 'tšikk']
        -
            header: 'Komplimendid'
            values: ['iludus', 'nägus', 'armas', 'kuum', 'seksikas']
        -
            header: 'Suhtekirjeldused'
            values: ['sõber', 'partner', 'poisssõber', 'tüdruksõber', 'pruut', 'peigmees', 'abikaasa', 'kallis']
    flags:
        defaultPronoun: 'tema'

calendar:
    enabled: false
    route: 'calendar'

census:
    enabled: false

redirects: []

api: ~
