title: 'lipu pi nimi mi'
domain: 'tok.pronouns.page'

home:
    link: 'lipu open'
    welcome: 'lipu nimi la kama pona!'
    intro: >
        lipu ni li pana e nimi wile jan e sona tonsi.
    why: 'lipu li lon tan seme?'
    about:
        - >
            mi toki e sina la mi wile kepeken nimi sina. sina soweli Kasijona la mi o toki ala e ni: sina jan Punima. mi ni la sina ken pilin nasa li ken pilin ike.
        - >
            ilo ni la sina ken pana e nimi sina tawa lipu.
            lipu la ale li ken sona e ni: ona o toki e sina kepeken nimi seme?
    whatisit: 'ni li suli tan seme?'
    mission:
        header: 'wile mi'
        summary: 'nasin toki o pona tawa jan ale.'
        freedom: >
            <strong>nimi pona o ken.</strong><br>
            jan ale li ante. <em>ona taso</em> li sona pona e ni: nimi seme li pona tawa ona.
        respect: >
            <strong>ale o nimi pona e jan ante.</strong><br>
            jan li sona e nimi pona ona la ale o pona o kepeken nimi wile ona.
        inclusivity: >
            <strong>sona ala la nimi o awen pona.</strong><br>
            ken la mi sona ala e jan e nimi wile ona.
            ken la mi toki e nasin jan e jan wan ala.
            nimi o pona lon ni kin.

community:
    header: 'kulupu'
    headerLong: 'kulupu tonsi'

faq:
    header: 'sona'
    headerLong: 'sona pi nasin nimi jan'
    questions:
        toki-inli:
            question: 'lipu Pronouns.page li lon tan ni: toki Inli li ike li nasin mije meli e nimi "ona". toki pona li ike ala la lipu ni o toki pona tan seme?'
            answer:
                - >
                    mi ante e toki la mi ante mute e nasin ilo kin. mi weka e ni: lipu li wile toki e kule mute pi nimi "ona". nasin lipu li kama ni: sina ken toki e nimi sina.
        flags:
            question: 'kulupu pali o! o pana e sitelen kule ni! o weka e sitelen kule ni!'
            answer:
                - >
        mu:
            question: 'mu?'
            answer:
                - >
                    mu! mu mu.

links:

    social: 'mi lon ma ilo ante'

    languageVersions: 'lipu ni li lon toki ante'

people: ~

# this section is a short English-language explanation of how gendered and gender-neutral forms work in your language
# see for example: https://zaimki.pl/english
# it's optional, but would be nice to have
english:
    header: 'English'
    headerLong: 'An overview in English'
    headerLonger: 'An overview of Toki Pona pronouns'
    description: 'If you don''t speak Toki Pona, yet still are interested in how this language tries to cope with the omnipresent binaries, we''ve prepared a short overview of those ideas in English.'
    intro:
        - >
            Toki Pona doesn't have gendered pronouns at all. The pronoun {/li/ona=ona} can refer to anyone and anything. So this version of Pronouns.page makes no mention of pronouns, and is essentially a website for telling people what your name is.
        - >
            TODO(tbodt): finish writing this

contact:
    header: 'o toki tawa mi'
    authors: 'jan mama lipu'
    groups:
        all: ''
        shop: 'o esun'
    faq: >
        sina wile e sona la, nanpa wan la o lukin e {/faq=lipu pi sona suli}. sona wile li lon ala la o toki tawa mi.
    technical: >
        sina wile ala wile e sona ilo? anu la sina lukin e <strong>pakala</strong>? wile la o toki tawa mi mute. nimi
        sina en ijo mute pi wile sina o lon toki. <strong>o toki mute.<strong>mi wile sona e ni: sitelen pakala li toki e
        seme? sina lon ilo lili anu ilo suli anu seme? sina lukin e ilo ni kepeken nasin seme? mi wile e pakala sama la
        mi o seme? sina ken ala ken pana e sitelen? ken la, namako ilo li pakala e lipu ni. wile la <strong>sina ken
        weka</strong> e namako ilo. ken la, ni li pona e pakala.
    hate: >
        sina wile toki jaki utala e ike tonsi tawa mi la, o ala.
        mi toki ala tawa jan ike.
        sina pali e toki ike sina kepeken tenpo lili, taso mi weka e toki sina e sina kepeken tenpo lili lili.
    language: >
        mi mute li tan ma mute li toki e toki mute. sina toki tawa mi mute la ken la toki sina li kama tawa jan pi toki
        pona ala. ni la, mi wile e ni: o toki tawa mi mute kepeken <strong>toki Inli anu toki Posuka.</strong>
    team:
        name: 'kulupu pi nasin toki pi kule jan ale'
        nameShort: 'kulupu pi nasin toki'
        description: >
            mi kulupu li wile e ni: toki li ken toki pona e kule jan ale.
            nasin toki la mi alasa li pali li pona li pana.
            kin la mi lukin sewi e kulupu anpa ale.
        extra: []
        logo: 'sitelen pi kulupu ni la sitelen tonsi en sitelen toki li wan.'
        members: 'jan ni li lon kulupu'
        member: 'li lon kulupu pali pi lipu ni'
        join:
            header: 'o pali lon kulupu!'
            encouragement: >
                sina wile pona e nasin toki li wile pana e pali tawa kulupu mi la pona mute!
                mi wile e pali sina!
            areasIntro: 'mi ken wile e pali ni:'
            areas:
                - 'o pona e lipu nimi o pana e nimi sin'
                - 'o pona e lipu ni: ona li toki e lipu ante mute'
                - 'o pana e toki pona e sitelen pona tawa ale lon lipu mi kulupu'
                - 'jan li toki tawa mi kulupu la o toki tawa ona'
                - 'o weka e lipu jan ike'
                - 'o pali e lipu sama kepeken toki ante'
                - 'o lawa e pali ni: jan li pali e lipu sama kepeken toki ante'
                - 'o pali e lipu lili lon lipu ni o toki e ijo sin'
                - 'o pali e ilo'
                - 'pali ante mute'
            allies: >
                pali mute li wile e sona tonsi la jan tonsi o pali ni. pali ante mute li wile ala e ni la mi ken wile e jan pi tonsi ala.
            how: 'sina wile kama lon kulupu la, o toki tawa mi kepeken nimi {mailto:contact@pronouns.page=contact@pronouns.page} o toki e ni:'
            application:
                - 'sina seme?'
                - 'lipu pronouns.page la lipu sina li lon seme?'
                - 'sina pana e toki tawa ale lon lipu ante seme?'
                - 'sina pali ala pali sama ni lon tenpo pini: sina utala tawa pona kulupu?'
                - 'sina wile pana e pali seme?'
    contribute:
        header: 'o pana e pali'
        intro: >
            lipu Pronouns.page li pali kulupu. jan mute li pana e pali tan wile pona tan wile mani ala.
            sina kin li ken! mi wile e pali ni.
            sina pali mute la mi ken kama e sina lon kulupu pali mi.
        entries:
            header: 'o pana e sona sin'
            description: >
                lipu ni la lipu lili mute li lipu sona. sina ken pana e sona tawa lipu. sona weka o kama, anu sona pakala
                o ante, la o pana e pali a!
        translations:
            header: 'o pona e toki lon ilo ni'
            description: >
                pakala li lon ante toki la sina o luka e nimi "o ante e toki pi ilo ni" lon anpa pi lipu ni. ilo li toki
                e nasin pali.
        version:
            header: 'o pali e lipu sama pi toki sin'
            description: >
                sina wile pali e lipu ni kepeken toki sina la o tawa {https://en.pronouns.page/new-version=ni}. tenpo
                open la {/contact=o toki tawa mi}. ken la jan ante li pali sama.
        technical:
            header: 'o pana e toki ilo'
            description: >
                pali ilo la pali wile li lon {https://gitlab.com/PronounsPage/PronounsPage/-/boards=lipu ni}. pali li lon
                lipu "To Do" la jan ala li pali e ona la sina wile pali la o toki e wile o pali a! sina wile sona e ijo
                la o toki.
            footer: 'o pali tawa ilo lipu'
        design:
            header: 'nasin sitelen pi lipu ni'

support:
    header: 'o pana e mani'
    description: >
        lipu ni li wile e mani tawa ijo ilo mute.
        kin la jan pali li ken wile e telo nasa lili.
        sina wile pana e mani tawa ni la o kepeken nasin ni:
    bankAccount: 'o pana e mani tan poki mani'

user:
    header: 'sina'
    headerLong: 'sina'
    login:
        help: 'sina ken pana e lon sina tawa ilo kepeken nasin tu. ken la o luka e nena pi ilo ante lon poka. ken la o pana e nimi sina pi ilo Email la ilo li pana e nanpa nasa tawa ni la o pana e nanpa sama tawa ilo.'
        placeholder: 'ilo Email la nimi sina (ilo ni la sina jo e nimi la ni kin li ken)'
        action: 'o lon'
        emailSent: 'mi pana e nanpa nasa luka wan tawa sina <strong>%email%</strong>. o pana e nanpa lon ni. nanpa ni li ken e sina lon tenpo wan taso lon tenpo poka taso.'
        email:
            subject: 'nanpa nasa sina li {{code}}'
            instruction: 'o pana e nanpa ni lon ilo la mi kama sona e sina:'
            extra: 'sina wile ala e nanpa ni la o weka e lipu ni. ale li pona.'
        why: >
            sina kama lon ilo la sina ken pali e lipu sina ({/@example=sama lipu ni}).
        passwordless: 'ilo li wile ala e ni: sina pali e nimi len open. {https://avris.it/blog/passwords-are-passé=o kama sona e nasin ante mi.}'
        deprecated: 'pakala'
    code:
        action: 'o lon'
        invalid: 'nanpa li ike.'
    account:
        changeUsername:
            header: 'ilo ni la nimi sina'
            action: 'o ante e nimi'
            taken: 'jan ante li kepeken nimi ni.'
        changeEmail:
            header: 'ilo Email la nimi sina'
            action: 'o ante e nimi pi ilo Email'
    logout: 'o weka tan ilo @%username%'
    avatar:
        header: 'sitelen'
        change: 'o ante e sitelen'
    deleteAccount: 'o moli e sijelo mi @%username%'
    socialConnection:
        list: 'sijelo sina pi ilo ante'
        connect: 'o pana e ni'
        refresh: 'o sin'
        disconnect: 'o weka e ni'
    mfa:
        header: 'ilo ante sina la mi ken sona e ni: sina sina a.'
        enable: 'o pana e ilo ante sina'
    qr:
        header: 'sitelen QR'
    accountSwitch:
        add: 'o kama e sijelo ante'
        helper: >
            sijelo wan la sina ken pali e lipu ante lon toki ante.
            lipu ni ale la @nimi li sama.
            taso, sina ken wile e lipu mute lon toki sama, la sina ken wile e sijelo mute.
            ken la, lipu wan la sina kepeken lon pali mani, lipu wan la sina kepeken lon jan poka sina.
            sina kama e sijelo mute lon ilo ni la sina ken kama lon sijelo ante kepeken tenpo lili.
    socialLookup: 'o ken e ni: ijo li sona e nimi sina pi ilo ante ni la ona li ken lukin e nimi sina pi ilo ni kin'
    socialLookupWhy: '(ni li ken pona tawa ni: ilo lili li ken wan e ilo ni e ilo Siko e ilo ante)'

profile:
    description: 'toki pi ijo sina'
    names: 'nimi'
    words: 'nimi ante'
    birthday: 'tenpo suno pi sike sin'
    birthdayInfo: 'mi pana ala e ni tawa lipu sina. mi nanpa e mute pi tenpo sike sina li pana e ni taso.'
    age: 'mi lon tenpo sike'
    timezone:
        header: 'tenpo sina'
        placeholder: 'o pana e ma tenpo sina'
        info: >
            sina pana tawa ni la lipu sina li toki e ni: <strong>ma sina la tenpo seme li lon</strong>.
            sina wile la lipu li toki e poka ma sina kin.
        detect: 'o sona tan ilo sina'
        publishArea: 'lipu la ma suli sina o lon sama ni:'
        publishLocation: 'lipu la poka sina o lon sama ni:'
        time: 'mi la tenpo ni li %time% lon tenpo suno %weekday%'
        approximate: 'ni li ma poka suli taso'
        areas:
            Africa: 'ma Apika'
            America: 'ma Mewika'
            Antarctica: 'ma lete Antasika'
            Arctic: 'ma lete Asika'
            Asia: 'ma Asija'
            Atlantic: 'telo Alansi'
            Australia: 'ma Oselija'
            Europe: 'ma Elopa'
            Indian: 'ma Palata'
            Pacific: 'telo Pasipi'
    flags: 'sitelen kule'
    flagsInfo: 'o tawa e sitelen pi wile sina tawa poki ni.'
    flagsAsterisk: 'nasin ni li nasa ala, taso sitelen ona li lon tan ni: ken la jan li nasa lon ijo ante li nasa ala lon ni, sama ni: jan meli li olin mije li tonsi.'
    links: 'lipu ante'
    linksRecommended: 'lipu ni li pona:'
    column: 'kulupu nimi #'
    opinions:
        header: 'sitelen pilin'
        description: 'sona…'
        colours:
            _: '(kule…)'
            pink: 'loje walo'
            red: 'loje'
            orange: 'loje jelo'
            yellow: 'jelo'
            teal: 'laso'
            green: 'laso kasi'
            blue: 'laso telo'
            purple: 'laso loje'
            brown: 'jelo pimeja'
            grey: 'pimeja'
        styles:
            _: '(selo…)'
            bold-italics: 'wawa en tawa poka'
            bold: 'wawa'
            italics: 'poka'
            small: 'lili'
        validation:
            missingIcon: 'sitelen o lon'
            missingDescription: 'toki sona o lon'
            duplicateIcon: 'sitelen o sama sitelen ante ala'
            duplicateDescription: 'toki sona o sama toki sona ante ala'
            invalidOpinion: 'sitelen ni li lon ala kulupu pi sitelen pilin sina'
        custom: 'ni li pilin sin tan jan ni:'
    expendableList:
        show: '(o lukin)'
        more: '… ijo ante %count% kin'
    list: 'lipu nimi sina'
    init: 'o pali e lipu'
    show: 'o lukin'
    edit: 'o ante'
    editor:
        save: 'o pana e lipu'
        defaults: 'o weka e ante'
    opinion:
        yes: 'pona a'
        jokingly: 'lon musi taso'
        close: 'tan jan poka taso'
        meh: 'ike ala'
        no: 'ike'
    bannerButton: 'o lipu e nimi sina e sina'
    card:
        link: 'ilo o sitelen e lipu'
        generate: 'o pali e sitelen'
        generating: 'ilo li pali… ni li kepeken tenpo.'
    personal:
        header: 'ijo sina'
        description: 'sina taso li ken lukin e ni'
    language:
        header: 'toki'
        description: 'toki ante ni la lipu ni li lon'
        # if your language has declension and it's hard to fit the username in that sentence,
        # just make is 'This card is…'
    circles:
        header: 'kulupu mi'
        yourMentions:
            header: 'jan ante ni la sina lon kulupu ona'
            description: >
                jan ni li pana e sina tawa kulupu ona.
                sina pana e ona tawa kulupu sina la
                sitelen pona [s:shield-check] li kama lon sina tu lon lipu kulupu pi sina tu.
        removeSelf:
            action: 'o weka e mi'
            confirm: 'sina wile ala wile weka a e sina tan kulupu pi jan @%username%?'
    sensitive:
        header: 'len lipu'
        display: 'o lukin e lipu'
        info: >
            ijo mute li ike ala tawa lipu {/terms=Terms of Service} li ken ike tawa jan lukin,
            li ken nasa tawa jan lili.
            ijo sama li lon lipu sina la o pona tawa jan lukin o toki e ken ike pi ijo ni tawa ona.
            toki li ken sama ni: "mi toki lili e telo nasa", "toki pi wile unpa li lon",
            "mi toki e tenpo ni: jan li wile pakala e jan pi olin kule"...
    share:
        customise: 'nasin nasin'
        local: 'o tawa lipu pi toki ni taso'
        atAlternative: 'o kepeken nimi /u/'
    backup:
        header: 'awen lipu'
        headerShort: 'awen lipu'
        export:
            action: 'o awen e lipu sina'
    flagsCustomForm:
        altExample: 'toki sitelen li ken sama ni:'
    markdown:
        enable: 'sina wile ala wile kepeken nasin Markdown lon lipu sina?'
        features: 'sona nasin la o luka e ni'
        examples:
            - 'sitelen **pimeja**'
            - 'sitelen _poka_'
            - 'sitelen `ilo`'
            - 'sitelen ~~weka~~'
            - 'sitelen ==kule=='
            - 'sitelen ^sewi^'
            - 'sitelen ~anpa~'
            - 'sitelen lili nasa o \_kule ala\_ la o kepeken palisa `\` pi tawa pini'
    calendar:
        header: 'lipu tenpo'

share: 'o pana e ni tawa jan ante'

crud:
    remove: 'o weka e ni'
    filter: 'o alasa…'
    filterLong: 'o alasa…'
    search: 'o alasa…'
    copy: 'o jo e nasin pi lipu ni'
    download: 'o kama jo'
    alt: 'toki sitelen:'

footer:
    license: >
         lawa {/license=OQL} li lawa e {https://gitlab.com/PronounsPage/PronounsPage=mama ilo lipu} e toki lipu.
    using: >
        lipu ni li kepeken sitelen kule {https://www.gradientmagic.com/=gradientmagic.com} li kepeken sitelen pilin
        {https://fonts.google.com/noto/specimen/Noto+Color+Emoji=Noto Color Emoji}.
    source: 'toki ilo'
    links: 'o toki tawa mi'
    legal: 'toki pi nasin lawa'
    technical: 'ijo ilo'
    sibling: 'pali poka'
    stats:
        header: 'nanpa'
        overall: 'lipu ale pi toki ante'
        current: 'lipu ni taso'
        keys:
            users: 'mi jo e jan'
            cards: 'mi jo e lipu nimi'
            visitors: 'mun li sike e ma la jan'
            pageviews: 'mun la lukin'
            realTimeVisitors: 'sina lukin e ni la jan lukin li'
        month: 'li lon lipu ni'
    version: 'ilo li sin nanpa'

notFound:
    message: 'lipu li lon ala'
    back: 'o tawa lipu open'

terms:
    header: 'lipu lawa'
    lastUpdate: 'lipu ni li kama ante lon tenpo'
    consent: 'sina kama lon ilo la {/lawa=nasin lawa mi} li pona tawa sina.'
    # you can leave the terms untranslated, or translate it (copy it from the English version) and include the following disclaimer:
    translationDisclaimer: >
        {https://en.pronouns.page/terms=lipu lawa lon} li toki Inli.
        ona taso li lawa.
        lipu ni li lawa ala li pana e sona taso tan toki Inli.
    content:
        intro: >
            lipu lawa ni li lawa e ni: sina kepeken mi.
            mi seme? mi lipu pronouns.page. lipu ante mi kin li mi.
            kulupu Neutral Language Council en kulupu Nero Network (NL, KVK 88409120) li lawa e mi.
            sina wile toki tawa ona la o toki tawa poki lipu ni:
        consent: >
            sina kepeken mi la sina kute e lawa ni li ken e ni:
            mi awen e toki sina e sona sina li kepeken ona.
            mi ni la mi kute e {/privacy=lipu lawa pi jo sona}.
        accounts:
            header: 'sijelo ilo sina'
            age: >
                mi ken wile ala e ni: sina pali e sijelo sina lon mi.
                mi wile e ni lon seme?
                ken la lawa pi ma sina li wile e ni: sina o pali ala e sijelo.
                ni la mi kin li wile e ni.
                ken la tenpo sike suno 13 la sina lili.
                ni kin la sina o pali ala e sijelo.
                ken la sina suli lili la sina lon tenpo sike 13 lon tenpo sike 16 ala.
                ni la wile pi mama sina li lawa e ken sijelo.
            authentication: >
                sina pali e sijelo lon mi la sina lawa e nasin ni:
                sina kama lon sijelo sina.
                sina o awen pona e nasin ni.
                o pana ala e ken ni: jan ante li ken kepeken sijelo sina.
                jan ante li kama lon sijelo sina la o toki a e ni tawa mi lon tenpo ni.
            termination: >
                ale la mi ken moli e sijelo sina.
                mi ken toki ala e kama ni tawa sina.
                mi ken wile ni tan ni:
                sina kute ala e lipu lawa ni.
                mi ken moli e sijelo tan ijo ante ale kin.
            inactivity: >
                jan li pali e sijelo li pali ala e lipu nimi li kepeken sijelo ala lon tenpo mun wan la mi moli e sijelo.
                kin jan ante li kama ken kepeken nimi ona.
        content:
            header: 'toki sina'
            ownership: >
                sina toki lon mi la sina ken e ni: toki sina la ma ale en nasin ale la mi ken kepeken li ken ante ale li ken pana ale.
                nasin pi wile mi la mi sin anu mute anu pana e ona.
            liability: >
                jan ante li toki lon mi la
                toki ona li ken lon ala li ken pona ala li ken ike.
                kin suli la toki li nasin tawa lipu ante tawa ilo ante tawa ma ante la mi ken ala sona e ni.
            violations: >
                toki sina li kute ala e lawa ma li ike tawa nasin kulupu jan la sina o toki ala e ni lon mi.
                ike li ken sama ni:
            violationsSeparator: '. '
            violationsEnd: ''
            violationsExamples:
                totalitarian: 'o pona ala e lawa ma ni: lawa li anpa ale e jan li kute ala e ona'
                hateSpeech: 'o toki utala ala e kule jan'
                racism: 'o toki ike ala e jan pi kule selo ante'
                xenophobia: 'o toki ike ala e jan pi ma ante'
                homophobia: 'o toki ike ala e ni: jan li olin pi kule sama'
                transphobia: 'o toki ike ala e jan tonsi ni: kon ona en sijelo ona li ante'
                enbyphobia: 'o toki ike ala e jan tonsi ni: ona li meli la li mije ala'
                queerphobia: 'o toki ala e ni: olin nasa li ike'
                exclusionism: 'o wile ala e ni: jan pi olin nasa o weka'
                sexism: 'o toki ala e ni: mije anu tonsi li ike'
                misogyny: 'o toki ala e ni: meli li ike'
                harassment: 'o pali ala e ike ni: jan li ike pilin tan pali ni'
                impersonation: 'o toki ala e ni: sina jan ante'
                selfHarm: 'o toki ala e ni: jan o pakala anu moli e ona sama'
                childPornography: 'o pana ala e sitelen ni: jan lili li unpa'
                pedophilia: 'o toki ala e ni: jan o ken unpa e jan lili'
                unlawfulConduct: 'o kute e lawa ma'
                misinformation: 'o toki ala e sona ike e sona weka'
                doxxing: 'o pana ala e sona len pi jan ante'
                spam: 'o mu jaki ala'
                trolling: 'o wile ala e ni: jan li ike pilin tan toki sina'
                advertisement: 'o toki ala e wile ni: jan o esun e ijo'
                copyright: 'o kute e lawa jo. o kepeken pali pi jan ante lon nasin ike ala'
            violationsStrict: >
                ken la sina toki ike li len e ni kepeken nimi nasa.
                mi pilin taso e ni la mi ken pali sama ni:
                sina toki ike.
            responsibility: >
                sina toki lon mi la sina lawa e toki ni.
                sina kute ala e lipu lawa ni la mi weka e toki sina.
                mi ken toki ala e ni tawa sina.
                ken la mi moli kin e sijelo sina.
                kin sina kute ala e lawa ma la mi toki e ni e ijo sina tawa lawa ma.
        closing:
            header: 'ijo ante'
            jurisdiction: >
                sina kute ala e lipu ni la
                pakala suli li kama la tomo pi lawa ma o pona e pakala ni.
                ni la lawa pi ma Netelan li lawa e tomo ni.
            changes: >
                tenpo ale la mi ken ante e lipu lawa ni.
                ante ni li suli la mi toki e ni tawa sina.
                ni la mun li sike wan lon ma la mi pana e ante ni tawa lipu.
                ni la lipu sin ni li lawa.

privacy:
    header: 'lipu lawa pi jo sona'
    content:
        effort: >
            sina pana e toki pi len ala e sona len tawa mi.
            mi wile jo pona e ni sina.
            mi pali suli tawa ni.
            sina pana e ijo la jan ante o kama ala lukin e ona la mi wile len pona e ni sina.
            sona sina li tawa tan mi tawa kulupu pona ante.
            mi toki e kulupu pona lon anpa.
        data: >
            mi awen ala e sona len mute sina.
            mi jo ala e ona.
            mi kepeken ala ona.
            taso mi jo a e sona ni:
            sina toki e ijo tawa jan ante lon mi la mi jo e ona.
            sina kama lon sijelo sina kepeken sijelo pi ilo ante la mi jo e nimi len OAuth pi sijelo ni.
        editRemoval: >
            sina toki e ijo tawa jan ante lon mi la sina ken ante e ona li ken weka e ona.
        contact: >
            sina toki tawa mi kepeken ilo Email la mi jo e nimi sina pi ilo Email.
            ni la mi kin li ken toki tawa sina.
        cookies: >
            sina lon mi la mi pana e sona lili ni tawa ilo sina:
            sina jan seme?
            sina wile e seme lon lipu ni?
            sina weka tan lipu ni la sona ni li awen lon ilo sina.
            ni la sina kama sin tawa lipu la sona li awen lon li pona e lipu.
            ona li lon sin e sijelo sina li pana e wile sina tawa lipu.
            sona ni li awen lon ilo sina taso.
            kin kulupu esun ante li pana ala e sona tawa ilo sina tan wile mi.
            ni la sona sina li awen lon anpa len.
        plausible: >
            jan seme li kama lon mi?
            tenpo kama li seme?
            mute jan li seme?
            mi wile sona e ni.
            mi kepeken ilo <strong>Plausible</strong> tawa ni.
            ilo li kama jo e sona ni.
            sona ni li jo ala e nimi sina e nimi IP pi ilo sina.
            {https://plausible.io/privacy=lipu lawa jo ona ni} li lawa e ilo ni.
        turnstile: >
            mi wile ala e ni: ilo ike li utala e mi.
            mi wile weka e ilo ni.
            mi kepeken ilo <strong>Cloudflare Turnstile</strong> tawa ni.
            {https://www.cloudflare.com/privacypolicy=lipu lawa jo ona ni} li lawa e ilo ni.
        gdpr: >
            ma Elopa la lawa jo GDPR li len pona.
            lawa ni li lawa e sona sina lon mi.
            lawa ni li ni e sona sina:
            sina wile la sina ken lukin e ona li ken ante e ona li ken weka e ona.

admin:
    header: 'ilo lawa'

table:
    empty: 'ala…'

api:
    header: 'nasin pi toki ilo (jan ale li ken kepeken ni)'

quotation:
    start: '“'
    end: '”'
    colon: ''

localise:
    shorter: 'o pana e toki sina'
    short: 'o pali e lipu ni pi toki sina'
    long: 'sina wile ala wile pali e lipu ni lon toki sina? wile la pona a! o tawa'
    longLink: 'lipu ni a!'

images:
    upload:
        instructionShort: 'o pana e sitelen'

error:
    invalidImage: 'ijo ni li pona ala. ona o sitelen. kin la ona o lili tawa 10 MB.'

captcha:
    reason: 'sina ilo ala ilo? mi wile sona tan ni: ilo mute li lukin jaki mute e lipu ni.'
    invalid: 'pakala. ken la sina ilo. sina ilo ala la o pali sin.'

mode:
    light: 'walo'
    automatic: 'sama ilo sina'
    dark: 'pimeja'
    reducedColours: 'kule o wawa lili'
    reducedItems: 'ijo o mute suli ala'
    accessibility: 'o pona e lukin: '

ban:
    action: 'o weka e jan ni'

report:
    action: 'lipu ni li ike'

translationMode:
    action: 'o ante e toki pi ilo ni'
    changes: 'ante'
    commit: 'o pana e ante'
    revert: 'o weka e ante'

flags_alt:
    Progress_Pride_: 'sitelen kule leko la palisa kule supa luka wan li lon li kule ni:. loje en loje jelo en jelo en laso jelo en laso en laso loje. poka open la leko suli pi poka tu wan li lon li ni tawa lukin: ona li tawa tan poka open tawa poka pini. ona la palisa kule mute li lon li kule ni tan poka open: jelo en walo en loje walo en laso walo en kule ma pimeja en pimeja. jelo la sike laso loje lili li lon insa.'
