# Niebinie z okładki. Alin Szewczyk w lipcowym numerze „Elle”

<small>2023-09-16 | [@Tess](/@Tess)</small>

![Alin Szewczyk na okładce „Elle”; fotografia: Lola Banet, make-up: Anna Kobalczyk, strój: Valentino.](/img-local/blog/elle-alin.jpg)

<p class="small">Alin Szewczyk na okładce „Elle”; fotografia: Lola Banet, make-up: Anna Kobalczyk, strój: Valentino.</p>

**„Alin. Niebinarny model podbija światowe wybiegi” – czytamy na okładce magazynu „Elle”. Choć Alin Szewczyk ma zaledwie 24 lata, zagrał już główną rolę w filmie, a na wybiegach prezentował kreacje najważniejszych domów mody na świecie.**

Magazyn „Elle” sprzedaje się w nakładzie około 40 tysięcy egzemplarzy, co oznacza, że dociera naprawdę szeroko. W lipcu wraz z kompendium wiedzy o modzie do osób czytelniczych trafił wywiad z Alin Szewczyk, osobą niebinarną zajmującą się modelingiem i aktorstwem. Twarz Alin zobaczyłośmy już na okładce. Choć wielu osobom może się wydawać, że to nic poważnego – ot, ciuchy, zdjęcia i wybiegi, błaha tematyka, nie to, co sprawy naprawdę ważne ([jak polityka](https://zaimki.pl/blog/osoby-niebinarne-na-listy-wyborcze)) – ta wiadomość oznaczała ni mniej, ni więcej, że w całej Polsce można było trafić na przekaz, że niebinarny model z Polski jest zauważalny w skali światowej. To kolejna cegiełka do naszej widoczności w społeczeństwie, a w dodatku wiadomość niezwykle odświeżająca: można być osobą queerową, transpłciową, niebinarną w Polsce – i mimo wszystko robić karierę! W rzeczywistości pełnej transfobii i enbyfobii potrzebujemy nie tylko historii o zmaganiach z problemami, ale też opowieści o sukcesie. 

> Kariera Alin nabrała rozpędu po tym, jak w sezonie wiosna-lato 2023 zamykał pokaz Prady podczas włoskiego tygodnia mody w Mediolanie. Potem były kolejne prêt-à-porter i haute couture. Damskie i męskie. JW Anderson, Dries Van Noten, Sportmax i wreszcie Louis Vuitton. Dla tego ostatniego domu mody Alin przeszedł już trzy razy, ostatnio w Seulu na spektakularnym show kolekcji cruise. Ale jego talent ma wiele wymiarów. Alin próbuje swoich sił także w filmie – debiutował w „Pewnego razu w listopadzie” w reż. Andrzeja Jakimowskiego u boku Agaty Kuleszy i właśnie zagrał pierwszą główną rolę w produkcji Netflixa „Fanfik”.

To wstęp do wywiadu, które przeprowadziły z Alin Szewczyk Maja Chitro oraz Angelika Warlikowska. Model opowiedział im o odkrywaniu swojej tożsamości, o formach językowych, jakich używa w języku polskim i angielskim i o doświadczeniach misgenderingu.

> **Jak się do Ciebie zwracać?**
>
> ALIN SZEWCZYK: Identyfikuję się jako osoba niebinarna, transmęska. Oznacza to, że nie wpasowuję się w pojmowanie tego, kim dla społeczeństwa jest kobieta lub mężczyzna. Z kolei w swojej fizycznej ekspresji skłaniam się w stronę męską. W języku polskim dopiero uczymy się mówić o osobach niebinarnych. Znam ludzi, którzy używają osobatywów albo neutralnych form „ono”, „jeno”, „jenu”. Ja używam męskich, żeby było prościej. A w języku angielskim mówię o sobie „they/them”.
>
> **Zdarzyło się, że ktoś odmówił używania wobec Ciebie męskich zaimków?**
>
> Ostatnio nie, bo funkcjonuję wśród ludzi, którzy mnie szanują. Jeśli dojdzie do użycia innej formy, to przez pomyłkę, z przyzwyczajenia. Grono, w którym się obracam, jest akceptujące. Ale zdarzają się osoby, które mówią, że dla nich jestem dziewczyną, więc będą zwracać się do mnie w żeńskiej formie.
>
> **Co wtedy czujesz?**
>
> Myślę: „Znowu to samo”. Raczej mnie to nie rusza, ale wolę unikać takich sytuacji, bo im częściej się trafiają, tym gorzej wpływają na moją głowę. Prawda jest taka, że wciąż niewiele osób spoza okołoqueerowego środowiska ma styczność ze społecznością trans, więc nie zastanawia się nad tym.
>
> **Pamiętasz, kiedy po raz pierwszy świadomie zacząłeś mówić, że jesteś osobą niebinarną?**
>
> Odkrywanie swojej tożsamości to długi proces. Wiedziałem, że jestem transpłciowy już w wieku 10 lat. Przez to, że nie znałem słów, które pomogłyby mi to nazwać, myślałem długo, że jestem transchłopakiem. Tuż po liceum, gdy zacząłem żyć na własną rękę, zrozumiałem, że nie uznaję podziału na dwie płcie, nie czuję tego ani nie czaję, dlaczego miałbym być dziewczyna albo chłopakiem. Słowo „niebinarność” pozwoliło mi osadzić się w rzeczywistości. Mniej więcej od 2019 roku zacząłem funkcjonować otwarcie jako osoba niebinarna, a w 2020 zmieniłem zaimki na męskie. (…)
>
> **Po premierze filmu pojawił się na Netfliksie dokument „Jesteśmy idealni”, który jest rozwinięciem historii kilku osób startujących do głównej roli.**
>
> To dla mnie bardzo ważny projekt, w którym kamera podąża za ludźmi nieheteronormatywnymi. Twórcy dokumentu, Marek Kozakiewicz i Anu Czerwiński, pokazali ich codzienność, wzloty, upadki, problemy, z którymi się borykają. Każda z tych osób jest inna, każda się otworzyła. Tytuł „Jesteśmy idealni” jest dla mnie puszczeniem oczka, bo – jak widać w dokumencie – nie jesteśmy. I bardzo dobrze zdajemy sobie z tego sprawę. Czasem czujemy się samotni w tym, jak nas postrzegają ci, którzy nas nie rozumieją. Podczas kręcenia tego materiału została wykonana tytaniczna praca emocjonalna zarówno ze strony twórców, jak i osób, które w nim występowały. Takie produkcje jak „Fanfik” czy „Jesteśmy idealni” mogą pomóc otworzyć oczy na to, że ludzie tacy jak ja to też ludzie. Zostawiłem w tym projekcie swoje flaki, jestem nim poruszony, dumny i cieszę się, bo zmienił moje życie na lepsze, zbliżył z powrotem z rodziną. Udowodnił moją sprawczość w życiu.

To oczywiście tylko fragment rozmowy z Alin – ten najbardziej koncentrujący się na kwestiach językowych. Nie jest to jedyny interesujący fragment wywiadu. Alin wyznał „Elle” m.in., że nie odpowiada mu konsumpcjonizm i mimo kariery… marzy o wybudowaniu mostu. Dosłownie – zdobył uprawienia spawacza i chciałby przekuć te umiejętności w czyn.

[„Popkultura nie pozwoli Wam ignorować niebinarności”](https://zaimki.pl/blog/popkultura-niebinarność) – pisałośmy, opowiadając o rolach Emmy D’Arcy, Belli Ramsey oraz innych niebinarnych osób aktorskich. Bo czy tego chcemy, czy nie, bez względu na to, co sądzimy o branży modowej czy Hollywood, to głośne kariery sprawiają, że nie da się przemilczeć naszego istnienia. To wywiady z osobami celebryckimi wymuszają korzystanie z poprawnych form i normalizują podawanie zaimków w biogramach. To ich zdjęcia trafiają na okładki głośnych pism – i oby było tak jak najczęściej! 
