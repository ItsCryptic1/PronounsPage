# Niebinarna Eurowizja

<small>2024-03-06 | [@andrea](/@andrea), [@T_Vos](/@T_Vos)</small>

![](/img-local/blog/eurovision-2024.png)

_Content warning: w tekście pojawia się wspomnienie o przemocy seksualnej_

- **[Eurowizja 2024 – komentarz w sprawie artykułu o queerowych osobach artystycznych](/blog/niebinarna-eurowizja-komentarz)**

[Konkurs Piosenki Eurowizji](https://eurovision.tv/) od dawna cieszy się dużą popularnością wśród społeczności queerowej; 
jest też pełen reprezentacji różnorodnych queerowych tożsamości
– w tematyce nienormatywności płciowej do głowy przychodzi chociażby [Dana International](https://www.youtube.com/watch?v=Fv83u7-mNWQ),
transpłciowa zwyciężczyni z 1998 roku reprezentująca Izrael,
czy austriacka drag queen [Conchita Wurst](https://www.youtube.com/watch?v=SaolVEJEjV4), która wygrała konkurs w 2014 roku.

Bardzo pozytywnym przejawem rosnącej akceptacji niebinarności w społeczeństwie jest to,
że tegorocznej edycji Eurowizji możemy się spodziewać bezprecedensowej reprezentacji
osób otwarcie niebinarnych i nienormatywnych płciowo – aż cztery kraje wystawiły do konkursu takie osoby!
Zapraszamy do zapoznania się z nimi i ich piosenkami:

## 🇬🇧 Olly Alexander – Dizzy

Wielką Brytanię będzie reprezentowała gwiazda muzyki pop, wokalista zespołu Years & Years – Olly Alexander.

Olly pochodzi z Anglii, jest najbardziej znany jako główny głos Years & Years i późniejszej solowej kariery.
Jest także uznanym aktorem m.in. z serialu „It's a sin” o epidemii HIV/AIDS z lat 80'tych.
Często wykorzystuje swoją platformę do poruszania kwestii ważnych dla środowiska LGBTQ+

Olly jest osobą niebinarną używającą [form męskich](https://en.pronouns.page/he):

> I feel very nonbinary, and you know, I identify as gay and queer and nonbinary <sup>[[link]](https://nonbinary.wiki/wiki/Olly_Alexander)</sup>

Olly napisał swoją Eurowizyjną piosenkę razem z Dannym L. Harle, znanym producentem z UK.
Dizzy to piosenka o zatracaniu się w miłości. Usłyszeć ją będzie można zarówno podczas pierwszego półfinału Eurowizji
(ten sam co Polska), jak i podczas sobotniego finału.

{embed=//youtube.com/embed/mvs92WfR8lM=Olly Alexander – Dizzy}

## 🇮🇪 Bambie Thug – Doomsday Blue

Z Irlandii na Eurowizję pojedzie Bambie Thug – piosenkarze i autorze piosenek, które swój styl muzyczny opisuje jako ouija-pop oraz hyperpunk.
W swojej twórczości często porusza tematy rozstań, wiedźmiństwa i uzależnień od narkotyków.

Bambie używa zaimków [they/them](https://en.pronouns.page/they) oraz [fae/faer](https://en.pronouns.page/fae).

Faego piosenka „Doomsday Blue” może być określona jako „łamacz gatunków”. W krótkim, 3-minutowym (wymagania Eurowizji)
utworze Bambie mieści rock alternatywny, pop i jazz. Samo określa ją jako „elektryczno-metalowe załamanie”.
Piosenka zawiera liczne odniesienia do zaklęć, w tym słynnej Avada Kedavry. Tekst utworu jest o nienazwanej
bliskiej osobie, która dopuściła się gwałtu na Bambie w maju 2023 –
a napisanie piosenki i wykonanie jej na Eurowizji w maju mają na celu „przeklęcie” tych wspomnień i oprawcy.

{embed=//youtube.com/embed/eA2fKlT8Khw=Bambie Thug – Doomsday Blue}

## 🇨🇭Nemo – The Code

Szwajcarcarię reprezentuje w tym roku raperze, piosenkarze i muzycze występujące pod mononimem Nemo.
Kariera Nemo rozpoczęła się w 2015 roku od piosenki „Clownfisch”, a 2017 roku duży sukces odniósł singiel „Du”.
W kolejnych latach było nominowane do wielu nagród Szwajcarskiego rynku muzycznego. Nemo ma 24 lata.

Nemo używa po angielsku zaimków [they/them](https://en.pronouns.page/they), jednak w swoim rodzimym niemieckim
woli kiedy mówi się o nim [po imieniu, bez używania zaimków](https://pronomen.net/:Nemo).

„The Code” to piosenka o wychodzeniu z szafy oraz o życiu jako osoba niebinarna, o trudnościach i korzyściach z tego płynących.
Nemo mówi, jak coming out dał Nemo poczucie wolności do złamania „kodu” społeczeństwa, do bycia sobą. 
Nemo będzie można usłyszeć podczas czwartkowego półfinału oraz pewnie podczas sobotniego finału. 
W momencie pisania tego artykułu Nemo wymienia się wśród osób faworyckich do wygrania konkursu.

{embed=//youtube.com/embed/kiGDvM14Kwg=Nemo – The Code}

## 🇦🇺 Electric Fields - One Milkali (One Blood)

Electric Fields to australijskie duo tworzące muzykę elektroniczną łączące współczesny soul z kulturą aborygeńską.
Składa się wokalisty Zaachariahy Fieldinga oraz klawiszowca i producenta Michaela Rossa.

Opisują się jako „dwaj kobiecy bracia” i używają [form męskich](https://en.pronouns.page/he). 

> Michael: We wrote that we are “two feminine brothers” in our bio and some people questioned if we should keep it in.
> Running and throwing “like a girl” was always an insult and that’s total horse shit. Girls and woman are total bosses and we love that part of us.
> 
> Zaachariaha: There’s room for everybody but the modern world loves building walls and categorizing everything.
> Am I a man, a woman, are we an Indigenous band, a queer band? All these boxes feel like barriers and we just fly right over the top on them… sorry suckers!
> 
> <sup>[[link]](https://www.dnamagazine.com.au/electric-fields/)</sup>

One Mikali to piosenka promująca dialog międzyludzki i rozwiązywanie konfliktów przez rozmowę, 
chwaląca kulturę Australijskich Osób Aborygeńskich (z której pochodzi Zaachariaha). 
Tekst jest po angielsku oraz w języku Yankunytjatjara. 

{embed=//youtube.com/embed/tJ2IaHxCvdw=Electric Fields - One Milkali}
