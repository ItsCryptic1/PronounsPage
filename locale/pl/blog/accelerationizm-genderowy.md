# Manifest Akceleracjonizmu Genderowego

<small>2024-05-29 | [@andrea](/@andrea)</small>

![Okładka książki: podzielona na różową część na górze i czarną na dole, z sylwetką osoby niosącej karabin maszynowy nad głową; tytuł „Manifest Akceleracjonizmu Genderowego” na górze i nazwiska autorów, Eme Flores & Vikky Storm, na dole](/img-local/blog/gender-accelerationist-manifesto.png)

**Content warning:** wspomnienia o przemocy, w tym seksualnej.

Przeczytałom w tym tygodniu coś, co połechtało mi mózg. Pokazało mi piękną, nową perspektywę, taką zjednoczoną teorię walki klas.
Może nie zgadzam się _całkowicie_ z jego treścią, o czym później, ale koniecznie muszę podzielić się tym znaleziskiem z większą liczbą osób!

Zaczyna się to tak:

> Śmierć dla genderu! Wolność dla queerów! Ale gender już umiera, jedząc własny ogon. Płeć już umiera.
> Jej ostatni oddech jest już nad nami – ale wciąż ma czas, by się uratować.
> To od nas zależy, żeby przyspieszyć jej koniec. Sprawić, by śmierć genderu…
>
> Przyspieszyła.

Mówię o [Manifeście Akceleracjonizmu Genderowego](https://theanarchistlibrary.org/library/vikky-storm-the-gender-accelerationist-manifesto),
napisanym przez Eme Flores & Vikky Storm i opublikowanym w 2019 roku.
W tym poście przedstawię wam treść Manifestu, ale oczywiście będę musiało pominąć wiele szczegółów
(i szczerze mówiąc, mogę źle rozumieć niektóre jego części), więc _bardzo_ polecam przeczytanie całości:

<a href="https://theanarchistlibrary.org/library/vikky-storm-the-gender-accelerationist-manifesto" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-4">
    <span class="fal fa-book-open"></span>
    Przeczytaj manifest na stronie The Anarchist Library
</a>

Więc… O co _naprawdę_ chodzi w płci? Osoby autorskie Manifestu twierdzą, że nie różni się ona tak bardzo
od pozostałych systemów społecznych, takich jak kapitalizm – w niej również chodzi o… **pracę**.

> Gender/płeć jest efektem głównie podziału pracy reprodukcyjnej.
> Praca reprodukcyjna to każda praca, która pomaga wytworzyć następne pokolenie, w tym seks, poród, opieka nad dziećmi i prowadzenie domu,
> a płeć jest definiowana przez to, jak ta praca jest podzielona, z różnymi płciami będącymi odrębnymi klasami,
> które mają wykonywać określone rodzaje zadań związanych z pracą reprodukcyjną.

To mi dało do myślenia. Zwykle myślimy o płci w kategoriach kultury lub biologii – ale nie pracy.
Ale też większość ludzi również nie myśli o pracy reprodukcyjnej jako o pracy, więc nie powinno mnie to dziwić…

Zwykle myślimy o płci jako o jakiejś cesze jednostki, po prostu kategoryzacji ludzi na grupy
– ale Eme i Vikky zrównują płeć z… patriarchatem.

> Jak już wspomniano wcześniej, gender to system klasowy, zdefiniowany przez dominację męskości nad społeczeństwem.
> Dlatego inną nazwą dla systemu klasowego gender jest patriarchat.
> Płeć jako system społeczny to patriarchat, a patriarchat to system klasowy płci.
> W ramach tego systemu klasowego znajdujemy trzy odrębne klasy, dwie akceptowane i jedną wywrotową.

Tak jak Marks i marksistki wyróżniają klasy społeczne (burżuazja, proletariat, drobnomieszczaństwo, lumpenproletariat)
i wzywają do ich zniesienia w imię budowania bezklasowego społeczeństwa,
osoby autorskie Manifestu stosują tę samą logikę do wyróżnienia **trzech klas** w systemie gender:
mężczyzn – tych, którzy kontrolują pracę reprodukcyjną i jej owoce,
kobiety – te, które są przez nich rządzone,
i queery – te osoby, które „odnoszą się do pracy reprodukcyjnej inaczej niż jest to narzucone populacji”,
i dla których „chociaż wciąż możliwa […] reprodukcja następnego pokolenia, nie jest już ona nieodłączną częścią seksu i relacji romantycznych.”

Zwykle myślimy o płci biologicznej jako o jakiejś obiektywnej rzeczywistości – wygodnie pomijając istnienie osób
o mieszanych cechach płciowych, jakby były jakimiś nieistotnymi wyjątkami – podczas gdy według Manifestu,
płeć biologiczna jest równie zmyślona, co inne arbitralne podziały.

> Ale płeć biologiczna jest rzeczą, a jeśli nie jest podstawą płci społeczno-kulturowej, to czym jest?
> Cóż, takie ujęcie sprawy nie jest błędne per se, jest po prostu odwrócone.
> Gender tworzy podstawę płci biologicznej. Nie rodzimy się z płcią biologiczną już w nas. Mamy penisy, waginy, piersi, brody, chromosomy itp.,
> ale te rzeczy same w sobie nie są płcią. Są cechami naszej biologii, my grupujemy je w płcie.
> Kiedy nazywamy penisy częściami męskimi, tworzymy i narzucamy ciału płeć.

> To oznacza, że płeć to genderowanie naszych cech biologicznych. Przypisujemy płeć naszej biologii i twierdzimy, że jest to coś wrodzonego.
> Używa się tego przypisania, aby przedstawić system klasowy gender jako naturalną rzecz, która po prostu istnieje,
> a nie jako system społeczny, który jest nam narzucany.
> Poprzez genderowanie naszych ciał, działamy tak, jakby gender po prostu istniał, zamiast być czymś, co same stworzyłyśmy.
> W ten sposób płeć biologiczna służy do wzmacniania i obrony genderu.

Powodem, dla którego to wszystko poprzewracało mi w głowie, jest to, jak to proste początkowe twierdzenie stawia _wszystko_ w perspektywie.
To podstawa spójnego systemu myślowego, który, przynajmniej w mojej głowie, składa wszystkie kawałki układanki w sensowny obraz.

Na przykład… Kojarzycie osoby krzyczące w internetach, że „LGB powinno oddzielić się od T, ponieważ jedno dotyczy orientacji seksualnej, a drugie płci”?
Albo że „poliamoria nie jest queerowa, ponieważ niektóre osoby poli mogą być cis i hetero”?
Nigdy zgadzałom się z takimi twierdzeniami i listowałom argumenty przeciwko nim –
ale jeśli po prostu spojrzymy na gender jako sposób wymuszania podziału pracy reprodukcyjnej, to nie musimy nawet szukać dalej:
patriarchat to system, który narzuca _bardzo_ specyficzny model rodziny, związków i życia seksualnego –
a jeśli odrzucasz ten model i żyjesz poza tym systemem – jesteś queer.

> Bo przecież queerowość to odrzucenie wymuszonej pracy reprodukcyjnej.

Nie mogę tu omówić _wszystkiego_, co jest poruszane w Manifeście –
serio, po prostu przeczytaj całość, tekst nie jest taki długi –
ale warto krótko wspomnieć o paru rzeczach:
o tym, że Manifest porusza również temat **różnorodności systemów płciowych w różnych kulturach**
i jak to wpasowuje się w ich wizję świata; o tym, że omawiają i wyjaśniają **przemoc seksualną**;
oraz o tym, jak wspaniale **intersekcjonalny** jest Manifest – na przykład ableizm również jest w nim wyjaśniany
z tej samej perspektywy wymuszonego podziału pracy.

Ale jeśli płeć/patriarchat to opresyjny i z natury brutalny system, który narzuca niesprawiedliwy, ograniczający i sztywny podział pracy reprodukcyjnej,
to co sprawia, że w ogóle go przestrzegamy? Odpowiedź to, niestety, my same. **Performujemy płeć, aż zaczniemy w nią wierzyć.**
Mówimy „jestem mężczyzną”, decydujemy się nie bawić niektórymi zabawkami, bo przecież „to zabawki dla chłopców”,
decydujemy się nie nosić ubrań czy makijażu, które nam się podobają, bo są z innej sekcji sklepu niż ta, do której zostałośmy przypisane.

> To służy jako metoda kontroli i reprodukcji. Płeć nie jest wrodzona, ale rozprzestrzenia się, przypisując nas
> do klasy i zmuszając nas do mówienia „tak” tej klasie. „Tak, jestem mężczyzną. To jest to, kim jestem i kim zawsze byłem.
> Nie mogę od tego uciec ani zaprzeczyć. Jestem mężczyzną.” To nic innego jak kłamstwo, które jesteśmy zmuszone powtarzać.
> Ale powtarzając to wystarczająco często, zaczynamy w nie wierzyć. 
> Płeć staje się naturalna, nieunikniona, niezmienna. Przestaje być narzuconą tożsamością
> i staje się nieodłączną częścią tego, kim jesteśmy. Sprzeciwiając się mojemu genderowi, sprzeciwiasz się temu, co jest we mnie wrodzone.

Osoby autorskie przechodzą do przedstawienia Ruchu Komunistycznego i rysują paralelę między tym,
jak komunizm jest rozwiązaniem dla opresyjnego reżimu kapitalizmu – a tym, jak Queer jest rozwiązaniem dla tyranii genderu.
Nazywają swój ruch „**Komunizmem Genderowym**”.

I szczerze mówiąc, to trochę pomaga mi zaakceptować popularny podział stanowisk politycznych na „lewicę” i „prawicę”.
Biorąc pod uwagę wszystkie różne tematy, na które można mieć różne opinie, sprowadzanie ich do jednej osi
zawsze wydawało mi się zbyt dużym uproszczeniem. Ale odkrycie tego związku między
„kwestiami ekonomicznymi” a „społecznymi” poprzez komunistyczną, anarchistyczną i queerową soczewkę systemów podziału pracy
faktycznie sprawia, że rozumiem, dlaczego tak wiele poglądów skupia się wokół tej osi lewica-prawica,
i doceniam, jak to uproszczenie może być przydatne do zrozumienia świata.

Ale tak czy siak… W dalszej części Eme i Vikky odnoszą się do potencjalnego zarzutu czy wątpliwości:
jeśli lubię swoją płeć, dlaczego chcecie mi ją odebrać?
Tożsamość płciowa jest _oczywiście_ ważna dla wielu ludzi, w tym osób transpłciowych,
zwłaszcza tych, które doświadczają silnej dysforii płciowej (lub euforii).
Osoby autorskie wyjaśniają, że porzucenie systemu, który wytwarza potrzebę podziału nas na płcie,
nie usunie niczyjej tożsamości – **po prostu uczyni ją opcjonalną zamiast narzuconą**.

Ja bym to ujęło tak: wyobraź sobie, że znajdujesz się na planecie, na której masz zapewnione dosłownie wszystko, czego tylko możesz potrzebować;
niedostatek zasobów cię nie ogranicza, możesz robić, co tylko chcesz, bez martwienia się o pieniądze!
Czy naprawdę tęskniłxbyś za pieniędzmi w takim świecie?
A w świecie, w którym możesz być tak kobieca lub tak męski, jak tylko chcesz, gdzie mogą ci się podobać osoby o dowolnym typie ciała,
gdzie możesz być w dobrowolnym związku dowolnego typu – czy naprawdę tęskniłxbyś za faktem,
że twoje genitalia przy urodzeniu zostały użyte do przypisania ci kategorii, która określa, co możesz nosić,
jak się zachowywać i z kim możesz być?

Następnie osoby autorskie przechodzą do sekcji zatytułowanej „Brak Przyszłości” –
i chciałobym po prostu zacytować ją w całości, ponieważ jest pięknym
podsumowaniem konfliktów społecznych i pełnym nadziei obrazem tego, co nadchodzi:

> Powiązana ze wszystkimi częściami obecnego stanu rzeczy jest konieczność ciągłego wzrostu.
> Państwa i biała supremacja pchają się wiecznie na zewnątrz, a często i do wewnątrz, poprzez ekspansję imperialistyczną i kolonialną.
> Kapitalizm dąży do nieskończonej ekspansji kapitału. A gender? Ostatecznym celem, jaki mu służy, jest ciągła ekspansja ludzkości.
> Praca reprodukcyjna, na której się opiera, służy niekończącemu się wzrostowi populacji.
> 
> Ten niezdrowy wzrost jest charakterystyczny dla obecnego stanu rzeczy i łączy wszystkie systemy opresji w jego ramach.
> Komunizm wszelkiego rodzaju musi ostatecznie zakwestionować tę potrzebę wzrostu i ekspansji.
> Socjalizm niszczy potrzebę wzrostu gospodarczego, anarchia potrzebę rozrastania się państw, a queerowość ostatecznie rozdziela miłość od reprodukcji.
> Nie jesteśmy już ograniczone do ról, które zmuszają nas do ciągłej reprodukcji i zamiast tego możemy żyć wolne, wybierając, czy chcemy to robić, czy nie.
> 
> Niszczenie potrzeby wzrostu i zakończenie niekończącej się reprodukcji, queerowość i ogólniej pojęty komunizm znosi przyszłość, jaką znamy.
> Tutaj znajduje się najbardziej radykalny koniec queerowości. Przez queerowość uwalniamy się od potrzeby wzrostu i w zamian mówimy „nie” przyszłości.
> I z tym radykalnym „nie” możemy sobie wyobrazić, że może być inaczej.

Upadek kapitalizmu, patriarchatu i państwa jest nieunikniony. Taaaaak, kurwa!

Ale teraz dochodzimy do miejsca, w którym zaczynam się nieco nie zgadzać z Manifestem –
z tą „akceleracjonistyczną” częścią „akceleracjonizmu genderowego” 😅

Podejście, które Eme i Vikky uważają w tej sytuacji za najlepsze, to przyspieszenie procesu.
Jeśli system i tak ma upaść, a ten upadek prawdopodobnie będzie burzliwy, zróbmy po prostu **rewolucję** teraz.
Podobnie jak w marksistowskiej „Dyktaturze Proletariatu”, postulują one „**Dyktaturę Queerów**” i „**Różowy Terror**”.
Według nich potrzebujemy „queerowych milicji, które walczyłyby ze strukturami władzy”.
No w sumie to potrzebujemy… W końcu ten system jest zły i trzeba go zniszczyć – a liczenie na jego instytucje,
aby chroniły osoby aktywnie wywrotowe wobec tego systemu, brzmi naiwnie.

Ale szczerze mówiąc, nazywanie tego „Różowym Terrorem” sprawia, że brzmi to o wiele gorzej niż to, o czym faktycznie mowa w Manifeście:
o ochronie osób queerowych przed przemocą, zachowując przy tym proporcjonalność reakcji.
To nie tak, że queerowe milicje będą biegać i mordować ludzi za [złopłcenie](/terminologia#złopłcenie) lub za bycie hetero.

W każdym razie… Osoby autorskie mówią, że brutalna, faszystowska kontrrewolucja jest nieunikniona i będzie wymagała odpowiedniej reakcji ze strony queerów.
I cóż… Mam nadzieję, że się mylą. Może to nadzieja, może to naiwność, zwał jak zwał. Ale…

Skrajna prawica może i rośnie w siłę krajach Zachodu, ale jednak żyjemy w zupełnie innym świecie
niż ten, w którym do władzy doszli Hitler czy Mussolini.
Patriarchat może i nadal trzyma się mocno, ale nasze wyzwolenie wydaje się bardziej na wyciągnięcie ręki niż kiedykolwiek wcześniej.
Wciąż dzieją się na świecie okropne zbrodnie, ale w porównaniu z brutalną historią naszego gatunku,
dziś jesteśmy zaskakująco pokojowe<sup>[[src]](https://ourworldindata.org/war-and-peace)</sup>.
W czasie rewolucji w Rosji tylko co trzeci mężczyzna i chłopiec potrafił czytać i pisać, a wśród kobiet i dziewcząt to była jedna na osiem<sup>[[src]](https://web.archive.org/web/20120313183610/http://www.russia.by/russia.by/print.php?subaction=showfull&id=1190296667&archive=&start_from=&ucat=22&)</sup>.
Dziś mamy w kieszeniach magiczne prostokąty, które dają nam dostęp do całej wiedzy ludzkości, 
które łączą nas z ludźmi z całego świata w czasie rzeczywistym, i które wystawiają nas na kontakt z różnorodnymi ludźmi i opiniami.
Marks żył w czasie i miejscu, gdzie homoseksualizm był przestępstwem – dziś mamy równość małżeńską i dostęp do opieki afirmującej płeć
w wielu krajach świata.
Świat zmienił się _ogromnie_ od czasów Marksa czy Lenina – może nasze metody również powinny ewoluować?
Świat zmienia się tak szybko, że wielu ludzi ledwo nadąża – czy naprawdę musimy to przyspieszać?
Oczywiście, wiele tych pozytywnych zmian społecznych nastąpiło właśnie _dzięki_ rewolucji, zamieszkom, protestom, niepokojom społecznym, strajkom, krwi i łzom.
Ale może po tej całej krwawej walce ludzkość wreszcie jest na tym etapie, w którym jesteśmy w stanie osiągnąć
pozytywne zmiany społeczne bez przemocy?

Wiem, wiem. To, że wiele z nas wierzy w bezkrwawą rewolucję, nie oznacza jeszcze,
że faszyści przestaną być nienawistnymi, brutalnymi skurwysynami, przed którymi będziemy musiały się bronić.
Ale to nie tylko queery kontra faszyści – to cały świat, który powoli, ale sukcesywnie staje się bardziej queerowy i otwarty.
Czy jestem naiwne, sądząc, że cishet osoby dobrej woli dadzą radę przynajmniej uczynić faszystów mniej zuchwałymi?

Zdaję sobie sprawę, że moje myśli na ten temat pochodzą z pozycji **przywileju**. Choć wiele queerfobicznych rzeczy
musiałom przetrwać w swoim życiu, nawet nie jestem w stanie tego porównać do tego, z czym zmagają się inne queery na całym świecie.
Może moje przytulne życie w akceptującym kraju jest po prostu _wystarczająco dobre_, by myśleć, że mała, stopniowa zmiana
jest lepszym pomysłem niż rozpoczynanie „różowego terroru” – ale gdybym mieszkało w miejscu, gdzie za zrobienie loda grozi kara śmierci,
to może byłobym pierwsze, które chwyciłoby za broń? Może jestem po prostu zbyt tchórzliwe na bardziej radykalne działania? Może.
Ale być może to, czego świat naprawdę teraz potrzebuje, to więcej pokojowej ewolucji, a mniej sprawiedliwej rewolucji?
Szczerze, nie wiem. Powinnom poczytać na ten temat. I powinnom w końcu nauczyć się strzelać, na wszelki wypadek –
ale naprawdę mam nadzieję, że nigdy nie będę musiało z tej umiejętności skorzystać.

Dla jasności: podobnie jak Eme i Vikky, zdecydowanie odrzucam podejście asymilacyjne do tematu.
I zdaję sobie sprawę, że mniej ekstremalne metody na osiągnięcie post-genderowego społeczeństwa niosą ryzyko, że osoby LGBTQ+ 
zadowolą się status quo zamiast domagać się jeszcze lepszego świata.
Jednak wierzę, że zawsze będziemy mieć odważne queerowe bohaterki, które będą nas inspirować do walki o nasze prawa i wolności;
że możliwe jest zagrzanie queerów do walki o to, na co zasługują, i wygranie tej walki –
nawet jeśli ich życie już udało się uczynić _nie takim strasznym_.

Tak sobie myślę, że potrzebujemy obu tych rzeczy.
Musimy być w stanie nawigować przez system i ulepszać go, dopóki jest on nam narzucony –
ale jeśli się mylę i ten przerażający świat jest nie do odkupienia, to musimy być gotowe na rewolucję.

W każdym razie… Jestem taaaaaaak szczęśliwe, że natknęłom się na ten Manifest – i chciałobym, żeby więcej osób go przeczytało.
Niezależnie od tego, czy podzielasz prognozy osób autorskich na przyszłość, czy jesteś od nich bardziej optymistycznx,
niezależnie czy Manifest zainspiruje cię do rozpoczęcia rewolucji, czy nie –
mam nadzieję, że da ci on niesamowitą, nową perspektywę na płeć i queerowość.
W pewnym sensie Manifest gloryfikuje transpłciowość – ale nie jako „transowanie cisów”, lecz **dodawanie siły transom**.
Musimy poczuć naszą siłę. Musimy dzielić się wizją lepszego świata, w którym systemy opresyjne są historią.
Każdy mały krok pomaga: normalizacja, reprezentacja, duma, odwaga do wyjścia na ulicę w ubraniu,
którego twoja płeć „nie powinna” nosić.
Ponieważ im więcej ludzi mówi płci „nie”, tym bardziej pomagają innym odważyć się na to samo,
– tym szybciej społeczne fikcje płci upadną.

<a href="https://theanarchistlibrary.org/library/vikky-storm-the-gender-accelerationist-manifesto" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-4">
    <span class="fal fa-book-open"></span>
    Przeczytaj manifest na stronie The Anarchist Library
</a>
