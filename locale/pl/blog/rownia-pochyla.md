# Zaimkowa równia pochyła

<small>2023-12-13 | [@andrea](/@andrea)</small>

Wydawało mi się, że mimo językowego skomplikowania polszczyzny nasze postulaty są względnie proste
i że udaje nam się je w czytelny sposób przekazać. Okazuje się jednak, że nie wszyscy dają radę zrozumieć
nasze propozycje na tyle, by oszczędzić sobie wygłaszania publicznej tyrady opartej o żenujący błąd logiczny 😅

Dotarł do mnie bowiem screenshot z FB, w którym autor komentarza apeluje, by do kwestii równościowych podchodzić
„bez przesadyzmu”: bo jeśli rozpowszechnią się feminatywy, to nieuchronnym krokiem będzie również
akceptacja neutratywów, a potem… rozbudowanie systemu końcówek i zaimków, by opisywały również
orientację seksualną, model związku, religię, preferencje polityczne, wiek czy stan majątkowy 🤦

![Równouprawnienie jest potrzebne i ważne, ale trzeba uważać, by (parafrazując klasyka) "celu nie sprofanowały środki". Jeżeli już bowiem rozpowszechnimy femininatywy, będziemy musieli również wprowadzić co najmniej dwa rodzaje neutratywów, mianowicie: - dla tych, którzy czują się neutralni płciowo, - dla tych, którzy nie chcą ujawniać płci. Jak już to się uda, trzeba będzie jeszcze zawalczyć o semimaskulinatywy i semifemininatywy, a więc końcówki i zaimki dla tych, którzy chcieliby językowo podkreślić swoją transpłciowość lub orientację seksualną, fakt posiadania jednego lub więcej partnerów, oraz poszukiwania kolejnych lub braku takiego zamiaru.  Potem może się okazać, że końcówki i zaimki będą musiały zawierać również informację o preferencjach religijnych, politycznych i dietetycznych, o wieku, sytuacji majątkowej, statusie społecznym, bliskości, zaufaniu lub szacunku do współrozmówcy itd. W pewnym momencie, przekaz językowy będzie zawierał więcej końcówek, zaimków i innych form wyrazu nivansów politycznych, że przekaz wszelkiej treści o charakterze techniczno-użytecznym całkowicie zaniknie. Zanim to jednak nastąpi, osoby ze spektrum autyzmu zostaną całkowicie wykluczone ze społeczeństwa, bo dla nich nawet rozróżnienie pomiędzy "na pan/pani i na ty" bywa kłopotliwe, a cóż dopiero dopasowanie się do coraz bardziej złożonych roszczeń osób, które chcą być inne, by zrobić na złość babci. Nie mówię zatem, że nie powinniśmy ułatwiać sobie życia przez reformy języka i eliminację zeń bagażu feudalno-patriarchalnego. Jak najbardziej, powinniśmy. Niemniej jednak, sugerowałbym rozwagę, by czasem nie trafić z deszczu pod rynnę jakiejś psychodelicznej, dystopicznej surrealokracji. Czasem dobrze jest kierować się lekarską zasadą: "przede wszystkim nie szkodzić". Poza tym: bez przesadyzmu. ](/img-local/blog/semimaskulinatywy.png)

Otóż, drogi autorze, żyjemy w językowej rzeczywistości, która wymusza na nas podawanie płci w wielu przypadkach, 
gdzie jest to informacja zupełnie zbędna. To nie jest nasz postulat, to nie jest coś, co my wprowadziłośmy,
to po prostu fakt o języku polskim. W normatywnej polszczyźnie musisz wybrać, czy jesteś „głodny” czy „głodna”,
czy może wolisz kombinować z przeformułowywaniem co drugiego zdania, by jako tako tego wyboru uniknąć (tu np. „chce mi się jeść”).
Musisz wybrać, czy „poszedłeś” czy „poszłaś” do sklepu, czy zwrócić się do obcej osoby „proszę pani” czy „proszę pana”.

Gdyby bez innowacji językowych dało się po polsku mówić zupełnie bez wskazywania na płeć, chętnie bym to robiło.
Istnieje wiele języków, które świetnie sobie radzą bez wrzucania płci w morfemy.
Gdy mówię po angielsku, wcale nie myślę sobie po cichutku „ech, gdyby tylko się dało zaznaczyć w tym zdaniu, że jestem niebinarne!”,
„och, dlaczego angielskie zaimki są upłciowione tylko w trzeciej osobie?!”.

Żyjemy w rzeczywistości, w której nie każdą osobę da się wrzucić [w jedną z raptem dwóch kategorii](/blog/ile-jest-płci).
Żyjemy też w rzeczywistości, w której nasz język wymaga zaznaczania tej kategorii, ale w powszechnym użyciu oferuje raptem dwie opcje
(a jedną z nich wręcz [traktuje jako domyślną](https://pl.wikipedia.org/wiki/Seksizm_j%C4%99zykowy)).
Problem jest oczywisty: dostępne nam narzędzie komunikacji nie przystaje do realnego świata i nie jest w stanie dobrze go opisać.
Nasze postulaty wprost odpowiadają na te bolączki: rozszerzmy system językowy tak, by każda osoba
(nie tylko kobiety i mężczyźni) mogła się w pełni w nim wyrazić, oraz skończmy z męską dominacją językową,
aby nasz język wreszcie włączał wszystkich.

O ile zgodzę się, że kwestie feminatywów i neutratywów są ze sobą ściśle związane
(w obu chodzi przecież o lepszą reprezentację językową osób o innej płci niż męska),
to dodawanie do systemu językowego morfemów determinujących takie rzeczy jak orientacja czy religia
byłoby zupełnym absurdem. Normatywna polszczyzna nie wymaga podawania takich informacji w podobny sposób,
w jaki wymaga podawania płci. Nikomu ten stan rzeczy nie przeszkadza, nikto nie postuluje, by jakkolwiek go zmieniać.

Że też ludziom nie wstyd wciąż używać [argumentu z równi pochyłej](https://yourlogicalfallacyis.com/slippery-slope)
i [bicia chochoła](https://yourlogicalfallacyis.com/strawman).
Nie postulujemy żadnej z rzeczy, o którą tak się boisz jako następnych kroków po normalizacji neutratywów.
Te rzeczy nie wynikają nijak z poprzednich. Nie istnieją problemy, które by rozwiązało stworzenie
osobnych zaimków dla biednych i bogatych 🤦

Jeśli twoim kontrargumentem wobec postulatów drugiej strony jest zmyślanie nieistniejących postulatów
i bezpodstawnych ciągów logicznych, to potrzebujesz lepszych kontrargumentów 😉
