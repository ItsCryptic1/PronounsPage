# Pinktober, Movember… Transcember! Ogłośmy grudzień miesiącem świadomości zdrowia osób transpłciowych

<small>2024-11-30 | [@Tess](/@Tess)</small>

![](/img-local/blog/transcember.png)

<section>
    <a href="https://transcember.net" target="_blank" class="btn btn-lg btn-primary d-block-force">
        <span class="fal fa-link"></span>
        Strona inicjatywy Transcember
    </a>
</section>
