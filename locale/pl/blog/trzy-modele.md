# Trzy modele płci

<small>2024-06-16 | [@andrea](/@andrea)</small>

![Ilustracja trzech modeli. Pierwsza część składa się z dwóch pudełek, różowego z rysunkiem sromu i symbolem ♀ oraz niebieskiego z rysunkiem penisa i symbolem ♂. Druga część składa się z pionowej linii przechodzącej w gradiencie od różowego (oznaczonego ♀) przez fioletowy (oznaczonego ⚧) do niebieskiego (oznaczonego ♂). Trzecia część to fioletowa ikona zaciśniętej pięści.](/img-local/blog/three-models.png)

<div class="alert alert-warning">
    <span class="fal fa-exclamation-triangle"></span>
    <strong>Ostrzeżenia dotyczące treści:</strong>
    wspomnienia o genitaliach, patriarchalnej opresji, queerfobicznych obelgach i przemocy
</div>

Przysłano nam ostatnio nam maila z prośbą o wyjaśnienie niektórych definicji z naszego [słownika terminologii queerowej](/terminologia).
I zdałom sobie przy tym sprawę, że trudno odpowiedzieć na to pytanie, nie odnosząc się najpierw do faktu,
że ludzie wciąż nie mogą się dogadać w kwestii tego, _czym właściwie jest płeć_.

Rzeczywistość jest, oczywiście, bardziej skomplikowana niż to, co zamierzam przedstawić, ale uważam,
że i tak warto rozróżnić trzy ogólne szkoły myślenia.

## Patriarchat

Patriarchalny system płci jest prosty i definitywny: **płeć = płeć genitalna przy urodzeniu**.

**Jeśli urodziłaś się ze sromem, jesteś kobietą**.
Nie możesz tego zmienić. Powinnaś być delikatna, opiekuńcza, posłuszna itp., powinnaś nosić sukienki, makijaż itp.,
powinnaś wyjść za mąż za jednego mężczyznę, być mu posłuszna, rodzić jego dzieci, wychowywać je,
gotować i sprzątać dla rodziny oraz wykonywać inne rodzaje pracy reprodukcyjnej.
**Jeśli urodziłeś się z penisem, jesteś mężczyzną**. Nie możesz tego zmienić. Powinieneś być odważny, silny, agresywny, zdecydowany itp.,
powinieneś nosić spodnie, interesować się samochodami, sportem i czym tam jeszcze, powinieneś wyjść za jedną kobietę,
zapłodnić ją, być głową rodziny, zapewniać jej środki do życia.
A **jeśli urodziłxś się z ambiwalentnymi genitaliami, trzeba cię „naprawić”**.
Jeśli chcesz być w romantycznym lub seksualnym związku z osobą tej samej płci, trzeba cię wyśmiać, ukarać, naprawić.
Jeśli nie chcesz wchodzić w żadne związki – lub chcesz wchodzić w wiele konsensualnych związków jednocześnie – coś jest z tobą nie tak.
Jeśli nie zrównujesz seksu z reprodukcją, jesteś dziwką i musisz się tego wstydzić.
Jeśli nie zrównujesz płci społecznej z biologicznymi atrybutami, jesteś trans dziwadłem niezasługującym na prawa.

Na szczęście, dzięki ruchowi sufrażystek, ruchowi feministycznemu, wyzwoleniu queerów i wielu innym walkom,
obserwujemy powolny upadek patriarchatu: rośnie społeczna akceptacja i prawne uznanie dla takich rzeczy jak
rozwód, aborcja, równość małżeńska, prawa osób transpłciowych itd. Ale ta walka jeszcze się nie zakończyła:
nie tylko niektóre obszary geograficzne mają się o wiele gorzej niż inne, nie tylko niektóre grupy mniejszościowe mają się o wiele gorzej niż inne,
ale nawet w rzekomo liberalnych i postępowych społeczeństwach wiele z dawnych, patriarchalnych sposobów myślenia wciąż jest obecne
w umysłach i uprzedzeniach ludzi…

## Spektrum płci

Często słyszymy to hasło: „płeć to spektrum”. I jest w tym niezaprzeczalna prawda:
są mężczyźni z pewnymi kobiecymi cechami, kobiety z cechami męskimi, oraz są osoby, których tożsamość lub ekspresja
leży gdzieś pomiędzy tymi dwoma patriarchalnymi archetypami.
Czy takie osoby są akceptowane i szanowane za to, kim są, czy raczej społecznie naciskane, by zbliżyły się do
przypisanego sobie końca spektrum, to kwestia ciągłej walki między patriarchatem a queerowym wyzwoleniem.

Osobiście nie jestem największem [fańczem](/neutratywy#fan) postrzegania płci jako spektrum – ponieważ [moja płeć](/terminologia#agender) się wcale na nim nie znajduje.
Spektrum jest użytecznym uproszczeniem, ale nie pełnym obrazem. Ale jednak jest to ogromny krok naprzód w porównaniu do ścisłego patriarchalnego binarizmu.
To zmiana od definiowania płci wyłącznie przez cechy biologiczne do bardziej zniuansowanego spojrzenia na wiele czynników,
do większej wolności w definiowaniu siebie. **Jesteś kobietą, jeśli mówisz, że jesteś kobietą. Jesteś mężczyzną, jeśli mówisz, że jesteś mężczyzną.
Jesteś tej płci, którą mówisz, że jesteś.**

Swoją drogą, uważam to za niezwykle ironiczne, że nawet najbardziej twardogłowi konserwatyści wydają się nie-wprost zgadzać, że płeć jest spektrum.
W końcu trudno jest dyskutować o tym, kto jest bardziej męski od innych mężczyzn, czy nazywać się „samcem alfa” lub „samcem sigma”,
nie przyznając jednocześnie, że bycie mężczyzną wymaga czegoś więcej niż posiadanie penisa przy narodzinach.
Choć ich ideał męskości i kobiecości jest nierealistyczny i szkodliwy,
ich desperackie próby utrzymania ludzi w patriarchalnych pudełkach dowodzą przynajmniej, że te pudełka nigdy nie były aż tak szczelne.

## Poza płcią

[Manifest Akceleracjonizmu Genderowego](/blog/accelerationizm-genderowy) definiuje płeć jako system utwierdzający
podział pracy reprodukcyjnej – innymi słowy: patriarchat. Osoby autorskie manifestu argumentują, że powinnośmy
**powiedzieć „nie” płci**. Po prostu nie uczestniczyć w systemie. Robić rzeczy, ponieważ _chcemy_ je robić,
a nie dlatego, że „jestem kobietą”, czy  bo „tak powinni postępować mężczyźni”. Bądźcie sobą, nie swoją „płcią”.

Płeć jest zmyślona. Chociaż [nie możemy udawać, że nie istnieje](/blog/czy-płeć-istnieje), bo w przeciwnym razie nie mogłobyśmy
odpowiednio adresować przemocy seksualnej, nierówności płci czy ataków na prawa reprodukcyjne, to możemy wyobrazić sobie idealny świat,
w którym wasze genitalia przy narodzinach mają tak mały wpływ na wasze ciało, strój, życie miłosne, seksualne, społeczne czy prawa człowieka,
co kolor waszych oczu. I możemy próbować zmieniać nasz świat tak, by był coraz bliżej tego ideału.

## Podsumowując

Linie między tymi trzema modelami są bardzo rozmyte. Mogę uznawać wpływ, jaki patriarchat wciąż ma na nasze postrzeganie płci,
jednocześnie odrzucać ten koncept, a także szczerze szanować tożsamości tych, którzy go nie odrzucają.
Często bronię spektrum i różnorodności etykietek, jednocześnie osobiście pragnąc, aby żadna z nich nie była już potrzebna.
Osoby mogą być płcią poza spektrum męskości i kobiecości, jednocześnie uznając jego istnienie i nie próbując obalić systemu.
Wiele kultur poza światem zachodnim faktycznie rozróżnia(ło) więcej niż dwie płcie,
ale czasami jest to bardziej płynny i swobodniejszy podział, a czasami to tylko większa liczba pudełek, które wciąż są bardzo restrykcyjne.

Ale mimo tych złożoności, uważam, że warto rozróżnić te modele.
Kiedy ktosio prosi mnie o zdefiniowanie „kobiety”, wiem, że istnieją przynajmniej trzy zupełnie różne sposoby spojrzenia na tę kwestię:
osoba urodzona ze sromem; osoba, która mówi, że jest kobietą; członkini klasy społecznej kontrolowanej przez mężczyzn.
Kiedy ktoś pyta mnie, jakiej jestem płci, w zależności od sytuacji mogę powiedzieć „żadną, jebać płeć” i zacząć
żywą dyskusję o queerowości, mogę po prostu powiedzieć „jestem niebinarne” i nie ciągnąć tematu,
albo mogę przewrócić oczami na [chujowo skonstruowany formularz](/formularze), dokonać mentalnych kalkulacji, którą z binarnych opcji
bardziej toleruję w danym kontekście, i poudawać danego dnia mężczyznę / kobietę.

Zasadniczo: większość pytań dotyczących płci, tożsamości i definicji nie ma prostych, jednoznacznych odpowiedzi;
możemy tego nie lubić, ale musimy to przyjąć do wiadomości tę rzeczywistość i rozważać każdy problem w szerszym kontekście.
