# Inkluzywna ortografia

<small>2024-04-25 | [@tymk](/@tymk)</small>

Brak inkluzywności w języku objawia się to nie tylko w mowie, ale też w sposobie jego zapisywania.
Nie lękaj się jednak, bo mam zamiar pokazać Ci, jak można temu zaradzić!

## 1. Osobatywy własne wielką czy małą literą?

Według współczesnych wytycznych przymiotniki własne należy pisać małą literą.
Natomiast, słowo „osoba” jako rzeczownik pospolity również należy pisać z małej.
Oznacza to, że osobatywy własne pisze się, normatywnie, małą literą np. „osoba polska”.
Odróżnia to je od maskulatywów i feminatywów i powoduje niechęć w używaniu ich przed strachem
o obrażenie [konoś](/ono/jeno) przez użycie małej litery. My jednak zachęcamy: **piszcie osobatywy własne dużą literą!**

Nie tylko te z „osobą”, ale też np. „Człowiek Praski” czy „Indywiduum Warszawskie”, czy w połączeniu ze zbioratywem, np. „Lud Ziemski”.
Zachęcamy do rozszerzenia tego zachowania również na inne rzeczowniki określane przez przymiotniki własne np. „Pracownice Krakowskie”, „Śmieciarze Wrocławscy”.

## 2. Kropka po niektórych skrótach tytułów

Tradycyjnie po skrótach zawodów kończących się na tę samą literę co sam zawód nie stawia się kropki – np. „dr”, „mgr”, „mjr” –
bo kropka ma oznaczać ominiętą końcówkę słowa.
**My zachęcamy jednak do stawiania po nich kropek**, bo przecież skrót „dr.” oznacza znacznie więcej niż tylko maskulatyw!
Kropka może oznaczać „doktorze”, „doktoru” albo nawet „osobę doktorską”.
W innych ortografiach świata to normalne, że po maskulatywie „dr” stawia się kropkę,
np. w niemieckim „Dr.” może oznaczać tak samo „Doktor”, jak i „Doktorin”.
Wykorzystajmy okazję, by zwiększając inklywność, jednocześnie uprościć polską ortografię:
zamiast zastanawiać się, czy dany skrót wymaga kropki czy nie, po prostu stawiajmy ją zawsze,
nawet przy maskulatywach, np. „mjr. dr. Kowalski”.

To by było na tyle, bądźcie sobą i nie bójcie się zmieniać języka, bo to Wy go tworzycie!
