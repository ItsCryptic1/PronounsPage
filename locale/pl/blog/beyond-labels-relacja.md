# Beyond Labels – relacja z Inkluzywnych Targów Pracy, Kraków 2024

<small>2024-10-13 | [@Tess](/@Tess)</small>

![](/img-local/blog/beyond-labels/beyond-labels-0.jpg)

**Powiedzieć, że targi Beyond Labels mnie zachwyciły, to nic nie powiedzieć. Dawno nie opuściłom żadnego miejsca z tak silnym poczuciem „chcę więcej”. Nie wiem, gdzie i kiedy odbędzie się czwarta edycja, ale już w myślach dopisuję ją do kalendarza.**

Słowem wstępu: Beyond Labels to pierwsze w Polsce Inkluzywne Targi Pracy, których trzecia edycja odbyła się w Krakowie, w Klastrze Innowacji Społeczno-Gospodarczych. Za organizację wydarzenia odpowiada Federacja Znaki Równości.

Targi trwały dwa dni, ale niestety udało mi się pojawić tylko w sobotę – szczerze żałuję, ponieważ ominęło mnie wiele interesujących rzeczy. Atmosfera wydarzenia była niesamowita – z jednej strony niezwykły profesjonalizm, z drugiej przytulność i poczucie przynależności.

{embed=//youtube.com/embed/dTZbXWaM2Tg=Beyond Labels WSPÓLNOŚĆ - trzecia edycja włączających targów pracy już 11 i 12/10 w Krakowie!}

Wydarzenie zostało podzielone na trzy bloki: strefę rozmów, w której odbywały się panele dyskusyjne oraz wykłady, strefę wystawową, na której w piątek prezentowały się inkluzywne firmy, a w sobotę – queerowe i sojusznicze organizacje, oraz strefę wytchnieniową, na której można było złapać oddech czy odprężyć się po przebodźcowaniu. Ponadto wszystkie osoby obecne na targach mogły zgłosić się na anonimowe, bezpłatne badania. Przy wejściu każda osoba otrzymywała wklepkę z miejscem na wpisanie imienia i zaimków; ponadto znajdował się tam plakat zachęcający do przedstawiania się z ich użyciem.

Choć Beyond Labels skierowane jest do wszystkich osób LGBTQIAP+, to było widać, że jest to wydarzenie mocno transinkluzywne oraz enbyinkluzywne – co w sumie wydaje się oczywiste w kontekście tematyki targów. Oczywiście są wśród nas osoby ze znakomitym passingiem, które mogą funkcjonować w pracy jako stealth (czyli uchodząc za osoby cispłciowe), ale osoby wyglądające w sposób nonkonformistyczny, niewpisujące się w cisheteronormatywne stereotypy płciowe czy po prostu korzystające z form innych niż ona/jej i on/jego będą zwracać uwagę, więc znalezienie inkluzywnego miejsca pracy jest dla nich szczególnie ważne.

Przyznam, że to niezwykle odświeżające i podnoszące na duchu – znaleźć się w tak barwnym gronie osób, które mogą wreszcie być sobą, bez oceniających spojrzeń, gdzie różowa broda i długa spódnica zestawione ze sobą nie budzą zdziwienia, tylko zachwyt. Znaleźć się w miejscu, gdzie pytanie o zaimki albo spojrzenie na identyfikator, by upewnić się, jakie są, jest zwyczajne i oczywiste. Jednocześnie było to miejsce o wiele bardziej przyjazne choćby dla osób neuroatypowych niż np. marsz równości, ponieważ nie pojawiało się przytłoczenie wywołane hałasem i tłumem, w dodatku zawsze można było udać się do strefy wyciszenia.

O tym, jak bardzo dopracowane było wydarzenie, mogą świadczyć drobne szczegóły: wklepki z zaimkami dla każdej osoby, plakaty „na tym wydarzeniu wszystkie toalety są unigender”, fakt, że choć realizowane były reportaże fotograficzne i wideo, to nie dotyczyły one strefy wytchnienia, aby obecne na niej osoby nie stresowały się obecnością mediów czy to, jak opracowane zostały identyfikatory. Z przodu znajdowały się imię, nazwisko oraz zaimki zaproszonej osoby, a z tyłu – program wydarzenia. To dobra i coraz częstsza praktyka, że na identyfikatorze umieszcza się przydatne informacje, np. numery telefonu do osób z organizacji, ale pierwszy raz spotkałom się z tym, że tył został wydrukowany… do góry nogami. Efekt: spoglądając na identyfikator zawieszony na szyi, widziało się całą treść w przystępny sposób bez konieczności zdejmowania go. Wydawałoby się, że to tak proste… a jednak wcześniej się z tym nie spotkałom, mimo uczestniczenia w licznych festiwalach, konferencjach, kongresach, targach czy konwentach.

Kolejną świetną rzeczą była różnorodność obecnych na miejscu organizacji: od TęczUJ, czyli koła studenckiego z wlepami finansowanymi z budżetu uczelni aż po Kolektyw Kamelia z hasłem: „Queer kurwy za dekryminalizacją naszych żyć”. Tak, na targach pracy pojawiło się hasło „Sex work is work” wraz z „Doświadczalnikiem” czy pakietami prezerwatyw rozdawanymi za darmo osobom pracującym seksualnie. Kolektyw Kamelia oferuje nieodpłatną pomoc prawną, konsultacje psychologiczne i ginekologiczne czy edukację z zakresu zdrowia seksualnego. Cieszę się, że okazało się, że Beyond Labels nie dzieli osób pracowniczych na lepsze i gorsze.

Sam program był niezwykle bogaty i z przyjemnością wysłuchałobym wszystkich paneli dyskusyjnych i wykładów. To tylko część poruszanych tematów: „Nieopowiedziane Queerstorie”, „Dobre praktyki w biznesie”,  „Jesteśmy w tym razem, panel dyskusyjny o związkach zawodowych” czy „ Być sobą w pracy – Queerowe Strategie Wspólności”. Samo pojawiłom się na panelu „Rozmowy o samorzecznictwie” prowadzonym przez Dawida Wojtyczke (on/jego, ona/jej), a oprócz mnie swoimi historiami podzielili się Grzegorz Żak (on/jego) z Fundacji Trans-Fuzja, Artur Kapturkiewicz (on/jego) z Fundacji Wiara i Tęcza oraz Michał Jabłoński (on/jego) prowadzący podcast Beza Wstydu.

[Z pełnym programem wydarzenia możecie zapoznać się tutaj.]( https://zaimki.pl/blog/beyond-labels)

Poszczególne punkty programu zostały nagrane i będą dostępne online, więc wszystkie zainteresowane osoby będą mogły się dowiedzieć, jak dokładnie wyglądała ta część wydarzenia.

Resztę czasu spędziłom przede wszystkim w strefie NGO-sów i znów – atmosfera była niesamowita. Okazało się, że zaimkowe wlepy dotarły tym razem niepocięte – zamawiałośmy je trochę na ostatnią chwilę. Owszem, przywiozłom ze sobą nożyczki, żeby móc docinać je na bieżąco, ale okazało się… że niemalże nie zdążyłom zacząć, a otrzymałom tyle pomocy, że lada moment wszystkie wlepy były pocięte. Absolutnym przodownictwem pracy wykazali się Remy (oni/ich), którzy wymiatali tak, że mam wrażenie, że pod stołem schowali gilotynę albo całą manufakturę, a także zorganizowali resztę wsparcia. Raz jeszcze dziękuję wszystkim osobom za pomoc i zaangażowanie. To był bardzo wzruszający gest, który sprawił, że naprawdę poczułom się częścią społeczności, w której rąk do wsparcia jest więcej niż nożyczek.

Wzruszające było też posłuchać osób, które na widok logo zaimków mówiły, że polecają nasza inicjatywę i dziękują za kawał dobrej roboty. Osób, które witały się, mówiąc, że już trzykrotnie brały udział w naszym Niebinarnym Spisie Powszechnym albo deklarowały, że czytały nasze relacje z prac nad ustawą o związkach partnerskich. Takie chwile naprawdę pokazują, że warto i że nie działamy w próżni.

Już po fakcie dociera do mnie, że z tyloma osobami widziałom się w biegu i nie udało nam się zrobić nawet selfie, a z częścią niestety się minęłośmy, ponieważ musiałom uciekać na pociąg. Mam nadzieję, że uda nam się nadrobić na kolejnej edycji, ponieważ nie wyobrażam sobie, żebym mogło ją opuścić. Tym razem stanę na rzęsach, ale postaram się być na całości!

Zarówno w czasie przygotowań do wydarzenia, jak i w jego trakcie, miałom stały kontakt z Dawidem Wojtyczką (on/jego, ona/jej) oraz Emanuelą Lewandowskx (onx/jex). Dziękuję Wam za ogrom pracy, zaangażowanie i serce. Było naprawdę świetnie!

Wspomnę też jeszcze o lokalizacji. Klaster Innowacji Społeczno-Gospodarczych mieści się pod adresem Zabłocie 22 w Krakowie, zaledwie trzy lub cztery przystanki od dworca Kraków Główny (do wyboru przystanki Zabłocie lub Klimeckiego, z bezpośrednim tramwajem jeżdżącym co 10 minut nawet w weekendy. W razie potrzeby zaledwie 700 metrów dalej mieści się stosunkowo tani hotel. Wszystko blisko i wygodnie. Uważam, że to ważne, ponieważ zdarza się, że fantastyczne wydarzenia odbywają się z trudno dostępnych lokalizacjach, co może zniechęcać do pojawienia się na nich. Nie wiem, czy w przyszłości targi Beyond Labels także będą odbywać się w Krakowie (druga edycja została zorganizowana w Gdańsku), ale jeśli tak, to podwójnie warto. Raz – ze względu na samo wydarzenie, dwa – ze względu na wygodny dostęp.

— Anna Tess Gołębiowska

{gallery={
    "/img-local/blog/beyond-labels/beyond-labels-1.jpg": "",
    "/img-local/blog/beyond-labels/beyond-labels-2.jpg": "",
    "/img-local/blog/beyond-labels/beyond-labels-3.jpg": "",
    "/img-local/blog/beyond-labels/beyond-labels-4.jpg": "",
    "/img-local/blog/beyond-labels/beyond-labels-5.jpg": "",
    "/img-local/blog/beyond-labels/beyond-labels-6.jpg": "",
    "/img-local/blog/beyond-labels/beyond-labels-7.jpg": "",
    "/img-local/blog/beyond-labels/beyond-labels-8.jpg": "",
    "/img-local/blog/beyond-labels/beyond-labels-9.jpg": "",
}}
