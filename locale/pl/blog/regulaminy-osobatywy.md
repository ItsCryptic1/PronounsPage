# Osobatywy w regulaminach?

<small>2024-08-21 | [@andrea](/@andrea)</small>

![Zdjęcie ilustracyjne: kartka papieru z napisem "Regulamin"](/img-local/blog/regulamin.png)

Zadano nam ostatnio bardzo ciekawe pytanie:

> Czy umieszczenie w regulaminie form neutralnych (osobatywów) może w jakikolwiek sposób wywrzeć skutek prawny,
> np. sprawić że regulamin będzie nieskuteczny?

Zacznijmy może od tego, że [osobatywy](/osobatywy) są formami normatywnymi:
w końcu to zwyczajnie słowo „osoba” połączone z odpowiednim imiesłowem lub przymiotnikiem.
Wielu osobatywów używamy w powszechnej mowie i nie widzimy w nich nic złego: np. „zaproszenie z osobą towarzyszącą” albo „osoba niebinarna”.
Zatem choć wiele osobatywów jest niewątpliwie formami rzadkimi i nietypowymi, to ciężko je nazwać „niepoprawnymi”.

Aby zapisy regulaminu były wiążące, muszą być zrozumiałe, precyzyjne i zgodne z przepisami prawa.
Naszym zdaniem, intencja tekstu prawnego używającego osobatywy jest jasna i jednoznaczna.
Ciężko nam sobie wyobrazić, żeby osoba dobrze znająca język polski i działająca w dobrej wierze
rzeczywiście nie miała pojęcia, co może oznaczać zapis „osoby wolontariackie są zobowiazane do XYZ”…
Jest to tym bardziej prawdziwe, jeśli chodzi o dokument sporządony przez organizację queerową lub sojuszniczą
i kierowany do społeczności queerowej, bardziej przecież otwartej na nietypowe formy językowe mające na celu inkluzywność.

Ale na wszelki wypadek możemy dołączyć do regulaminu objaśnienie, na przykład:

> W niniejszym regulaminie, wszystkie formy zawierające wyrażenie „osoba” w połączeniu z imiesłowem lub przymiotnikiem,
> takie jak „osoba wolontariacka”, „osoba uczestnicząca”, „osoba organizująca”, „osoba kucharska” itp.,
> należy rozumieć jako neutralne płciowo określenie odpowiadających im ról lub funkcji.
> W przypadku, gdy w regulaminie zastosowano taką formę, odnosi się ona do każdej osoby, która pełni wskazaną funkcję, niezależnie od płci,
> tożsamości płciowej czy preferowanych form gramatycznych.

Polskie prawo nie zabrania używania języka neutralnego płciowo ani nie narzuca używania konkretnych form gramatycznych, jak maskulatywy czy feminatywy.
Ba, polskie prawo nie widzi problemu w używaniu form męskich jako generycznych: 
zapis „wolontariusz jest zobowiązany do XYZ” stosuje się przecież również do wolontariuszek płci żeńskiej,
mimo że użyty w dokumencie rodzaj gramatyczny nie ozdwierciedla używanych przez nie zaimków osobowych.
Można by zatem argumentować, że osobatywy są precyzyjniejsze niż użyte generycznie maskulatywy!
W końcu lepiej ujmują w słowa intencje organizacji publikującej dokument:
związanie jego zapisami _wszystkich_ osób pełniących dane funkcje, niezależnie od ich płci.

Nie jesteśmy jednak osobami prawniczymi, nie jest nam też wiadomo o istnieniu jakichkolwiek postanowień sądów w tej kwestii.
Spytałośmy więc [Kolektwyw SZPIL(A)](https://www.instagram.com/kolektyw.szpila/) o potwierdzenie naszych przypuszczeń:

> Z naszej rozkminy wychodzi, że regulamin to jest wewnętrzny dokument,
> więc najlepiej jeśli będzie miał słowniczek czy wyjaśnienie, i nie widzimy żadnych przeszkód.

Także odważnie włączajmy wszystkie osoby w nasze regulaminy!
