# Obowiązujące prawo pozwala zmienić imię i nazwisko na używane — niezależnie od formalnej płci

<small>2024-04-30 | Hanna Talczewska</small>

![Zdjęcie decyzji o zmianie imienia, skupione na słowie „Decyzja”, reszta rozmyta](/img-local/blog/decyzja-zmiana-imienia.png)

<div class="alert alert-info">
    <p class="mb-0">
        <span class="fal fa-info-circle"></span>
        To jest wpis gościnny.
    </p>
</div>

Jak to zrobić?

Cały proces jest opisany przez [właściwą ustawę](https://isap.sejm.gov.pl/isap.nsf/download.xsp/WDU20082201414/U/D20081414Lj.pdf) — ma 6 stron i nie jest ciężką lekturą, więc warto się z nią zapoznać.

Potrzebujemy dla sukcesu spełnić bardzo proste warunki:

- udowodnić, że faktycznie używamy imienia i nazwiska, które chcemy przyjąć,
- że nie zachodzą żadne szczególne okoliczności, które by to wykluczały,
- że to stała przemyślana decyzja, a nie nierozsądny impuls.
- Często też niestety musimy poinformować Urząd, jakie jest prawo i dlaczego faktycznie ma zrobić, czego od niego chcemy, zamiast zepchnąć problem gdzie indziej.

Ustawa wylicza kilka możliwych ważnych powodów, ale lista jest otwarta — dla własnej pewności będziemy używać tego, które jest najbardziej wprost do udowodnienia, bez uznaniowości — zmiany imienia i nazwiska na faktycznie używane. Dzięki temu nie zmuszamy urzędników do decyzji czy nasze zdrowie (moim zdaniem ważny powód) naruszane deadnamingiem jest ważniejsze niż przyjęcie nazwiska konkubenta — które ważnym powodem nie jest. O zmianie na używane wiemy, że jest ważne, więc tego się trzymamy.

<aside class="card card-primary w-lg-50 float-lg-end m-3">
    <div class="card-body">
        <p>Zbieram na operację! Poświęciłam na otwarcie tych drzwi i przygotowanie tego poradnika dużo czasu i wysiłku — pomóżcie, proszę, żebym nie została przez to bardzo z tyłu z moją zbiórką.</p>
        <p>Można też wyłapać fajne torby i worko-plecaki z moją grafiką!</p>
        <a href="https://zrzutka.pl/cz42tk" target="_blank" rel="noopener" class="btn btn-outline-primary d-block-force my-4">
            <span class="fal fa-donate"></span>
            A żebym ocipiała! (zrzutka.pl)
        </a>
        <p>Przyda jej się aktualizacja, ale ten tekst był dla mnie priorytetem 🙂</p>
    </div>
</aside>

Jak udowodnić, jak się nazywamy? Dokumentami na nasze właściwie imię i nazwisko! Zeznania osób są niezerowym dowodem, ale dużo, dużo lepsza w oczach Urzędu jest biurokracja. Masz imię na grafiku z pracy? Cyk, do materiału dowodowego. Lekarz wystawił jakąś diagnozę na właściwe dane? Leci do teczki. Nie wystawił? Poproś o wydanie kopii, w moim doświadczeniu na prywacie problemów z tym nie było, NFZ też bez tragedii. Zapisz się na wydarzenie dotyczące swojego hobby, podpisz umowy, wykup domenę ze swoim nazwiskiem i na swoje nazwisko, wrzuć identyfikator ze szkoły czy pracy.

Nie jest to niezbędne, ale i tak zachęcam: jeśli jesteś w tranzycji medycznej i/lub jesteś w stanie potwierdzić jakimiś dowodami, że twój wygląd odbiega od kojarzonego z płcią imienia przypisanego — korzystaj z tego. Możesz napisać, że jesteś osobą transpłciową. Główną funkcją imienia i nazwiska ma być rozpoznawanie danej osoby, miano mylące nie spełnia swojej funkcji.

Z każdej kategorii spokojnie wystarczą 1-2 dowody – nie ma sensu rzucania teczką 40 kopii prawie tego samego zdania, tylko od innej osoby, czy też kolejną rozmową z messengera — wystarczy wskazać, że to coś, co istnieje, jak Urząd będzie potrzebować więcej, to nam o tym da znać.

## Co uniemożliwia zmianę nazwiska?

Sama ustawa wskazuje przesłankę negatywną dla zmiany nazwiska na historyczne, które nie występuje w naszej rodzinie — ok, wiemy żeby nie próbować brać sobie Mickiewiczów, Piłsudskich i Moczarów, ma nawet sens.

Dzięki osobie, która próbowała zmienić miano na „Herold Królestwa Teokracji Teokracjusz” mamy też wyrok<sup>1</sup> o treści możliwej do skrócenia do: „imię musi być imieniem, nawet jeśli nie musi być polskim imieniem z polską pisownią” — co może być przykrą wiadomością dla tych, którzy mogą chcieć mniej osobowego miana.

Sprawianie wrażenia kaprysu lub fanaberii. Pisanie, że chcemy tym zrobić komuś na złość, że ot tak nam przyszło. Ten punkt pokrywamy niejako już dobrze przygotowanym wnioskiem z historią użycia imienia, wystarczy sobie nie strzelać w stopę.

Wrzucenie za dużo — możemy mieć 2 imiona i dwuczłonowe nazwisko. Nie kombinujemy z „van”, „der”, „von” i podobnymi partykułami pochodzenia i szlachectwa, bo możemy trafić na grząski grunt dowodzenia co jest nazwiskiem a co mianem szlacheckim i czy nie byłyby próbą prostowania aktu urodzenia jako wskazujące na konkretne pochodzenie — nie potrzebujemy tego w swoim życiu, nie babrajmy się w tym.

I oczywiście nie zmienimy przy tym urzędowego oznaczenia płci — bo to kompletnie nie o tym, to całkiem inna sprawa. Nic o tym nie piszemy, nie prosimy o to — bo jakby urząd miał wrażenie, że to tego od niego chcemy, to zdecydowanie nasz wniosek powinien odrzucić.

Masz już to wszystko, masz wybrane imię, używasz go jakiś czas i jesteś w stanie to udowodnić? Super, teraz pozostaje zebrać to we wniosek dostosować do rzeczywistości urzędowej!

Na raz musimy wiedzieć 2 rzeczy:

 - Urząd jako instytucja wykonuje prawo. Niezależnie od osobistych opinii i odczuć, tam gdzie prawo opisuje, jaka decyzja zapada w jakich warunkach, to Urząd ma taką podjąć.
 - Urzędnik jest człowiekiem. Jako ludzie nie wiemy wszystkiego i generalnie staramy się nie wystawiać na złe konsekwencje.

Wychodząc od pytania: „Czy osoba transpłciowa może zmienić imię na imię innej płci?”, ktoś mógłby wpaść na przepisy z [Prawa o aktach stanu cywilnego](https://sip.lex.pl/akty-prawne/dzu-dziennik-ustaw/prawo-o-aktach-stanu-cywilnego-18148247/art-59) o nadawaniu imion niemowlętom przez rodziców. One faktycznie związują opcje płcią dziecka, zabraniają form zdrobniałych itp. – i dobrze, bo wiemy że inaczej ktoś by nazwał dziecko Bubunieczek albo X Æ A-12 — ale nie mogą być stosowane w wypadku osób świadomie wybierających swoje imię.

W ramach naszej działalności edukacyjnej możemy w swoim wniosku przytoczyć wyrok: 

> (…) regulacja ustawy z dnia 17 października 2008 r. o zmianie imienia i nazwiska (t.j. Dz.U. z 2020 r. poz. 707) dalej też „ustawa” jest kompletna i samodzielnie reguluje istnienie przesłanek prawnych odnoszących się do zmiany imion i nazwisk. Nie ma zatem potrzeby aby w tej procedurze stosować jako wyłączną ustawę Prawo o aktach stanu cywilnego. Takie stanowisko prezentowano już na tle ustawy z 15 listopada 1956 r. o zmianie imion i nazwisk w stosunku do ówcześnie obowiązującej ustawy z 29 września 1986 r. Prawo o aktach stanu cywilnego (por. wyrok WSA w Krakowie z 8 czerwca 2004 r. sygn. akt II SA/Kr 3199/00 oraz NSA oz. we Wrocławiu z 9 lipca 1993 r., sygn. akt SA/Wr 605/93).
>
> — Wyrok WSA w Opolu z 15.04.2021 r., II SA/Op 170/21, LEX nr 3184595

…który stwierdza, że w sprawie zmian imion urzędy się opierają wyłącznie o Ustawę o zmianie imion i nazwisk, nie o akt o imionach niemowląt. Można dodać, że „podobne stanowisko wyraził Minister Spraw Wewnętrznych i Administracji w jednej z decyzji wydanych w 2023 r. w toku postępowania prowadzonego w trybie szczególnym w przedmiocie stwierdzenia nieważności decyzji”.

Super, z tymi informacjami powinno być jasne, że interpretacja przepisów dla nas korzystna jest tą obowiązującą, notujemy to we wniosku i, hops, składać? Niekoniecznie! Poziom edukacji wśród urzędników Polski jest różny. W niektórych miejscach (gorąco pozdrawiam Wrocławskie USC z panią Aleksandrą na czele!) już dobrze wiedzą, jak wygląda prawo, i problemów nie będzie, ale jeśli chcemy zrobić wszystko pewnie i bezpiecznie, to mamy jeszcze jedną opcję zabezpieczenia.

Szykujemy swój wniosek, z dowodami na to, jakiego imienia i nazwiska używamy i wyraźnie wskazanym, że to w zasadzie jedyne, co się liczy — i udajemy się do naszego Urzędu Stanu Cywilnego po opinię, czy wniosek jest przygotowany poprawnie, czy potrzebuje uzupełnienia dowodów lub czy występują jakiekolwiek wątpliwości prawne. Można osobiście, można mailem, jak Wam pasuje. Dzięki temu dowiemy się, czy mogą być jakieś „ale” i będzie można uzupełnić braki w naszym wniosku, gdyby występowały.

Chcę Was bardzo zachęcać do próbowania w swoich miejscowych USC i dzielenia się wrażeniami ze społecznością i ze mną osobiście. Może się okazać, że mamy w Polsce dużo więcej sensownych i w porządku miejsc, niż nam się wydaje, a na etapie konsultacji spokojnie możemy skierować swój wniosek gdzie indziej.

[**Uzasadnienie**](https://docs.google.com/document/d/e/2PACX-1vSJ5Mx5AtTzwBX0ZDD-GMag0vfVTW3s3Ht_D7rNf3bcvM6vtjve-hRKGSJjBNDjrVB_VhQObHgRyL8d/pub) ws. mojego [**wniosku o zmianę imienia i nazwiska**](https://docs.google.com/document/d/e/2PACX-1vTwUAdXbb8IDGr3OQSVmIJiuz5LJ5evDjzBWP1vGxXPb9RoVaIPcv0h5Uqbr5JNOhZUT3EyaMyPjHlO/pub) jest dla Was bardzo pomocne, ponieważ wskazuje, które elementy były znaczące, a które okazały się mniej kluczowe. Bardzo zachęcam do przeczytania tego uzasadnienia, czerpania z niego pełnymi garściami. Uważam że może być dobrym materiałem pomocniczym, gdyby zaszła potrzeba edukowania naszych lokalnych urzędników.

Zachęcam do [**zadawania pytań**](https://www.facebook.com/hezort), możemy stworzyć razem jakieś FAQ, jeśli macie doświadczenia z tym podejściem – dzielcie się nimi! Razem możemy rozwiązywać rzeczy, które w pojedynkę wydają się nie do przejścia!

Dzięki za lekturę, zostawcie proszę reakcję i komentarz na ołtarzu algorytmu, a jeśli jest to może być temat bliski Waszym znajomym — udostępnijcie, dajcie im szansę się dowiedzieć. I w miarę swoich możliwości, pomóżcie mi, proszę — [A żebym ocipiała!]( https://zrzutka.pl/cz42tk)

<div class="alert alert-info">
    <span class="fal fa-microphone-alt fa-fw"></span>
    Wywiad z Hanną Talczewską przeczytacie <a href="/blog/hanna-talczewska"><strong>tutaj</strong></a>.
</div>

---

<sup>1</sup> „(…) posiadane imiona, nie muszą mieć formy odosobowej, mogą pochodzić od funkcji, nie muszą być słowiańskie ani chrześcijańskie, mogą być obce lub stanowić neologizmy ale pod pewnymi warunkami – swoją formą i pisownią nowe imiona, tak jak i nazwiska, powinny pozostawać w zgodzie z formą i pisownią imion, choć nie mają do nich, jako do nazw własnych, zastosowania przepisy ustawy o języku polskim”
