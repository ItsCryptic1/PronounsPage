# Stanowisko Kolektywu „Rada Języka Neutralnego” w konsultacjach publicznych rządowego projektu ustawy o związkach partnerskich

<small>2024-11-13 | [@Tess](/@Tess)</small>

![Zdjęcie slajdu prezentacji z tekstem: Związki partnerskie – założenia ustawy o rejestrowanych związkach partnerskich i ustawy wprowadzającej](/img-local/blog/kprm-2-5.jpg)

_Rządowe konsultacje projektu ustawy o związkach partnerskich trwają do 15 listopada 2024 roku. Zgłaszanie uwag jest możliwe poprzez opcję „[wyślij komentarz do projektu](https://legislacja.rcl.gov.pl/projekt/12390651/komentarz)” na portalu Rządowego Procesu Legislacyjnego lub mejlowo, pod adresem [konsultacje.zwiazki@kprm.gov.pl](mailto:konsultacje.zwiazki@kprm.gov.pl). Poniżej publikujemy stanowisko przesłane przez Kolektyw „Rada Języka Neutralnego”. Wszystkie osoby, które nie wysłały jeszcze uwag, zachęcamy do przesyłania opinii – możecie skopiować nasze stanowisko lub na jego podstawie zredagować własne._

_Możecie także skorzystać z ułatwień technologicznych, proponowanych m.in. przez [Miłość Nie Wyklucza](https://mnw.org.pl/media/konsultacje-zwiazkow-partnerskich-milosc-nie-wyklucza-zaprasza-do-pisania-do-rzadu) czy [Akcję Demokrację](https://kampania.akcjademokracja.pl/letters/zabierz-glos-w-konsultacjach-publicznych-ustawy-o-zwiazkach-partnerskich)._

_O kluczowych założeniach ustawy o rejestrowanych związkach partnerskich [piszemy tutaj](/blog/zwiazki-partnerskie-2)._

---

Szanowny Panie Premierze, Szanowna Pani Ministro Równości, Szanowni Ministrowie i Ministry, Posłowie i Posłanki,

jako Kolektyw „Rada Języka Neutralnego” chcemy wyrazić poparcie dla projektu ustawy o rejestrowanych związkach partnerskich z dnia 18 października 2024 r. (nr UD87 w wykazie prac legislacyjnych i programowych Rady Ministrów), a także dla projektu ustawy wprowadzającej ustawę o rejestrowanych związkach partnerskich (nr UD88 w wykazie prac legislacyjnych i programowych Rady Ministrów). Jako osoby niebinarne oraz sojusznicze zajmujące się tematyką niebinarności doceniamy, że ustawa umożliwia zawieranie związków bez względu na płeć osób partnerskich.

Prawdziwa równość wobec prawa nie będzie możliwa bez wprowadzenia równości małżeńskiej, ale rejestrowane związki partnerskie stanowią niezbędne minimum w respektowaniu praw osób LGBTQ+ w Polsce, jak również do realizowania przez Polskę wyroków Europejskiego Trybunału Praw Człowieka. Co więcej, niezależnie od potrzeby wprowadzenia równości małżeńskiej, w polskim systemie prawnym powinna istnieć forma związków odpowiednich dla osób, które bez względu na płeć nie chcą lub nie mogą zawrzeć małżeństwa.

Jednocześnie sądzimy, że ustawa o rejestrowanych związkach partnerskich powinna zostać rozszerzona o minimum dwie kwestie.

Pierwszą jest przysposobienie wewnętrzne. Według raportu „Tęczowe rodziny w Polsce” przez pary jednopłciowe jest wychowywanych około 50 tysięcy dzieci. Choć mała piecza jest formą minimalnego zabezpieczenia ich praw, to nadal nie jest to ochrona równa tej, która przysługuje dzieciom w rodzinach heteronormatywnych. Brak przysposobienia wewnętrznego oznacza dyskryminację dzieci wychowywanych w związkach jednopłciowych. Zdajemy sobie sprawę, że wprowadzenie małej pieczy zamiast przysposobienia wewnętrznego ma być formą kompromisu z konserwatystami, ale kompromisem jest już rezygnacja z adopcji – która odbiera wielu dzieciom szansę na prawdziwy dom i zmusza je do życia w systemie zamiast w kochającej rodzinie. To jednak kwestia teoretyczna. Dzieci wychowywane przez dwóch ojców i dwie matki w Polsce – to część naszej rzeczywistości. Jako społeczeństwo jesteśmy winni tym dzieciom ochronę, zapewnienie bezpieczeństwa i możliwości dorastania w szczęśliwej rodzinie. Mała piecza to najdogodniejsze rozwiązanie – w sytuacji, w której drugi rodzic biologiczny dziecka żyje i zachował pełnię praw rodzicielskich.

Druga kwestia to uregulowanie dostępu do uroczystej ceremonii zawarcia związku partnerskiego. Brak przepisów normujących tę kwestię otwiera furtkę do dyskryminowania tęczowych par przez konserwatywne osoby kierujące Urzędem Stanu Cywilnego. Brak ceremonii w wyraźny sposób lekceważy powagę wydarzenia oraz konsekwencji prawnych wiążących się z nim. Jest to dyskryminacja symboliczna, ale wciąż dotkliwa, pokazująca, w świetle polskiego prawa są osoby „równe” i „równiejsze”. Osoby o konserwatywnych poglądach, które za wszelką cenę chcą odróżnić się od osób queerowych, mogą bez problemu wziąć ślub kościelny. Urzędy Stanu Cywilnego powinny być otwarte na wszystkich, bez względu na orientację seksualną czy tożsamość płciową.

Ponadto uważamy, że skoro rząd nalega na utworzenie rejestrowanego związku partnerskiego jako instytucji odrębnej i różnej od małżeństwa, to związki takie powinny być formą zdecydowanie bardziej odważną i nowoczesną niż małżeństwa, a więc powinny umożliwiać sformalizowanie związku przez więcej niż dwie osoby. Osoby w relacjach poliamorycznych są niezauważane przez polski system prawny, a formuła związku innego niż małżeński pozwalałaby na dostrzeżenie ich potrzeb.

Mamy nadzieję, że rząd naszego kraju wywiąże się z obietnic wyborczych i będzie pracować nad Polską, w której wszystkie osoby będą miały zagwarantowane bezpieczeństwo, równość wobec prawa, godność i prawa człowieka.

Z poważaniem,  
Kolektyw „Rada Języka Neutralnego”, prowadzący portal zaimki.pl  
(w składzie: Anna Tess Gołębiowska-Kmieciak, Tomasz Vos-Malenkowicz, Andrea Vos, Archie Pałka, Tymoteusz Lisowski, Karolina Grenda, Sybil Grzybowski, Szymon Misiek)
