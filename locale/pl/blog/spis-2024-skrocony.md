# Niebinarny Spis Powszechny 2024 – skrócony raport

<small>2024-04-15 | [@szymon](/@szymon), [@andrea](/@andrea), [@tess](/@tess)</small>

<img src="/img-local/census/spis-2024.png" class="hero d-none" alt="">

Ponieważ raporty z kolejnych edycji Niebinarnego Spisu Powszechnego stają się coraz bardziej obszerne,
w tym roku postanowiłośmy przygotować także skróconą wersję prezentującą skrót najważniejszych wyników.
Po szczegółowe omówienie całości, wraz z wykresami porównującymi poszczególne podgrupy i zmiany na przestrzeni lat,
zapraszamy [**tutaj**](/blog/spis-2024).

{table_of_contents}

---

## Demografia

Próba: {json=spis-2024/general/stats.json=size} osób.
Średnia wieku wynosi {json=spis-2024/general/stats.json=ageStats.avg} lat,
mediana to {json=spis-2024/general/stats.json=ageStats.median} lat,
a odchylenie standardowe wynosi {json=spis-2024/general/stats.json=ageStats.std}.
{json=spis-2024/general/stats.json=ageStats.under_30}% osób jest przed trzydziestką,
a {json=spis-2024/general/stats.json=ageStats.adults}% to osoby pełnoletnie.

{graph=/docs-local/spis-2024/general/age}

## Nazewnictwo

### „Rodzaj nijaki” czy „rodzaj neutralny”?

W naszym materiałach [promujemy](/rodzaj-neutralny) nazwę „rodzaj neutralny” zamiast „rodzaj nijaki” ze względu na negatywne
konotacje „nijakości”. Zanim osoby badane zetknęły się z tą nazwą w dalszej częśći formularza, były pytane o preferowaną
przez nie nazwę dla form typu *byłom*, *byłoś*, *(ono) było*.

{json=spis-2024/general/stats.json=neuter.rodzaj neutralny}% osób opowiedziało się za „rodzajem neutralnym”.
Jedynie {json=spis-2024/general/stats.json=neuter.rodzaj nijaki}% woli „rodzaj nijaki”.
Dużo mniej popularne były odpowiedzi zaczerpnięte z zeszłorocznych dopisków: „rodzaj niebinarny” – {json=spis-2024/general/stats.json=neuter.rodzaj niebinarny}%
i „rodzaj łosiowy” (od końcówki drugiej osoby – *-łoś*) – {json=spis-2024/general/stats.json=neuter.rodzaj łosiowy}%.
{json=spis-2024/general/stats.json=neuter.nie mam zdania}% osób nie miało w tej kwestii zdania.
Pozostałe 0,5% nie wybrało żadnej z odpowiedzi, a jedynie dodało komentarz.

{graph=/docs-local/spis-2024/general/neuter}

## Używane formy

Zaznaczamy, że osoby respondenckie były pytanie o to, jak *chcą* mówić i jak *chcą* by się do nich zwracać – nawet jeśli nie jest to (obecnie) możliwe.
W dalszej części formularza pytałośmy też o powody nieużywania niestandardowych form w życiu.

<div class="alert alert-info small">
    <span class="fal fa-info-circle"></span>
    We wszystkich poniższych pytaniach możliwe było zaznaczenie więcej niż jednej odpowiedzi – dlatego wartości sumują się do <span class="text-nowrap">ponad stu procent</span>.
</div>

### Rodzaj gramatyczny używany w mowie

{graph=/docs-local/spis-2024/general/pronounGroups}

- Rodzaj męski (*byłem zmęczony*) – {json=spis-2024/general/stats.json=pronounGroups.rodzaj męski}%
- Rodzaj żeński (*byłam zmęczona*) – {json=spis-2024/general/stats.json=pronounGroups.rodzaj żeński}%
- Rodzaj neutralny (*byłom zmęczone*) – {json=spis-2024/general/stats.json=pronounGroups.rodzaj neutralny}%
- [Rodzaj postpłciowy / dukaizmy](/onu) (*byłum zmęczonu*) – {json=spis-2024/general/stats.json=pronounGroups.rodzaj postpłciowy}%
- Liczba mnoga, rodzaj męskoosobowy (*byliśmy zmęczeni*) – {json=spis-2024/general/stats.json=pronounGroups.liczba mnoga, rodzaj męskoosobowy}%
- Liczba mnoga, rodzaj niemęskoosobowy (*byłyśmy zmęczone*) – {json=spis-2024/general/stats.json=pronounGroups.liczba mnoga, rodzaj niemęskoosobowy}%
- [Liczba mnoga, rodzaj neutralny](/ona/ich) (*byłośmy zmęczone*) – {json=spis-2024/general/stats.json=pronounGroups.liczba mnoga, rodzaj neutralny}%
- [Liczba mnoga, rodzaj postpłciowy / mnogie dukaizmy](/ony) (*byłuśmy zmęczone*) – {json=spis-2024/general/stats.json=pronounGroups.liczba mnoga, rodzaj postpłciowy}%
- Unikanie form nacechowanych płciowo (*dopadło mnie zmęcznie*) – {json=spis-2024/general/stats.json=pronounGroups.unikanie form nacechowanych płciowo}%

{json=spis-2024/general/stats.json=pronounGroupsAggr.łącznie: wyłącznie binarne}% osób używa *wyłącznie* standardowych form (męskiej i/lub żeńskiej).
{json=spis-2024/general/stats.json=pronounGroupsAggr.łącznie: wyłącznie niebinarne}% – *wyłącznie* jednej lub kilku niestandardowych form.

### Zaimki używane w piśmie

<div class="alert alert-info small">
    <span class="fal fa-info-circle"></span>
    Przez „zaimki” rozumiemy tutaj <a href="/pytania#zaimki" target="_blank">skrót myślowy</a> obejmujący również korespondujące z nimi inne formy gramatyczne.
</div>

{graph=/docs-local/spis-2024/general/pronouns}

To pytanie oferowało więcej opcji do wyboru niż poprzednie: uwzględniono różne warianty zaimków korespondujących z jednym rodzajem
(np. [ono/jej](/ono/jej) i [ono/jejgo](/ono/jejgo) z rodzajem neutralnym czy różne warianty pojedynczych dukaizmów).
Zamiast opcji „unikanie form nacechowanych płciowo” dostępne były różne niewymawialne formy graficzne (np. [iksy](/onx)).

wartości dla rodzaju neutralnego, pojedynczych dukaizmów i form graficznych podano zbiorczo (tzn. tyle procent osób wybrało
co najmniej jedną formę z danej kategorii).

- Rodzaj męski (*on jest fajny, lubię jego zwierzątko*) – {json=spis-2024/general/stats.json=pronouns.on/jego}%
- Rodzaj żeński (*ona jest fajna, lubię jej zwierzątko*) – {json=spis-2024/general/stats.json=pronouns.ona/jej}%
- Rodzaj neutralny (np. *ono jest fajne, lubię jego zwierzątko*) (łącznie) – {json=spis-2024/general/stats.json=pronounsAggr.łącznie: neutralne}%
- [Rodzaj postpłciowy / dukaizmy](/onu) (np. *onu jest fajnu, lubię jenu zwierzątko*) (łącznie) – {json=spis-2024/general/stats.json=pronounsAggr.łącznie: postpłciowe}%
- Liczba mnoga, rodzaj męskoosobowy (*oni są fajni, lubię ich zwierzątko*) – {json=spis-2024/general/stats.json=pronouns.oni/ich}%
- Liczba mnoga, rodzaj niemęskoosobowy (*one są fajne, lubię ich zwierzątko*) – {json=spis-2024/general/stats.json=pronouns.one/ich}%
- [Liczba mnoga, rodzaj neutralny](/ona/ich) (*ona są fajne, lubię ich zwierzątko*) – {json=spis-2024/general/stats.json=pronouns.ona/ich}%
- [Liczba mnoga, rodzaj postpłciowy / mnogie dukaizmy](/ony) (*ony są fajne, lubię ich zwierzątko*) – {json=spis-2024/general/stats.json=pronouns.ony/ich}%
- Formy graficzne (np. *onx jest fajnx, lubię jex zwierzątko*) (łącznie) – {json=spis-2024/general/stats.json=pronounsAggr.łącznie: graficzne}%

<!--
{json=spis-2024/general/stats.json=pronounAggr.łącznie: wyłącznie binarne}% osób używa *wyłącznie* standardowych form (męskiej i/lub żeńskiej).
{json=spis-2024/general/stats.json=pronounAggr.łącznie: wyłącznie niebinarne}% – *wyłącznie* jednej lub kilku niestandardowych form (z wyłączeniem graficznych).
-->

### Formy rzeczownikowe

{graph=/docs-local/spis-2024/general/nouns}

- Maskulatywy (np. *autor*) – {json=spis-2024/general/stats.json=nouns.maskulatywy}%
- Feminatywy (np.*autorka*) – {json=spis-2024/general/stats.json=nouns.feminatywy}%
- [Neutratywy](/neutratywy) (np. *autorze*) – {json=spis-2024/general/stats.json=nouns.neutratywy}%
- [Dukatywy](/dukatywy) (np. *autoru*) – {json=spis-2024/general/stats.json=nouns.dukatywy}%
- [Iksatywy](/iksatywy) (np. *autorx*) – {json=spis-2024/general/stats.json=nouns.iksatywy}%
- [Osobatywy](/osobatywy) (np. *osoba autorska*) – {json=spis-2024/general/stats.json=nouns.osobatywy}%


### Formy grzecznościowe

{graph=/docs-local/spis-2024/general/honorifics}

Zdecydowanie najpopularniejszą odpowiedzią było **na „ty”**, jednak tylko
{json=spis-2024/general/stats.json=honorifics.łącznie: wyłącznie ty}%
zaznaczyło *wyłącznie* tę odpowiedź.

- „per ty” (*czy chcesz usiąść?*) – {json=spis-2024/general/stats.json=honorifics.„per ty”}%
- „per wy” (*czy chcecie usiąść?*) – {json=spis-2024/general/stats.json=honorifics.„per wy”}%
- pan (*czy chce pan usiąść?*) – {json=spis-2024/general/stats.json=honorifics.pan}%
- pani (*czy chce pani usiąść?*) – {json=spis-2024/general/stats.json=honorifics.pani}%
- państwo, l.poj. (*czy chce państwo usiąść?*) – {json=spis-2024/general/stats.json=honorifics.państwo (l\\. poj\\.)}%
- państwo, l.mn. (*czy chcą państwo usiąść?*) – {json=spis-2024/general/stats.json=honorifics.państwo (l\\. mn\\.)}%
- pań (*czy chce pań usiąść?*) – {json=spis-2024/general/stats.json=honorifics.pań}%
- panu (*czy chce panu usiąść?*) – {json=spis-2024/general/stats.json=honorifics.panu}%
- pano (*czy chce pano usiąść?*) – {json=spis-2024/general/stats.json=honorifics.pano}%
- osoba (*czy chce osoba usiąść?*) – {json=spis-2024/general/stats.json=honorifics.osoba}%

### „Konsekwentność” liczby mnogiej

Osoby, które w pytaniu o mowę zaznaczyły, że użwyają przynajmniej jednej z form mnogich zapytałośmy o szczegóły ich użycia.

Po pierwsze, czy „konsekwentnie” chcą stosowania form mnogich także w tych wyrażeniach, które nie są nacechowane
płciowo (np. czasowniki w czasie teraźniejszym – *jesteś*, *mówisz*).

{json=spis-2024/general/stats.json=pluralNonGendered.nie}% woli w tych przypadkach liczbę pojedynczą.
{json=spis-2024/general/stats.json=pluralNonGendered.tak}% chce użycia form mnogich we wszystkich przypadkach.
{json=spis-2024/general/stats.json=pluralNonGendered.wymiennie / bez różnicy / nie mam zdania}% nie ma preferencji.

Po drugie, czy „konsekwentnie” chcą być określane rzeczownikami w liczbnie mnogiej (np. *nauczyciele*, *pracownice*)
czy też w pojedynczej (np. *nauczyciel*, *pracownica*).

{json=spis-2024/general/stats.json=pluralNouns.nie}% chce być określane rzeczownikami w liczbnie pojedynczej.
{json=spis-2024/general/stats.json=pluralNouns.tak}% – rzeczownikami w liczbie mnogiej.
{json=spis-2024/general/stats.json=pluralNouns.wymiennie / bez różnicy / nie mam zdania}% nie ma preferencji.

{graph=/docs-local/spis-2024/general/pluralNonGendered}

{graph=/docs-local/spis-2024/general/pluralNouns}

### Zaimki w języku angielskim

{graph=/docs-local/spis-2024/general/english}

Pięć najpopularniejszych opcji:
- *they/them* – {json=spis-2024/general/stats.json=english.łącznie: they}%
- *he/him* – {json=spis-2024/general/stats.json=english.he/him}%
- *she/her* – {json=spis-2024/general/stats.json=english.she/her}%
- *it/its* – {json=spis-2024/general/stats.json=english.it/its}%
- *xe/xem* – {json=spis-2024/general/stats.json=english.xe/xem}%

## Motywacje

### Powody nieużywania form niebinarnych

Przypomnijmy, że pytania w Spisie dotyczą tego, jak osoby *chcą* mówić o sobie i być określane, nawet jeśli nie (zawsze) jest to możliwe.
Zapytałośmy je, co sprawia, że nie mogą używać niestandardowych form.

{json=spis-2024/general/stats.json=obstacles.nie chcę, pasują mi normatywne, binarne formy}% odpowiedziało, że po prostu pasują im
normatywne binarne formy. {json=spis-2024/general/stats.json=obstacles.nic, używam takich form}% – że używa form niestandardowych
bez przeszkód.

Wśród powodów nieużywania form niestandardowych najczęściej wybierano:
„męczy mnie tłumaczenie tematu, nie chcę ściągać uwagi na temat mojej płci” ({json=spis-2024/general/stats.json=obstacles.męczy mnie tłumaczenie tematu, nie chcę ściągać uwagi na temat mojej płci}%),
„strach przed transfobią/enbyfobią” ({json=spis-2024/general/stats.json=obstacles.strach przed transfobią/enbyfobią}%) oraz
„nie chcę sprawiać innym problemu, robić zamieszania” ({json=spis-2024/general/stats.json=obstacles.nie chcę innym sprawiać problemów, robić zamieszania}%).

{graph=/docs-local/spis-2024/general/obstacles}

### Powody wyboru form

Wśród powodów wyboru konkretnych form dominowały przyczyny „wewnętrzne”.

Najczęściej odpowiadano, że decydował „komfort, poczucie dopasowania, vibe” danej formy ({json=spis-2024/general/stats.json=reasons.komfort, poczucie dopasowania, vibe}%).
W dalszej kolejności: „używanie tych form daje mi euforię płciową” ({json=spis-2024/general/stats.json=reasons.używanie tych form daje mi euforię płciową}%)
oraz „łatwość użycia” ({json=spis-2024/general/stats.json=reasons.łatwość użycia}%)
i „przyzwyczajenie” ({json=spis-2024/general/stats.json=reasons.przyzwyczajenie}%).

{graph=/docs-local/spis-2024/general/reasons}

### Wybór imienia

{json=spis-2024/general/stats.json=namesAggr.łącznie: nadane}% osób używa w jakiejś formie imienia nadanego przez rodziców.
Najwięcej – {json=spis-2024/general/stats.json=names.nic, używam imienia nadanego mi przez rodziców}% – bez żadnych zmian;
{json=spis-2024/general/stats.json=names.jest neutralną/unisex formą imienia nadanego mi przez rodziców}% używa jego wersji neutralnej płciowo,
a {json=spis-2024/general/stats.json=names.jest formą przeciwnej płci binarnej imienia nadanego mi przez rodziców}% – wersji
kojarzonej z płcią „przeciwną” do przypisanej.

{json=spis-2024/general/stats.json=namesAggr.łącznie: wybrane}% wybrało inne imię.
{json=spis-2024/general/stats.json=names.wybrał_m neutralne/unisex imię niezwiązane z tym nadanym mi przez rodziców}%
deklaruje, że jest to imię niewskazujące na płeć; 
{json=spis-2024/general/stats.json=names.wybrał_m nacechowane binarnie imię niezwiązane z tym nadanym mi przez rodziców}% – że
jest to imię nacechowane binarnie, a
{json=spis-2024/general/stats.json=names.wybrał_m na imię rzeczownik, który tradycyjnie nie był używany jako imię}% – że
jest to rzeczywnik nieużywany tradycyjnie jako imię

Pozostałe 4,4% nie zaznaczyło żadnej z odpowiedzi, a jedynie dopisało własną – najczęściej, że używa kilku imion,
posługuje się nazwiskiem albo, że jeszcze nie jest pewnie jakiego imienia chce używać.

{graph=/docs-local/spis-2024/general/names}

## Język neutralny płciowo

### Opisywanie grup mieszanych

Pytanie dotyczyło tego, jak osoby badane [mówią o mieszanych płciowo grupach](/grupy).

- Normatywnych form męskoosobowocyh (*oni zrobili*) używa {json=spis-2024/general/stats.json=groups.rodzaj męskoosobowy}%.
- Form niemęskooosobowych/żeńskoosobowych (*one zrobiły*) używa {json=spis-2024/general/stats.json=groups.rodzaj niemęskoosobowy / żeńskoosobowy}%.
- [Neologicznej liczby mnogiej w rodzaju neutralnym](/ona/ich) (*ona zrobiły*) używa {json=spis-2024/general/stats.json=groups.rodzaj neutralny w liczbie mnogiej}%.
- [Neologicznych mnogich dukaizmów](/ony) (*ony zrobiły*) używa {json=spis-2024/general/stats.json=groups.rodzaj postpłciowy w liczbie mnogiej}%.
- [Neologicznych form z zaimkiem *onie*](/onie) (*onie zrobiły*) używa {json=spis-2024/general/stats.json=groups.neozaimek „onie”}%.

{graph=/docs-local/spis-2024/general/groups}

## Etykietki

### Etykietki opisujące płeć

5 najpopularniejszych odpowiedzi to terminy parasolowe:

- *nonbinary* – {json=spis-2024/general/stats.json=labelsGender.nonbinary}%
- *niebinarn_* – {json=spis-2024/general/stats.json=labelsGender.niebinarn_}%
- *enby* – {json=spis-2024/general/stats.json=labelsGender.enby}%
- *queer* – {json=spis-2024/general/stats.json=labelsGender.queer}%
- *osoba* – {json=spis-2024/general/stats.json=labelsGender.osoba}%

Najpopularniejsze terminy bardziej szczegółowo opisujące doświadczenie płci to
*agender* ({json=spis-2024/general/stats.json=labelsGender.agender}%)
i *genderfluid* ({json=spis-2024/general/stats.json=labelsGender.genderfluid}%).

{json=spis-2024/general/stats.json=labelsGender.łącznie: trans\*}% badanych określiło
się jako osoby transpłciowe, tj. wybrało przynajmniej jedną etykietkę z cząstką *trans-*.

{json=spis-2024/general/stats.json=labelsGender.łącznie: binarne}%
badanych osób wybrało „binarnie nacechowane” określenia (*kobieta*, *kobiec_*, *mężczyzna* i/lub *męsk_*).

Częściej wybierano terminy anglojęzyczne niż ich polskie odpowiedniki.

{graph=/docs-local/spis-2024/general/labelsGender}

| Angielska etykietka | %                    | Polska etykietka        | %                                                                         | Polska etykietka    | %            |
| ------------------- | -----                | ----------------------- |---------------------------------------------------------------------------| ------------------- | ----         |
| nonbinary           | {json=spis-2024/general/stats.json=labelsGender.nonbinary}%          | niebinarn\_             | {json=spis-2024/general/stats.json=labelsGender.niebinarn\_}%             |                     |                 |
| enby                | {json=spis-2024/general/stats.json=labelsGender.enby}%               | niebinie                | {json=spis-2024/general/stats.json=labelsGender.niebinie}%                |                     |                 |
| agender             | {json=spis-2024/general/stats.json=labelsGender.agender}%            | apłciow\_               | {json=spis-2024/general/stats.json=labelsGender.apłciow\_}%               | agenderow\_         | {json=spis-2024/general/stats.json=labelsGender.agenderow\_}%  |
| bigender            | {json=spis-2024/general/stats.json=labelsGender.bigender}%           | bipłciow\_              | {json=spis-2024/general/stats.json=labelsGender.bipłciow\_}%              |bigenderow\_            | {json=spis-2024/general/stats.json=labelsGender.bigenderow\_}% | 
| queer               | {json=spis-2024/general/stats.json=labelsGender.queer}%              | kłir                    | {json=spis-2024/general/stats.json=labelsGender.kłir}%                    |                     |                 |
| androgyne           | {json=spis-2024/general/stats.json=labelsGender.androgyne}%          | androgyniczn\_          | {json=spis-2024/general/stats.json=labelsGender.androgyniczn\_}%          |                     |                 |
| aporagender         | {json=spis-2024/general/stats.json=labelsGender.aporagender}%        | aporapłciow\_           | {json=spis-2024/general/stats.json=labelsGender.aporapłciow\_}%           | aporagenderow\_     | {json=spis-2024/general/stats.json=labelsGender.aporagenderow\_}%  |
| autygender          | {json=spis-2024/general/stats.json=labelsGender.autygender}%         | autypłciow\_            | {json=spis-2024/general/stats.json=labelsGender.autypłciow\_}%            | autygenderow\_      | {json=spis-2024/general/stats.json=labelsGender.autygenderow\_}%  |
| demigender          | {json=spis-2024/general/stats.json=labelsGender.demigender}%         | demipłciow\_            | {json=spis-2024/general/stats.json=labelsGender.demipłciow\_}%            | demigenderow\_      | {json=spis-2024/general/stats.json=labelsGender.demigenderow\_}%  |
| demigirl            | {json=spis-2024/general/stats.json=labelsGender.demigirl}%           | demidziewczę            | {json=spis-2024/general/stats.json=labelsGender.demidziewczę}%            |                     |                 |
| demiboy             | {json=spis-2024/general/stats.json=labelsGender.demiboy}%            | demichłopię             | {json=spis-2024/general/stats.json=labelsGender.demichłopię}%             |                     |                 |
| maverique           | {json=spis-2024/general/stats.json=labelsGender.maverique}%          | maweryczn\_             | {json=spis-2024/general/stats.json=labelsGender.maweryczn\_}%             |                     |                 |
| xenogender          | {json=spis-2024/general/stats.json=labelsGender.xenogender}%         | ksenopłciow\_           | {json=spis-2024/general/stats.json=labelsGender.ksenopłciow\_}%           | ksenogenderow\_     | {json=spis-2024/general/stats.json=labelsGender.ksenogenderow\_}%  |
| neutrois            | {json=spis-2024/general/stats.json=labelsGender.neutrois}%           | neutralnopłciow\_       | {json=spis-2024/general/stats.json=labelsGender.neutralnopłciow\_}%       | neutralnogenderow\_ | {json=spis-2024/general/stats.json=labelsGender.neutralnogenderow\_}%  |
| genderqueer         | {json=spis-2024/general/stats.json=labelsGender.genderqueer}%        | nienormatywn\_ płciowo  | {json=spis-2024/general/stats.json=labelsGender.nienormatywn\_ płciowo}%  |                     |                 |
| pangender           | {json=spis-2024/general/stats.json=labelsGender.pangender}%          | panpłciow\_             | {json=spis-2024/general/stats.json=labelsGender.panpłciow\_}%             | pangenderow\_       | {json=spis-2024/general/stats.json=labelsGender.pangenderow\_}%  |
| genderfluid         | {json=spis-2024/general/stats.json=labelsGender.genderfluid}%        | płynnopłciow\_          | {json=spis-2024/general/stats.json=labelsGender.płynnopłciow\_}%          | płynnogenderow\_    | {json=spis-2024/general/stats.json=labelsGender.płynnogenderow\_}%  |
| genderflux          | {json=spis-2024/general/stats.json=labelsGender.genderflux}%         | zmiennopłciow\_         | {json=spis-2024/general/stats.json=labelsGender.zmiennopłciow\_}%         | zmiennogenderow\_   | {json=spis-2024/general/stats.json=labelsGender.zmiennogenderow\_}%  |
| gender questioning  | {json=spis-2024/general/stats.json=labelsGender.gender questioning}% | rozważając\_ swoją płeć | {json=spis-2024/general/stats.json=labelsGender.rozważając\_ swoją płeć}% |                     |                 |
| transfeminine       | {json=spis-2024/general/stats.json=labelsGender.transfeminine}%      | transkobiec\_           | {json=spis-2024/general/stats.json=labelsGender.transkobiec\_}%           | trans kobieta       | {json=spis-2024/general/stats.json=labelsGender.trans kobieta}%  |
| transmasculine      | {json=spis-2024/general/stats.json=labelsGender.transmasculine}%     | transmęsk\_             | {json=spis-2024/general/stats.json=labelsGender.transmęsk\_}%             | trans mężczyzna     | {json=spis-2024/general/stats.json=labelsGender.trans mężczyzna}%  |
| transgender         | {json=spis-2024/general/stats.json=labelsGender.transgender}%        | transpłciow\_           | {json=spis-2024/general/stats.json=labelsGender.transpłciow\_}%           | transgenderow\_     | {json=spis-2024/general/stats.json=labelsGender.transgenderow\_}%  |
| trans               | {json=spis-2024/general/stats.json=labelsGender.trans}%              |                         |                                                                           |                     |                                               |
| transneutral        | {json=spis-2024/general/stats.json=labelsGender.transneutral}%       | transneutraln\_         | {json=spis-2024/general/stats.json=labelsGender.transneutraln\_}%         |                     |                 |
| trigender           | {json=spis-2024/general/stats.json=labelsGender.trigender}%          | tripłciow\_             | {json=spis-2024/general/stats.json=labelsGender.tripłciow\_}%             | trigenderow\_       | {json=spis-2024/general/stats.json=labelsGender.trigenderow\_}%  |
|                     |                                                                      | kobiec\_                | {json=spis-2024/general/stats.json=labelsGender.kobiec\_}%                | kobieta             | {json=spis-2024/general/stats.json=labelsGender.kobieta}%  |
|                     |                                                                      | męsk\_                  | {json=spis-2024/general/stats.json=labelsGender.męsk\_}%                  | mężczyzna           | {json=spis-2024/general/stats.json=labelsGender.mężczyzna}%  |

### Etykietki opisujące seksualność

Najczęściej wybierano określenie *queer* – {json=spis-2024/general/stats.json=labelsSexuality.queer}%.

Drugą najliczniejszą grupą były osoby czujące pociąg do więcej niż jednej płci – {json=spis-2024/general/stats.json=labelsSexuality.łącznie: mspec}%
W tej grupie najczęściej wybierano etykietkę *biseksualn_* ({json=spis-2024/general/stats.json=labelsSexuality.biseksualn_}%).

Trzecią najliczniejszą grupą były osoby na spektrum aseksualności – {json=spis-2024/general/stats.json=labelsSexuality.łącznie: aspec}%.
W tej grupie najczęściej wybierano etykietkę *aseksualn_* ({json=spis-2024/general/stats.json=labelsSexuality.aseksualn_}%).

Jako *homoseksualne* określiło się {json=spis-2024/general/stats.json=labelsSexuality.homoseksualn_}% osób,
a jako *heteroseksualne* – {json=spis-2024/general/stats.json=labelsSexuality.heteroseksualn_}%.

{graph=/docs-local/spis-2024/general/labelsSexuality}

### Etykietki opisujące romantyczność

{json=spis-2024/general/stats.json=labelsAttractionSplit.tak}% badanych osób podało,
że rozdziela orientację seksualną od romantycznej. Spośród nich:

Najwięcej osób określiło swoją orientację romantyczną jako queer – {json=spis-2024/general/stats.json=labelsRomantic.queer}%

Drugą najliczniejszą grupą były osoby czujące pociąg romantyczny do więcej niż jednej płci – {json=spis-2024/general/stats.json=labelsRomantic.łącznie: mspec}%.
W tej grupie najcześćiej wybierano określenie *panromantyczn_* ({json=spis-2024/general/stats.json=labelsRomantic.panromantyczn_}%).

Trzecią najliczniejszą grupą były osoby na spektrum aromantyczności – 18,90%.
W tej grupie najcześćiej wybierano określenie *aromantyczn_* ({json=spis-2024/general/stats.json=labelsRomantic.aromantyczn_}%).

Jako *homoromantyczne* określiło się {json=spis-2024/general/stats.json=labelsRomantic.homoromantyczn_}% osób,
a jako *heteroromantyczne* – {json=spis-2024/general/stats.json=labelsRomantic.heteroromantyczn_}%.

{graph=/docs-local/spis-2024/general/labelsRomantic}

## Proces tranzycji

W tym roku zdecydowałośmy się wyjść naprzeciw licznym prośbom, by rozszerzyć ankietę o kwestie pozajęzykowe – mianowicie
kilka nieobowiązkowych pytań dotyczących procesu tranzycji. 
{json=spis-2024/general/stats.json=transtionAnswered.tak}% osób respondenckich zdecydowało się z tej możliwości skorzystać.
Te {json=spis-2024/general/stats.json=transtionAnswered.tak}% przekłada się na {json=spis-2024/general/stats.json=size_transition} osób –
i właśnie do tej grupa będzie stanowić punkt odniesienia w analizie wszystkich pytań dotyczących tranzycji.

Niestety, pierwszy raz to też okazja do nauki: powinnośmy były poświęcić więcej uwagi konstruowaniu pytań,
ponieważ w trakcie przeglądania dopisków okazało się, że wkradły się nam dwa błędy spowodowane tym,
że do każdego z pytań użyłośmy niemal tych samych opcji wyboru, mimo że niektóre rodzaje tranzycji wymagałyby
raczej bardziej rozbudowanych opcji. Dane, które zebrałośmy, wciąż mogą być użyteczne, nawet jeśli nie są do końca jasne,
dlatego zdecydowałośmy się je opublikować z następującymi zastrzeżeniami:
 
- W pytaniu o tranzycję hormonalną znaczenie opcji „już przeszł_m proces” nie jest do końca jasne,
  ponieważ hormony przeważnie bierze się przez całe życie. Zdecydowanie lepszym sformułowaniem byłoby na przykład
  „już osiągnęł_m zamierzane efekty” – co lepiej oddaje znaczenie, jakie chciałośmy, by ta opcja miała.
  Niestety, nie jesteśmy w stanie zagwarantować, że właśnie tak została ona odebrana przez osoby respondenckie, 
  dlatego wartość może być zaniżona.
- W pytaniu o tranzycję prawną w zakresie znacznika płci sprawę komplikuje fakt, że w Polsce
  jedynymi dostępnymi opcjami są K i M – i choć wiele osób niebinarnych jak najbardziej może chcieć zmienić
  swój znacznik na jeden z tych dwóch, nawet jeśli byłaby dostępna trzecia opcja
  (np. ze względu na przykład na bliższe identyfikowanie się z daną binarną płcią albo chcąc uniknąć potencjalnej dyskryminacji),
  to jednak na przykład pod opcją „nie zamierzam przechodzić zmiany” kryją sytuacje, których rozróżnienie
  jest bardzo istotne: niektóre spośród osób, które zaznaczyły tę opcję, nie zamierzają zmiany znacznika przechodzić wcale,
  podczas gdy inne rozważyłyby tę możliwość, gdyby był dla nich dostępny znacznik X lub N.

Przepraszamy za niedociągnięcia, obiecujemy je poprawić w przyszłej edycji – i prosimy brać odpowiednią poprawkę na poniższe wyniki.

### Wnioski ogólne dotyczące tranzycji

Poszczególne elementy tranzycji cieszą się różną popularnością wśród społeczności osób niebinarnych i poszukujących.
Dla ułatwienia opisu badań podzieliłośmy odpowiedzi odpowiedzi na grupy: negatywną
(„nie zamierzam przechodzić tranzycji”, „przechodzę/przeszł_m detranzycję”),
neutralną („jeszcze nie wiem”, „nie chcę odpowiadać”)
oraz pozytywną („jestem w trakcie”, „już przeszł_m proces”, „zamierzam zacząć w przyszłości”, „stosuję microdosing”, „w części miejsc tak, w części nie”).

<div class="wide-escape">
    <div class="table-responsive">
        <table class="table table-striped table-balanced">
            <thead>
                <tr>
                    <th rowspan="2"></th>
                    <th rowspan="2">tranzycja społeczna</th>
                    <th colspan="2">tranzycja prawna</th>
                    <th colspan="3">tranzycja medyczna</th>
                </tr>
                <tr>
                    <th>zmiana imienia</th>
                    <th>zmiana znacznika płci</th>
                    <th>niewymagająca pomocy lekarskiej</th>
                    <th>terapia hormonalna</th>
                    <th>interwencje chirurgiczne</th>
                </tr>
            </thead>
            <tbody>
                <tr class="accent-success">
                    <th>już przeszł_m proces</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocial.już przeszł_m proces}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionName.już przeszł_m proces}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarker.już przeszł_m proces}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysical.już przeszł_m proces}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonal.już przeszł_m proces}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgical.już przeszł_m proces}%{/optional}</td>
                </tr>
                <tr class="accent-success">
                    <th>jestem w trakcie</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocial.jestem w trakcie}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionName.jestem w trakcie}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarker.jestem w trakcie}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysical.jestem w trakcie}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonal.jestem w trakcie}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgical.jestem w trakcie}%{/optional}</td>
                </tr>
                <tr class="accent-success">
                    <th>stosuję microdosing</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocial.stosuję microdosing}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionName.stosuję microdosing}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarker.stosuję microdosing}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysical.stosuję microdosing}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonal.stosuję microdosing}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgical.stosuję microdosing}%{/optional}</td>
                </tr>
                <tr class="accent-success">
                    <th>w części miejsc tak, w części nie</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocial.w części miejsc tak, w części nie}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionName.w części miejsc tak, w części nie}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarker.w części miejsc tak, w części nie}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysical.w części miejsc tak, w części nie}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonal.w części miejsc tak, w części nie}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgical.w części miejsc tak, w części nie}%{/optional}</td>
                </tr>
                <tr class="accent-success">
                    <th>zamierzam zacząć w przyszłości</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocial.zamierzam zacząć w przyszłości}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionName.zamierzam zacząć w przyszłości}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarker.zamierzam zacząć w przyszłości}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysical.zamierzam zacząć w przyszłości}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonal.zamierzam zacząć w przyszłości}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgical.zamierzam zacząć w przyszłości}%{/optional}</td>
                </tr>
                <tr class="accent-warning">
                    <th>jeszcze nie wiem</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocial.jeszcze nie wiem}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionName.jeszcze nie wiem}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarker.jeszcze nie wiem}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysical.jeszcze nie wiem}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonal.jeszcze nie wiem}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgical.jeszcze nie wiem}%{/optional}</td>
                </tr>
                <tr class="accent-warning">
                    <th>nie chcę odpowiadać</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocial.nie chcę odpowiadać}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionName.nie chcę odpowiadać}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarker.nie chcę odpowiadać}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysical.nie chcę odpowiadać}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonal.nie chcę odpowiadać}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgical.nie chcę odpowiadać}%{/optional}</td>
                </tr>
                <tr class="accent-danger">
                    <th>nie zamierzam</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocial.nie zamierzam}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionName.nie zamierzam}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarker.nie zamierzam}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysical.nie zamierzam}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonal.nie zamierzam}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgical.nie zamierzam}%{/optional}</td>
                </tr>
                <tr class="accent-danger">
                    <th>przechodzę/przeszł_m detranzycję</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocial.przechodzę/przeszł_m detranzycję}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionName.przechodzę/przeszł_m detranzycję}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarker.przechodzę/przeszł_m detranzycję}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysical.przechodzę/przeszł_m detranzycję}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonal.przechodzę/przeszł_m detranzycję}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgical.przechodzę/przeszł_m detranzycję}%{/optional}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="table-success">
                    <th>Ogólnie pozytywne</th>
                    <td><strong>{optional}{json=spis-2024/general/stats.json=transitionSocialSentiment.positive}%{/optional}</strong></td>
                    <td><strong>{optional}{json=spis-2024/general/stats.json=transitionNameSentiment.positive}%{/optional}</strong></td>
                    <td><strong>{optional}{json=spis-2024/general/stats.json=transitionMarkerSentiment.positive}%{/optional}</strong></td>
                    <td><strong>{optional}{json=spis-2024/general/stats.json=transitionPhysicalSentiment.positive}%{/optional}</strong></td>
                    <td><strong>{optional}{json=spis-2024/general/stats.json=transitionHormonalSentiment.positive}%{/optional}</strong></td>
                    <td><strong>{optional}{json=spis-2024/general/stats.json=transitionSurgicalSentiment.positive}%{/optional}</strong></td>
                </tr>
                <tr class="table-warning">
                    <th>Ogólnie neutralne</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocialSentiment.neutral}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionNameSentiment.neutral}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarkerSentiment.neutral}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysicalSentiment.neutral}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonalSentiment.neutral}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgicalSentiment.neutral}%{/optional}</td>
                </tr>
                <tr class="table-danger">
                    <th>Ogólnie negatywne</th>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSocialSentiment.negative}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionNameSentiment.negative}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionMarkerSentiment.negative}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionPhysicalSentiment.negative}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionHormonalSentiment.negative}%{/optional}</td>
                    <td>{optional}{json=spis-2024/general/stats.json=transitionSurgicalSentiment.negative}%{/optional}</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

Zdecydowanie najpopularniejsza okazała się tranzycja społeczna – 
odpowiedzi pozytywne to aż {json=spis-2024/general/stats.json=transitionSocialSentiment.positive}%
w zestawieniu z zaledwie {json=spis-2024/general/stats.json=transitionSocialSentiment.negative}% negatywnych.
Popularnością cieszy się też tranzycja medyczna niewymagająca pomocy lekarskiej:
{json=spis-2024/general/stats.json=transitionPhysicalSentiment.positive}% odpowiedzi pozytywnych
w porównaniu z {json=spis-2024/general/stats.json=transitionPhysicalSentiment.negative}% odpowiedzi negatywnych.

Niemal połowa osób respondenckich ({json=spis-2024/general/stats.json=transitionNameSentiment.positive}%)
dała pozytywną odpowiedź na pytanie o tranzycję prawną w zakresie zmiany imienia,
przy {json=spis-2024/general/stats.json=transitionNameSentiment.negative}% odpowiedzi negatywnych.
Podobnie wygląda zainteresowanie tranzycją medyczną w zakresie chirurgii
({json=spis-2024/general/stats.json=transitionSurgicalSentiment.positive}% odpowiedzi pozytywnych
i {json=spis-2024/general/stats.json=transitionSurgicalSentiment.negative}% negatywnych)
oraz hormonalnej terapii zastępczej
({json=spis-2024/general/stats.json=transitionHormonalSentiment.positive}% odpowiedzi pozytywnych
i {json=spis-2024/general/stats.json=transitionHormonalSentiment.negative}% negatywnych)

Najmniej popularna okazała się tranzycja prawna polegająca na zmianie znacznika płci:
{json=spis-2024/general/stats.json=transitionMarkerSentiment.negative}% odpowiedzi pozytywnych
w porównaniu do {json=spis-2024/general/stats.json=transitionMarkerSentiment.negative}% negatywnych –
jednocześnie ten wynik może być zafałszowany ze względu na nieprecyzyjnie sformułowane pytanie.
Osoby, które wybrały „nie zamierzam” mogły kierować się brakiem prawnej możliwości zmiany znacznika płci na X lub N w Polsce,
ponieważ nie są zainteresowane zmianą znacznika na ten oznaczający inną płeć binarną.

### Tranzycja społeczna

> Tranzycja społeczna obejmuje zmiany takie jak zmiana sposobu ubierania się, noszenie makijażu,
> używanie nowych imion i zaimków, a także informowanie rodziny, bliskich czy miejsca nauki/pracy
> o swojej tożsamości płciowej.

{graph=/docs-local/spis-2024/general/transitionSocial}

### Tranzycja prawna: zmiana imienia

{graph=/docs-local/spis-2024/general/transitionName}

### Tranzycja prawna: znacznik płci

{graph=/docs-local/spis-2024/general/transitionMarker}

### Tranzycja medyczna: niewymagająca pomocy lekarskiej

> To pytanie obejmuje zmiany takie jak depilacja, ćwiczenie głosu, noszenie binderów, packerów…

{graph=/docs-local/spis-2024/general/transitionPhysical}

### Tranzycja medyczna: terapia hormonalna

{graph=/docs-local/spis-2024/general/transitionHormonal}

### Tranzycja medyczna: interwencje chirurgiczne

{graph=/docs-local/spis-2024/general/transitionSurgical}

## Porównanie z poprzednimi edycjami

Forma                        | 2021                                                                  | 2022                                                                                 | 2023                                                                                 | 2024                                                                                 |
-----------------------------|-----------------------------------------------------------------------|--------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|
[Rodzaj neutralny](/ono)     | {json=spis-2021/general/stats.json=pronounGroups.rodzaj neutralny}%   | {json=spis-2022/general/stats.json=pronounGroups.rodzaj neutralny}%                  | {json=spis-2023/general/stats.json=pronounGroups.rodzaj neutralny}%                  | {json=spis-2024/general/stats.json=pronounGroups.rodzaj neutralny}%                  |                 
[Rodzaj postpłciowy](/onu)   | {json=spis-2021/general/stats.json=pronounGroups.rodzaj postpłciowy}% | {json=spis-2022/general/stats.json=pronounGroups.rodzaj postpłciowy}%                | {json=spis-2023/general/stats.json=pronounGroups.rodzaj postpłciowy}%                | {json=spis-2024/general/stats.json=pronounGroups.rodzaj postpłciowy}%                |               
Wyłącznie formy binarne      | 53.6%                                                                 | {json=spis-2022/general/stats.json=pronounGroupsAggr.łącznie: wyłącznie binarne}%    | {json=spis-2023/general/stats.json=pronounGroupsAggr.łącznie: wyłącznie binarne}%    | {json=spis-2024/general/stats.json=pronounGroupsAggr.łącznie: wyłącznie binarne}%    |   
Wyłącznie formy niebinarne   | 8.4%                                                                  | {json=spis-2022/general/stats.json=pronounGroupsAggr.łącznie: wyłącznie niebinarne}% | {json=spis-2023/general/stats.json=pronounGroupsAggr.łącznie: wyłącznie niebinarne}% | {json=spis-2024/general/stats.json=pronounGroupsAggr.łącznie: wyłącznie niebinarne}% |
[Neutratywy](/neutratywy)    | {json=spis-2021/general/stats.json=nouns.neutratywy}%                 | {json=spis-2022/general/stats.json=nouns.neutratywy}%                                | {json=spis-2023/general/stats.json=nouns.neutratywy}%                                | {json=spis-2024/general/stats.json=nouns.neutratywy}%                                |                               

## Ogólne wnioski

Wyniki naszego badania pokazują przede wszystkim **ogromną różnorodność** społeczności osób niebinarnych:
choć statystycznie jesteśmy młode, to istnieją niebinia w każdym wieku;
choć klaruje się standard niebinarnego języka, to używamy również wielu innych form, bardzo często też tych binarnych;
nazywamy swoją płeć i orientację seksualną i romantyczną na wiele różnych sposobów;
część z nas nazywa się osobami trans, część nie; część używa imienia nadanego, część wybiera własne…
Nie jesteśmy monolitem – nasza piękna i bogata różnorodność jest naszą siłą. 

Dane z Niebinarnego Spisu Powszechnego malują obraz społeczności, która swoją tożsamość dokładnie analizuje
i nieustannie odkrywa, która przy wyborze używanego języka kieruje się bardziej czynnikami wewnętrznymi niż zewnętrznymi –
ale też **boi się odrzucenia** ze strony społeczeństwa i **stara się nie prowokować** otoczenia, często kosztem własnego szczęścia.
Nie przepadamy za formalnościami, w bezpiecznych przestrzeniach chętnie rozmawiamy o naszej tożsamości i procesie tranzycji,
a by się opisać, wciąż chętniej sięgamy po zapożyczenia z angielskiego niż ich spolszczenia.

Dane z Niebinarnego Spisu Powszechnego zwracają też uwagę na **kwestie ważne dla społeczności**:
jak ułatwienie procesu tranzycji prawnej i medycznej, udostępnienie trzeciej opcji znacznika płci,
czy zastąpienie nazwy „rodzaj nijaki” przez „rodzaj neutralny”.

Porównując wyniki [pierwszego](/spis-2021), [drugiego](/spis-2022) i [trzeciego](/spis-2023) spisu,
można zaobserować bardzo **zdecydowany zwrot w stronę nienormatywnych form językowych**:
znacznie wzrosła liczba osób wybierających rodzaj neutralny i znacznie spadła liczba tych,
które wybierają jedynie formy męskie i żeńskie. Mamy nadzieję, że nasza działalność choć trochę przyczyniła się
do odważniejszego wyrażania swojej tożsamości przez osoby niebinarne i częstszego sięgania po formy, które odpowiadają
ich potrzebom. Obecnie trendy te zdają się stabilizować, zmiany są mniej drastyczne niż na początku badania.

Bardzo nas cieszy **rosnąca popularność form rodzaju neutralnego** oraz fakt, że coraz więcej osób niebinarnych decyduje się
na **odważniejsze używanie form niebinarnych**.
Co ciekawe, temu trendowi nie towarzyszy podobny wzrost popularności neutratywów; 
ta od początku utrzymuje się na poziomie kilkunastu procent, z niewielkimi wahaniami.
Jesteśmy bardzo ciekawe, jak sytuacja będzie rozwijać się w przyszłości. Coraz więcej osób deklaruje,
że używa (lub chciałoby używać) _wyłącznie_ niestandardowych, „niebinarnych” form.

Popularność form rodzaju neutralnego może być sygnałem dla osób pisarskich, tłumaczących i dziennikarskich, by nie
obawiały się ich używać w swoich tekstach (niemniej, ze względu na potencjalnie przykre skojarzenia warto wyjaśnić
tę kwestię we wstępie czy przypisie, choćby odwołując się do naszego badania).

W przypadku form rzeczownikowych, najbezpieczniejsze wydają się osobatywy, zwłaszcza jeśli nie mamy możliwości zapytać
danej osoby, jak o niej mówić/pisać lub też nie mamy pewności, jak utworzyć neutratyw od danego słowa. Jako kolektyw
niezmiennie zachęcamy do używania neutratywów, choć zaznaczamy, że – póki co – same osoby niebinarne nie sięgają po nie tak często.

## Kolejna edycja!

Następny Niebinarny Spis Powszechny odbędzie się **w lutym 2025**.

[Pod tym linkiem](/spis) możesz zapisać się na **mailową przypominajkę** 😉

## Cytowanie

Jeśli cytujesz gdzieś nasz raport, prosimy [skontaktuj się z nami](/kontakt) i daj znać, gdzie i jak 😉

{details=Jak cytować?}

- **MLA**: Misiek, Szymon, Andrea Vos i Anna Tess Gołębiowska-Kmieciak. 2024. „Niebinarny Spis Powszechny 2024 – skrócony raport.” _Zaimki.pl_. Kolektyw „Rada Języka Neutralnego.” \[publikacja internetowa] https://zaimki.pl/blog/spis-2024
- **APA**: Misiek. Szymon, Andrea Vos i Anna Tess Gołębiowska-Kmieciak (2024) „Niebinarny Spis Powszechny 2024 – skrócony raport.” _Zaimki.pl_. Kolektyw „Rada Języka Neutralnego.” https://zaimki.pl/blog/spis-2024
- **Chicago**: Misiek, Szymon, Andrea Vos i Anna Tess Gołębiowska-Kmieciak. „Niebinarny Spis Powszechny 2024 – skrócony raport.” _Zaimki.pl_, Kolektyw „Rada Języka Neutralnego”, 2024, https://zaimki.pl/blog/spis-2024

{/details}
