# Odświeżone, wektorowe flagi

<small>2025-01-01 | [@andrea](/@andrea)</small>

![](/img-local/blog/odświeżone-flagi.png)

My queery uwielbiamy nasze flagi, no nie? 😅

Odezwało się do nas ostatnio [@Śreżoga](/@Śreżoga) z propozycją odświeżenia flagi [neutratywów i rodzaju neutralnego](/neutratywy).
Flaga ta przedstawia łosia trzymającego łom – ze względu na końcówki czasowników w [rodzaju neutralnym](/rodzaj-neutralny): „-łom”, „-łoś” i „-ło”.
Według rozmów z jeno znajomymi, łoś na fladze wygląda „ponuro”, „na trochę zagubionego”, „jakby szedł komuś złożyć nieprzyjemną wizytę” 😅
Fakt, że ciężko było nam znaleźć odpowiednią grafikę, którą mogłobyśmy użyć.
W oryginalnym designie [@ausir](/@ausir) użył [znalezionej na wikimediach grafiki](https://commons.wikimedia.org/wiki/File:NO_road_sign_146.1.svg)
kroczącego łosia z norweskiego znaku ostrzegawczego.
Teraz Śreżoga podsesłało szkic pomysłu z użyciem łosia z stylu heraldycznego trzymacza,
dzięki czemu wygląda on dumniej i przyjaźniej (choć łom z natury jest ciężko przedstawić nie-groźnie, więc _całkiem_ przyjaźnie i tak nie da rady 😅).

Znalazłom wektorowego heraldycznego łosia na otwartej licencji w [herbie Ontario](https://commons.wikimedia.org/wiki/File:Coat_of_arms_of_Ontario,_Canada.svg),
skleiłom nowy design i spytałom o Wasze opinie na naszych socjalkach
[<svg class="icon" viewBox="-50 -50 700 630" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="m135.72 44.03c66.496 49.921 138.02 151.14 164.28 205.46 26.262-54.316 97.782-155.54 164.28-205.46 47.98-36.021 125.72-63.892 125.72 24.795 0 17.712-10.155 148.79-16.111 170.07-20.703 73.984-96.144 92.854-163.25 81.433 117.3 19.964 147.14 86.092 82.697 152.22-122.39 125.59-175.91-31.511-189.63-71.766-2.514-7.3797-3.6904-10.832-3.7077-7.8964-0.0174-2.9357-1.1937 0.51669-3.7077 7.8964-13.714 40.255-67.233 197.36-189.63 71.766-64.444-66.128-34.605-132.26 82.697-152.22-67.108 11.421-142.55-7.4491-163.25-81.433-5.9562-21.282-16.111-152.36-16.111-170.07 0-88.687 77.742-60.816 125.72-24.795z" fill="currentColor"/></svg>](https://bsky.app/profile/zaimki.pl/post/3lekaf6imib24)
[<span class="fab fa-mastodon"></span>](https://kolektiva.social/@neutratywy/113743359269500584)
[<span class="fab fa-twitter"></span>](https://x.com/neutratywy/status/1873809275207770303)
[<span class="fab fa-facebook"></span>](https://www.facebook.com/neutratywy/posts/904432608509054)
[<span class="fab fa-instagram"></span>](https://www.instagram.com/p/DENrq5ER0Vr/).
Te, jak to w kwestiach gustu, są podzielone – ale ogólnie odzew był mocno pozytywny 🎉

Uznałom jednak, że zanim wrzucę nową flagę na stronę, warto być pójść za ciosem i odświeżyć też pozostałe dwie flagi
stworzone przez nasz kolektyw: [dukatywów i dukaizmów](/dukatywy) oraz [iksatywów](/iksatywy).

Oryginalne wersje są w formacie rastrowym, przez co ciężko je skalować do większych rozmiarów oraz wprowadzać zmiany
jak przesunięcie elementów by móc dodać tekst na [baner](/blog/łosie-masowej-zagłady).
Nowe flagi są wektorowymi plikami SVG, pozbawionymi tych wad.
W przeszłości nie trzymałośmy też konsekwentnych proporcji wymiarów flag (neutratywowa była ciut szersza niż pozostałe), co teraz zostało naprawione.

W nowej wersji duszek z flagi dukatywów dostał trochę przeźroczystości, jak na duszka przystało, a napis otrzymał czcionkę spójną z designem strony.

Flaga iksatywów, która dotychczas zawiarała tylko zwyczajną literę „x”, otrzymała trochę więcej charakteru: proporcje 1:1, cieńsze, ostrzejsze wnętrze, a grubsze końcówki.

Oto i nowe flagi!

<div class="row">
    <div class="col-12 col-lg-4">
        <h5>Neutratywy i rodzaj neutralny</h5>
        <figure class="mb-0">
            <img src="/img-local/flags/neutratywy.svg" alt="Flaga osób niebinarnych z naniesionym łosiem trzymającym łom." class="mb-3 shadow">
            <figcaption>
                <p>
                    Ze względu na końcówki „-łom”, „-łoś” i „-ło”, flaga rodzaju neutralnego i neutratywów przedstawia łosia z łomem.
                </p>
            </figcaption>
        </figure>
        <div class="small px-3 py-0">
            <p class="mb-2">
                <span class="fal fa-user-friends"></span>
                Autorstwo:
            </p>
            <ul>
                <li>Oryginalny projekt: <a href="/@ausir">@ausir</a></li>
                <li>Pomysł redesignu: <a href="/@Śreżoga">@Śreżoga</a></li>
                <li>Realizacja redesignu: <a href="/@andrea">@andrea</a></li>
                <li>Flaga osób niebinarych <a href="https://commons.wikimedia.org/wiki/File:Nonbinary_flag.svg" target="_blank" rel="noopener">Kye Rowan</a> (public domain)</li>
                <li>Wektor łosia: <a href="https://commons.wikimedia.org/wiki/File:Coat_of_arms_of_Ontario,_Canada.svg" target="_blank" rel="noopener">Herb Ontario</a> (public domain)</li>
                <li>Wektor łoma: <a href="https://www.vecteezy.com/free-vector/crowbar" target="_blank" rel="noopener">Vecteezy</a></li>
            </ul>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <h5>Dukatywy i dukaizmy</h5>
        <figure class="mb-0">
            <img src="/img-local/flags/dukatywy.svg" alt="Flaga osób niebinarnych z naniesionym duszkiem krzyczącym „-łu”." class="mb-3 shadow">
            <figcaption>
                <p>
                    Ze względu na końcówki „-łum”, „-łuś” i „-łu”, flaga dukazimów i dukatywów przedstawia duszka krzyczącego „łu!”.
                </p>
            </figcaption>
        </figure>
        <div class="small px-3 py-0">
            <p class="mb-2">
                <span class="fal fa-user-friends"></span>
                Autorstwo:
            </p>
            <ul>
                <li>Oryginalny projekt: <a href="/@ausir">@ausir</a></li>
                <li>Projekt: <a href="/@andrea">@andrea</a></li>
                <li>Flaga osób niebinarych <a href="https://commons.wikimedia.org/wiki/File:Nonbinary_flag.svg" target="_blank" rel="noopener">Kye Rowan</a> (public domain)</li>
            </ul>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <h5>Iksatywy</h5>
        <figure class="mb-0">
            <img src="/img-local/flags/iksatywy.svg" alt="Flaga osób niebinarnych z naniesioną literą „x”." class="mb-3 shadow">
        </figure>
        <div class="small px-3 py-0">
            <p class="mb-2">
                <span class="fal fa-user-friends"></span>
                Autorstwo:
            </p>
            <ul>
                <li>Projekt: <a href="/@andrea">@andrea</a></li>
                <li>Flaga osób niebinarych <a href="https://commons.wikimedia.org/wiki/File:Nonbinary_flag.svg" target="_blank" rel="noopener">Kye Rowan</a> (public domain)</li>
            </ul>
        </div>
    </div>
</div>
