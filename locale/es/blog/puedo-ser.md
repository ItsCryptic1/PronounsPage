# ¿Puedo ser...

<small>2022-11-19 | [@andrea](/@andrea) | Traducido el 2024-06-18 por [@Kaix0](/@Kaix0)</small>

![Capturas de pantalla de títulos de las publicaciones en reddit (en inglés), todas citadas abajo](/img-local/blog/can-i-be.png)

Recientemente he notado que muchas publicaciones en algunos subreddits que sigo son básicamente peticiones de validación a la identidad propia.
Aquí unos pocos ejemplos (traducidos) de sólo uno de ellos ([r/Nonbinary](https://reddit.com/r/Nonbinary)):

 - _“¿Se puede ser [AMAN](https://pronombr.es/terminologia#Asignade%20mujer%20al%20nacer) y muy femenine y aún así ser no binarie?”_,
 - _“¿¿¿Puedo ser heterosexual y aún así ser no binarie???”_,
 - _“¿¿Puedo todavía más o menos pensar en mí mismo como un chico pero aún así ser no binarie??”_,
 - _“¿Puedo ser de [género fluido](https://pronombr.es/terminologia#g%C3%A9nero%20fluido) y ser no binarie?”_
 - _“¿Puedo ser no binarie pero aún así disfrutar mucho mi apariencia masculina? Soy nueve en esto 😅”_

Cuando leo un título como estos, sencillamente giro los ojos y pienso: _¡Por supuesto que puedes, tonte!_ 😉

Entiendo de dónde vienen esas preguntas. Vivimos en una sociedad patriarcal, alocisheteronormativa.
Hay esquemas qué seguir. Reglas qué obedecer. Permisos que otorgar.

Un hombre cis hetero nunca se ha preguntado "¿Puedo ser hombre, casarme con una mujer, y tener muches hijes?"
– porque eso es exactamente lo que la sociedad espera de él, ese es el esquema que ve en las películas, en libros, seguido por celebridades…
Sólo cuando no sigues el Verdadero Camino de la Vida™ te tienes que preocupar de qué pensarán les otres.
Nuestra sociedad nos enseña que necesitamos pedir permiso para ser quienes somos;
que no podemos ser quienes somos hasta que quienes nos rodean lo juzgan válido.

Y queer significa decir: _a la mierda con eso_.

No necesitas permiso para ser tú.
El cómo te ves y el cómo te expresas no tienen por qué coincidir con las expectativas de nadie, se
basen en tu género asignado al nacer, nombre, etiquetas, pronombres, o lo que sea.
Tus etiquetas no tienen que tener sentido para otres.
Claro que son herramientas útiles para comunicar ideas, para aproximar significados –
pero al final del día tus etiquetas son _tuyas_.

Sí, puedes ser un chico no binario. Incluso si alguien más te dice que no puedes, su opinión no te hará dejar de ser un chico no binario.
Puedes ser une enebe AMAN y femenine. Puedes ser hetero y no binarie. Puedes ser hombre y mujer al mismo tiempo.
Tú eres quien mejor sabe quién eres, cómo es tu relación con el género, quiénes te atraen…
Tú encontraste las palabras que mejor describen lo que sientes – así que siéntete en la libertad de… usar esas palabras para describir lo que sientes.
¡Para eso es que están ahí!

Podemos tener muchas experiencias en común con otras personas, pero, créeme, nadie siente _exactamente_ lo que tú sientes.
El género es complejo. También lo es la atracción, sea en el plano sexual, romántico o platónico.
Podemos tratar de poner esos conceptos dentro de definiciones simples y rígidas, pero siempre habrá alguien que, sencillamente… no cabrá en ellas.

Y está bien.

No necesitas permiso para ser queer.
