locale: 'zh'

header: true

pronouns:
    enabled: true
    route: 'pronouns'
    default: '他'
    any: '任何'
    plurals: false
    honorifics: false
    generator:
        enabled: true
        slashes: false
    multiple:
        name: '可互換形式'
        description: '很多非二元性別的人使用多個代詞，不介意他人互換。'
        examples: ['他&她', '她&伊', '他&佢']
    null: false
    emoji:
        description: '表情符號代詞'
        history: '表情符號代詞用於網絡上的交流，沒有發音。'
        morphemes:
            3rd_person: '#'
            2nd_person: '#'
        examples: ['💫', '💙']
    others: 'Other pronouns'

pronunciation:
    enabled: true
    voices:
        CN:
            language: 'cmn-CN'
            voice: 'Zhiyu'
            engine: 'standard'

sources:
    enabled: true
    route: 'sources'
    submit: true
    mergePronouns: {}

nouns:
    enabled: false
    route: 'dictionary'
    collapsable: false
    plurals: true
    pluralsRequired: false
    declension: false
    submit: true
    templates:
        enabled: false

inclusive:
    enabled: false

terminology:
    enabled: true
    published: false # TODO
    categories:
        -
            key: '性吸引倾向'
            text: '性吸引倾向'
        -
            key: '浪漫（吸引）倾向'
            text: '浪漫（吸引）倾向'
        -
            key: '第三吸引倾向'
            text: '第三吸引倾向'
        -
            key: '性别'
            text: '性别'
        -
            key: '性别表达'
            text: '性别表达'
        -
            key: '关系模型'
            text: '关系模型'
        -
            key: '语言'
            text: '语言'
        -
            key: '吸引'
            text: '吸引'
        -
            key: '政治'
            text: '政治'
        -
            key: '偏见'
            text: '偏见'
    route: '术语'

names:
    enabled: false

people:
    enabled: false

english:
    enabled: false

faq:
    enabled: false
    route: 'faq'

links:
    enabled: false
    route: 'links'
    blogRoute: 'blog'
    academic: {}
    links:
        -
            icon: 'comment-alt-edit'
            lang: ['en']
            url: 'https://www.genderinlanguage.com/spanish'
            headline: 'Gender in Language Project'
    mediaGuests: []
    mediaMentions: []
    recommended: []
    blog: false

contact:
    enabled: true
    route: 'contact'
    authors:
        -
            footerName: '台灣非二元酷兒浪子'
            link: 'https://www.facebook.com/TaiwanNonbinary/'
            group: true
            footerAreas: '譯員'
        -
            footerName: '曹志豪 (離退休)'
            username: 'zhihao'
            footerAreas: '離退休譯員'
    team:
        enabled: true
        route: 'team' # TODO

support:
    enabled: true

user:
    enabled: true
    route: 'account'
    termsRoute: 'terms'
    privacyRoute: 'privacy' # TODO

profile:
    enabled: true
    editorEnabled: true
    defaultWords:
        -
            header: '稱謂'
            values: ['沒有尊稱', '君', '先生', '小姐', '夫人', '同志', '太太']
        -
            header: '個人及家庭描述詞'
            values: ['男', '女', '哥哥/弟弟', '姐姐/妹妹', '朋友']
        -
            header: '讚美用詞'
            values: ['可愛', '美麗', '性感', '好看', '帥哥']
        -
            header: '關係用詞'
            values: ['男朋友', '女朋友', '知音', '丈夫', '妻子']
    flags:
        defaultPronoun: '他'

census:
    enabled: false

redirects: []

api: ~
