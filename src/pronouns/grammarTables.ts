export interface Header {
    name: string;
    short: string;
}

export interface GrammarTable {
    columnHeader: Header[];
    sections: SectionDefinition[];
}

export interface SectionDefinition {
    header?: Header;
    variants: PronounVariantDefinition[] | PronounVariantsFromDeclensionDefinition;
}

export interface PronounVariantDefinition {
    name?: string;
    morphemeCells: (string | MorphemeCellDefinition)[];
}

export interface PronounVariantsFromDeclensionDefinition {
    base: string;
    type: string;
}

export interface MorphemeCellDefinition {
    morpheme: string;
    highlightsMorphemes?: string[];
    prepend?: string;
}

export interface PronounVariant {
    name?: string;
    numerus?: 'singular' | 'plural';
    icon?: string;
    morphemeCells: MorphemeCell[];
}

export interface MorphemeCell {
    morpheme: string;
    highlightsMorphemes?: Set<string>;
    prepend?: string;
}
