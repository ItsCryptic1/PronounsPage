export type Color = 'primary' | 'secondary' | 'success' | 'danger' | 'warning' | 'info' | 'light' | 'dark';

export type ModalSize = 'sm' | 'lg' | 'xl' | undefined;
