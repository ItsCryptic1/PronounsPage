import type { IncomingMessage } from 'connect';
import type { Request } from 'express';

export default (err: Error, req: Request | IncomingMessage | undefined): string => {
    return `[${new Date().toISOString()}] [${req ? `${req.method} ${req.url}` : ''}] ${err.message || err} ${err.stack}`;
};
