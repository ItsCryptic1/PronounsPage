export const usernameRegex = /^[\p{L}\p{N}._-]+$/gu;
export const usernameUnsafeRegex = /[^A-Za-z0-9._-]/gu;
