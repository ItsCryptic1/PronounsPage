export interface User {
    id: string;
    username: string;
    email: string;
    emailHash?: string;
    emailObfuscated?: string;
    roles: string;
    avatarSource: string;
    bannedReason: string;
    usernameNorm: string;
    bannedTerms?: string;
    bannedBy: string;
    lastActive: number;
    banSnapshot: string;
    inactiveWarning: number;
    adminNotifications: number;
    loginAttempts: string;
    timesheets: string;
    socialLookup: number;
    payload?: string;
    mfa?: boolean;
    mfaRequired?: boolean;
    authenticated?: boolean;
    code?: null;
    codeKey?: string | null;
}

export interface Account {
    token: string;
    account: User;
}
