import type { Config } from '../locale/config.ts';

const today = new Date();

export const birthdateRange = (config: Config): { min: Date; max: Date } => {
    return {
        min: new Date(1900, 0, 1),
        max: new Date(today.getFullYear() - (config.ageLimit || 13), today.getMonth(), today.getDate()),
    };
};

export const formatDate = (bd: Date | string | null): string | null => {
    if (!bd) {
        return null;
    }
    if (typeof bd === 'string') {
        return bd;
    }
    return `${bd.getFullYear()}-${`0${bd.getMonth() + 1}`.slice(-2)}-${`0${bd.getDate()}`.slice(-2)}`;
};

export const parseDate = (bd: Date | string | null): Date | null => {
    if (typeof bd !== 'string') {
        return bd;
    }
    return new Date(
        parseInt(bd.substring(0, 4)),
        parseInt(bd.substring(5, 7)) - 1,
        parseInt(bd.substring(8, 10)),
    );
};
