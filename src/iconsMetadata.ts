import iconsMetadataRaw from '@fortawesome/fontawesome-pro/metadata/icons.yml';
import type { IconStyle } from '@fortawesome/fontawesome-pro/metadata/icons.yml';

export interface IconMetadata {
    name: string;
    styles: IconStyle[];
    searchTerms: string[];
}

const iconsMetadata: IconMetadata[] = [];
for (const [iconName, iconMetadataRaw] of Object.entries(iconsMetadataRaw)) {
    iconsMetadata.push({
        name: iconName,
        styles: iconMetadataRaw.styles,
        searchTerms: [
            ...iconMetadataRaw.search.terms.map((t) => `${t}`.toLowerCase()),
            iconName.toLowerCase(),
            iconMetadataRaw.label.toLowerCase(),
        ],
    });
}
export default iconsMetadata;
