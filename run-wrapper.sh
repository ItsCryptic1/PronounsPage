#!/bin/bash
# wrapper for external contexts (cronjob, supervisor) which have no access to nvm to
# use the correct node version and execute node scripts

cd "$(dirname "$0")" || exit

nvm_bin=~/.nvm/versions/node/"$(<.nvmrc)"/bin
export PATH=$nvm_bin:$PATH
if [ "$1" = "start" ]; then
    exec node .output/server/index.mjs -- "$(dirname "$0")" "$PORT"
else
    bin_file=./node_modules/.bin/"$1"
    if [ -f "$bin_file" ]; then
        # directly call bin files when possible to have no pnpm startup cost
        exec "$bin_file" "${@:2}"
    else
        # call through pnpm in other cases
        exec pnpm "$@"
    fi
fi
