declare module '@fortawesome/fontawesome-pro/metadata/icons.yml' {
    export type IconStyle = 'solid' | 'regular' | 'light' | 'duotone' | 'brands';
    interface IconMetadata {
        changes: string[];
        label: string;
        search: { terms: string[] };
        styles: IconStyle[];
        unicode: string;
        voted: boolean;
    }
    const iconMetadata: Record<string, IconMetadata>;
    export default iconMetadata;
}
