declare module '*.vue' {
    import Vue from 'vue';

    export default Vue;
}

declare module '*.md' {
    declare const content: string;
    export default content;
}

declare module '*?raw' {
    declare const content: string;
    export default content;
}
