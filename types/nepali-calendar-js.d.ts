declare module 'nepali-calendar-js' {
    export const nepaliMonthLength: (nYear: number, nMonth: number) => number;
    export const toGregorian: (nYear: number, nMonth: number, nDay: number) => { gy: number; gm: number; gd: number };
}
