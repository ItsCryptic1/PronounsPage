declare module 'zh_cn_zh_tw' {
    export const convertToSimplifiedChinese: (text: string) => string;
    export const convertToTraditionalChinese: (text: string) => string;
}
