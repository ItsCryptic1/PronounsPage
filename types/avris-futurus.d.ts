declare module 'avris-futurus' {
    class Futurus {
        futuriseWord(word: string): string;

        futuriseText(text: string): string;
    }
    const futurus: Futurus;
    export default futurus;
}
