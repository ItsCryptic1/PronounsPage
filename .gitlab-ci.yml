include: '.deploy.gitlab-ci.yml'

stages:
    - 'test'
    - 'deploy staging'
    - 'deploy unpublished'
    - 'deploy production'

workflow:
    rules:
        - if: $CI_COMMIT_BRANCH
        - if: $CI_COMMIT_TAG

check:
    stage: test
    rules:
        - if: $CI_COMMIT_BRANCH
    image: node:20.12.2
    before_script:
        - set -o pipefail
        - export NODE_ENV=development
        - >
            start_section () {
                current_section=$1;
                echo -e "\e[0Ksection_start:`date +%s`:${current_section//[^a-zA-Z0-9]/-}\r\e[0K$current_section";
            };
            end_section () {
                echo -e "\e[0Ksection_end:`date +%s`:${current_section//[^a-zA-Z0-9]/-}\r\e[0K";
            };
            failures=();
            record_failure () {
                exit_code=$?;
                name=${1:-$current_section};
                echo -e "\033[0;31m$name failed with exit code $exit_code\033[0m"; failures+=("$name");
            };
    script:
        - start_section "Setup language version"
        - make switch LANG=en
        - end_section

        - start_section "Install Dependencies"
        # temporarily replace FontAwesomePro dependency with its fake version as the CI can’t access it
        # the name needs to be changed in both package.json and pnpm-lock.yaml
        - 'sed -i "s/git+ssh:\/\/git@gitlab.com:Avris\/FontAwesomePro.git/git+https:\/\/gitlab.com\/Avris\/FakeFontAwesomePro.git/" package.json pnpm-lock.yaml'
        # the referenced SHA needs to be additionally changed in pnpm-lock.yaml
        - 'sed -i "s/git@gitlab.com+Avris\/FontAwesomePro\/f00db606f659dca78b143b7bcab5671b2cb459a8/gitlab.com\/Avris\/FakeFontAwesomePro\/0d322c775cbe9bf99da261700be30251291b51a8/" pnpm-lock.yaml'
        - 'sed -i "s/resolution: {commit: f00db606f659dca78b143b7bcab5671b2cb459a8, repo: git@gitlab.com:Avris\/FontAwesomePro.git, type: git}/resolution: { tarball: https:\/\/gitlab.com\/api\/v4\/projects\/Avris%2FFakeFontAwesomePro\/repository\/archive.tar.gz?sha=0d322c775cbe9bf99da261700be30251291b51a8 }/" pnpm-lock.yaml'
        - corepack enable pnpm
        - pnpm install || record_failure
        - end_section

        - start_section "Setup environment"
        - make install || record_failure
        # revert the changes for the FontAwesomePro dependency only now because `make install` calls `pnpm install`
        - git restore package.json pnpm-lock.yaml
        - end_section

        - start_section "Type checking"
        - pnpm nuxi typecheck || record_failure
        - end_section

        - start_section "Unit Tests"
        - >
            pnpm vitest --reporter=default --reporter=junit --outputFile=junit.xml --coverage || record_failure
        - end_section

        - start_section "Check linting rules"
        - pnpm lint --format gitlab --color --fix | grep -v "^\S*warn" || record_failure
        - end_section

        - start_section "Check for fixable problems"
        - git diff --stat --exit-code || record_failure
        - end_section

        # calls scripts to check for simple issues while disabling actual work (e.g. no publishing to third party)
        # some scripts are left out because they need special configuration or are not safe to smoke test
        - start_section "Smoke test server scripts"
        - pnpm run-file server/migrate.ts || record_failure "Smoke test migrate"
        - pnpm run-file server/calendarBot.js en,pl,es,pt,de,nl,fr,ja,ru,sv,lad,ua,vi "" || record_failure "Smoke test calendarBot"
        - pnpm run-file server/cleanupAccounts.js || record_failure "Smoke test cleanupAccounts"
        - pnpm run-file server/notify.js || record_failure "Smoke test notify"
        - pnpm run-file server/stats.ts || record_failure "Smoke test stats"
        - pnpm run-file server/subscriptions.js || record_failure "Smoke test subscriptions"
        - end_section

        - >
            if [[ ${failures[@]} ]]; then
                echo -e "\033[0;31mSome checks have failed:";
                printf -- "- %s\n" "${failures[@]}";
                echo -en "\033[0m";
                false;
            fi
    coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
    artifacts:
        reports:
            junit: junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/cobertura-coverage.xml
            codequality: gl-codequality.json
    timeout: 20m
# include:
#     - template: Security/Dependency-Scanning.gitlab-ci.yml
#
# Pending resolution of: https://gitlab.com/PronounsPage/PronounsPage/-/merge_requests/453#note_1911466136
